## Requisitos
- Tener instalado **JDK 8 o superior**
- Tener instalado **última versión** de **Maven**
- Solicitar archivo **application.properties**

## Usar Maven
## Usar Maven

Abrir una ventana de comando de window y ejecutar:

    REGRESION CUENTA NEGOCIO - ASSI : mvn clean verify -Denvironment=uat -Dcucumber.options="--tags @regresion_assi"
    - Argumento -Denvironment: uat, stg
    - Argumento -Dcucumber.options: se agrega el tag @regresion_assi colocado en los feature
    
    REGRESION CUENTA NEGOCIO - HYPERLOOP : mvn clean verify -Denvironment=uat -Dcucumber.options="--tags @regresion-hyperloop"
    - Argumento -Denvironment: uat, stg
    - Argumento -Dcucumber.options: se agrega el tag @regresion-hyperloop colocado en los feature
~~~~
REGRESION FACTORING MLP - HYPERLOOP : mvn clean verify -Denvironment=uat -Dcucumber.options="--tags @regresion-hyperloopFactoringMLP"
- Argumento -Denvironment: uat, stg
- Argumento -Dcucumber.options: se agrega el tag @regresion-hyperloopFactoringMLP colocado en los feature
~~~~
~~~~
REGRESION ORQUESTADOR CN: clean verify -Denvironment=uat -Dcucumber.filter.tags=@regresionOrquestadorCN
~~~~
~~~~
REGRESION RESERVA CC: clean verify -Denvironment=uat -Dcucumber.filter.tags=@regresion-hyperloop-CC-Reserva
- Argumento -Denvironment: uat, stg
- Argumento -Dcucumber.options: se agrega el tag @regresion-hyperloop-CC-Reserva colocado en los feature
~~~~
## Tags de ejecución - CUENTA NEGOCIO ASSI

| Tag | Descripción |
| ------------- | ------------- |
| `@regresion_assi` | Ejecución de todos los escenarios. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop_assi.feature) |
| `@persona-juridica` | Ejecución de escenarios con persona juridica. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop_assi.feature) |
| `@persona-natural` | Ejecución de escenarios con persona natural. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop_assi.feature) |
| `@plan-tradicional-soles` | Ejecución de escenarios en plan tradicional soles.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop_assi.feature) |
| `@plan-tradicional-dolares` | Ejecución de escenarios en plan tradicional dolares.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop_assi.feature) |
| `@plan-tradicional-bimoneda` | Ejecución de escenarios en plan tradicional bimoneda.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop_assi.feature) |
| `@plan-digital-soles` | Ejecución de escenarios en plan digital soles.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop_assi.feature) |
| `@plan-digital-dolares` | Ejecución de escenarios en plan digital dolares.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop_assi.feature) |
| `@plan-digital-bimoneda` | Ejecución de escenarios en plan digital bimoneda.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop_assi.feature) |

## Tags de ejecución - CUENTA NEGOCIO HYPERLOOP

| Tag | Descripción |
| ------------- | ------------- |
| `@regresion_hyperloop` | Ejecución de todos los escenarios. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@empresa-constituida` | Ejecución de escenarios con empresa constituida. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@empresa-no-constituida` | Ejecución de escenarios con empresa no constituida. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@plan-tradicional-soles-hyperloop` | Ejecución de escenarios en plan tradicional soles.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@plan-tradicional-dolares-hyperloop` | Ejecución de escenarios en plan tradicional dolares.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@plan-tradicional-bimoneda-hyperloop` | Ejecución de escenarios en plan tradicional bimoneda.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@plan-digital-soles-hyperloop` | Ejecución de escenarios en plan digital soles.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@plan-digital-dolares-hyperloop` | Ejecución de escenarios en plan digital dolares.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@plan-digital-bimoneda-hyperloop` | Ejecución de escenarios en plan digital bimoneda.feature En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |

## Tags de ejecución - FACTORING MLP HYPERLOOP
| Tag | Descripción |
| ------------- | ------------- |
| `@regresion-hyperloopFactoringMLP` | Ejecución de todos los escenarios principales de regresión factoring MLP. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@apertura-cuenta-dolares-hyperloop-Factoring` | Ejecución de escenarios con creación de cuenta en dolares. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@apertura-cuenta-soles-hyperloop-Factoring` | Ejecución de escenarios con creación de cuenta en soles. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@apertura-cuenta-ambasmonedas-hyperloop-Factoring` | Ejecución de escenarios con creación de cuenta en ambas monedas. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@plan-digital-hyperloop-Factoring` | Ejecución de escenarios con creación de cuenta en plan digital. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@plan-tradicional-hyperloop-Factoring` | Ejecución de escenarios con creación de cuenta en plan tradicional. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@RL-Girador-NoCliente` | Ejecución de escenarios de RL y girador NO clientes con creación de cuenta. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@RL-Girador-Cliente` | Ejecución de escenarios de RL y Girador clientes de solo afiliación y de creación de cuenta. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@RlNoCliente-GiradorCliente` | Ejecución de escenarios de RL No Cliente y Girador cliente con creación de cuenta. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@RlCliente-GiradorNoCliente` | Ejecución de escenarios de RL Cliente y Girador No cliente con creación de cuenta. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@afiliacion-hyperloop-Factoring` | Ejecución de escenarios de solo afiliación sin creación de cuenta. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |

## Tags de ejecución - ORQUESTADOR CN
| Tag | Descripción |
| ------------- | ------------- |
| `@regresionOrquestadorCN` | Ejecución de todos los escenarios principales de regresión Orquestador CN. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@Empresa-constituida-orquestador` | Ejecución de escenarios con creación de cuenta para empresas constituidas. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@Empresa-no-constituida-orquestador` | Ejecución de escenarios con creación de cuenta para empresas no constituidas. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@Empresa-no-cliente-orquestador` | Ejecución de escenarios con creación de cuenta para empresas y representante legal como no clientes. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@Empresa-cliente-orquestador` | Ejecución de escenarios con creación de cuenta para empresas y representante legal como clientes. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@Empresa-rl-cliente-no-cliente-orquestador` | Ejecución de escenarios con creación de cuenta para los casos de "empresa cliente - RL no cliente" y "empresa no cliente - RL cliente" . En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |

## Tags de ejecución - RESERVA SOLICITUD CC
| Tag | Descripción |
| ------------- | ------------- |
| `@regresion-hyperloop-CC-Reserva` | Ejecución de todos los escenarios principales de regresión Reserva Solicitud CC. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@CC-UnRepresentanteLegal` | Ejecución de escenarios con un solo representante legal que tiene todas las facultades. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@CC-DosRepresentantesLegales` | Ejecución de escenarios con dos representantes legales. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@CC-Clientes` | Ejecución de escenarios con todos los actores como clientes. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |
| `@CC-NoClientes` | Ejecución de escenarios con todos los actores como No clientes. En la siguiente ruta se encuentran los features: [hyperloop-automation/src/test/resources/features](../master/hyperloop-automation/src/test/resources/features/hyperloop.feature) |

## Ver los reportes

El comando proporcionado anteriormente producirá un reporte de prueba de Serenity en el directorio `target/site/serenity`.



