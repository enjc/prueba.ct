package pe.interbank.hyperloop.automation.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import pe.interbank.hyperloop.automation.steps.CuentaCorrienteAuthSteps;
import net.thucydides.core.annotations.Steps;

public class CuentaCorrienteAuthSD {

   // CuentaCorrienteAuthSteps steps;
    @Steps
    CuentaCorrienteAuthSteps steps;

    @Dado("que los RLs quieren autorizar la reserva")
    public void queLosRLsQuierenAutorizarLaReserva() {
        steps.losRLQuierenAutorizarReserva();
    }

    @Cuando("ingresa al link de la reserva")
    public void ingresaAlLinkDeLaReserva() {
        steps.ingresarLinkDeLaReserva();
    }
    @Y("el RL1 ingresa los datos")
    public void RL1IngresarDatos(DataTable table){
        steps.Rl1IngresaDatos(table);
    }


    @Y("el RL2 ingresa los datos")
    public void elRLIngresaLosDatos(DataTable table) {
        steps.Rl2IngresaDatos(table);
    }

    @Entonces("se realiza la autorización para la apertura de cuenta corriente")
    public void seRealizaLaAutorizaciónParaLaAperturaDeCuentaCorriente() {
        steps.seRealizaLaAutorizaciónParaLaAperturaDeCuentaCorriente();
    }


}
