package pe.interbank.hyperloop.automation.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import pe.interbank.hyperloop.automation.steps.CuentaNegocioTDMSteps;




public class CuentaNegocioTDMSD {

    @Steps
    CuentaNegocioTDMSteps steps;

    @Cuando("realiza la solicitud de CN")
    public void realizaLaSolicitud() {
        steps.SelectResidenciaFiscal();
        Serenity.takeScreenshot();
        steps.obtenerDataCN();
    }
    @Y("el usuario de tipo (.*) ingresa sus datos desde TDM$")
    public void elUsuarioDeTipoCLIENTEIngresaSusDatos(String tipo, DataTable table){
        steps.elUsuarioDeTipoCLIENTEIngresaSusDatos(tipo, table);
    }

}
