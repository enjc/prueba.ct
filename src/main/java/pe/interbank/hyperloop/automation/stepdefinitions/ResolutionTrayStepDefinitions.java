package pe.interbank.hyperloop.automation.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.es.Dado;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import pe.interbank.hyperloop.automation.ui.assi.*;


public class ResolutionTrayStepDefinitions {

    @Steps
    LoginPage loginPage;

    private EnvironmentVariables environmentVariables;

    @Given("^el usuario ingresa en ASSI$")
    public void riskAnalystEnterASSI() {
        loginPage.openUrl(EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("assi.base.url"));
        loginPage.typeUserName("XT9900");
        loginPage.typePassword("Lima2021");
        Serenity.takeScreenshot();
        loginPage.clickLoginButton();
    }

    @Dado("que el usuario apertura de una cuenta negocio")
    public void queElUsuarioAperturaDeUnaCuentaNegocio() {
        loginPage.openUrl(EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("webdriver.base.url")  );
    }

    @Dado("que el usuario desea aperturar una cuenta negocio en el flujo de factoring MLP")
    public void queElUsuarioDeseaAperturarUnaCuentaNegocioEnElFlujoDeFactoringMLP() {
        loginPage.openUrl(EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("factoring.base.url"));
    }

    @Dado("que el usuario ingresa al flujo de factoring MLP")
    public void queElUsuarioIngresaAlFlujoDeFactoringMLP() {
        loginPage.openUrl(EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("factoring.base.url"));
    }

    @Dado("que el operador desea reservar la solicitud de una cuenta corriente")
    public void queElOperadorDeseaReservarLaSolicitudDeUnaCuentaCorriente() {
        loginPage.openUrl(EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("cuentacorriente.base.url"));
    }
}
