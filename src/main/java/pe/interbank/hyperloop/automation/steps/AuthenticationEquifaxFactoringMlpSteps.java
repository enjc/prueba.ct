package pe.interbank.hyperloop.automation.steps;

import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import pe.interbank.hyperloop.automation.ui.hyperloop.AuthenticationFactoringMlpPage;
import pe.interbank.hyperloop.automation.utils.Util;

public class AuthenticationEquifaxFactoringMlpSteps {
    @Steps
    AuthenticationFactoringMlpPage authenticationFactoringMlpPage;

    @Y("el representante legal se autentica por equifax")
    public void elRepresentanteLegalSeAutenticaPorEquifax() {
        Util.waitFor(2);
        authenticationFactoringMlpPage.clickPrimeraRespuesta();
        authenticationFactoringMlpPage.clickSegundaRespuesta();
        authenticationFactoringMlpPage.clickTerceraRespuesta();
        Util.waitFor(1);
        authenticationFactoringMlpPage.clickContinuarEquifaxFactoring();
        Serenity.takeScreenshot();

    }

    @Y("el representante legal selecciona la opcion Estos Datos No son Mios")
    public void elRepresentanteLegalSeleccionaLaOpcionEstosDatosNoSonMios() {
        Util.waitFor(3);
        authenticationFactoringMlpPage.clickEstosDatosNoSonMios();
        Serenity.takeScreenshot();
    }
}
