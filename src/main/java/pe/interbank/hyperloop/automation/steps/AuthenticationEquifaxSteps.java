package pe.interbank.hyperloop.automation.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import pe.interbank.hyperloop.automation.ui.hyperloop.AuthenticationPage;
import pe.interbank.hyperloop.automation.utils.Constantes;
import pe.interbank.hyperloop.automation.utils.Util;

import java.util.List;
import java.util.Map;

public class AuthenticationEquifaxSteps {
    @Steps
    AuthenticationPage authenticationPage;
    private Constantes constantes;
    public AuthenticationEquifaxSteps() {
        constantes = new Constantes();
    }

    @Y("el usuario valida su identidad en EQUIFAX")
    public void elUsuarioValidaSuIdentidadEnEQUIFAX(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        //if(constantes.getTipoCliente().equals("CLIENTE")){
            Util.waitFor(10);
            authenticationPage.SelectEquifax();
            Serenity.takeScreenshot();
       //}

        Util.waitFor(10);
        authenticationPage.validarEquifax(  list.get(0).get("Tarjeta de Credito Activa"),
                                            list.get(0).get("Nombre Padre/Madre"),
                                            list.get(0).get("Nombre Hijo/Hija"),
                                            list.get(0).get("Nombre Hermano/Hermana"),
                                            list.get(0).get("Nombre Abuelo/Abuela"),
                                            list.get(0).get("Credito Hipotecario"),
                                            list.get(0).get("Casado(a)"),
                                            list.get(0).get("Credito Vehicular"),
                                            list.get(0).get("Prestamo Personal"),
                                            list.get(0).get("Ha tenido Carro"),
                                            list.get(0).get("Representa Empresa Lima/Provincia"),
                                            list.get(0).get("Representante Empresa"),
                                            list.get(0).get("Direccion Correspondencia"),
                                            list.get(0).get("Empresa Trabaja"));
        authenticationPage.continuarEquifax();
    }
}
