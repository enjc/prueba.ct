package pe.interbank.hyperloop.automation.steps;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import pe.interbank.hyperloop.automation.ui.hyperloop.AutorizacionPage;

import java.util.List;
import java.util.Map;

public class CuentaCorrienteAuthSteps {

    AutorizacionPage page;
    private EnvironmentVariables variables;
    public void losRLQuierenAutorizarReserva(){
        page.ingresarLinkDeLaReserva();
    }
    public void ingresarLinkDeLaReserva(){
        page.openUrl(EnvironmentSpecificConfiguration.from(variables).getProperty("cuentacorrienteauth.base.url"));
        page.ingresarLinkDeLaReserva();
    }
    public void Rl1IngresaDatos(DataTable table){
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        page.seleccionarTipoDocumento(list.get(0).get("Tipo Documento"));
        page.ingresarNumeroDocumento(list.get(0).get("Numero Documento"));
        page.selectPoliticaPrivacidadCC();
        page.SelctCaptchaCC();
        page.BtSiguienteCC();
        page.obtenerResumenCC();
        page.BtSiguienteCC();
        page.obtenerResumenValid();
        page.aceptarFrima();
        page.aceptaTerminosyCondiciones();
        page.residenciaFiscal();
        page.BtnComenzarBiometría();
        page.solicitudBiometria();
        //page.resumenCC();
        page.BtnTerminar();

    }
    public void Rl2IngresaDatos(DataTable table){
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        page.seleccionarTipoDocumento(list.get(0).get("Tipo Documento"));
        page.ingresarNumeroDocumento(list.get(0).get("Numero Documento"));
        page.selectPoliticaPrivacidadCC();
        page.SelctCaptchaCC();
        page.BtSiguienteCC();
        page.obtenerResumenCC();
        page.BtSiguienteCC();
        page.obtenerResumenValid();
        page.aceptarFrima();
        page.aceptaTerminosyCondiciones();
        page.residenciaFiscal();
        page.BtnComenzarBiometría();
        page.solicitudBiometria();
        page.resumenCC();
        page.BtnTerminar();
    }

    public void seRealizaLaAutorizaciónParaLaAperturaDeCuentaCorriente() {
        System.out.println("Se realizó la apertura de cuenta Corriente");
    }

}
