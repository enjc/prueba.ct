package pe.interbank.hyperloop.automation.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pe.interbank.hyperloop.automation.TDM_CC.dataTDM_CC;
import pe.interbank.hyperloop.automation.utils.Util;
import pe.interbank.hyperloop.automation.ui.assi.LoginPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.SearchPage;
import pe.interbank.hyperloop.automation.utils.Constantes;

import java.util.List;
import java.util.Map;

public class CuentaCorrienteTDMSteps {

    private Constantes constantes;
    @Steps
    SearchPage searchPage;
    LoginPage loginPage;

    public CuentaCorrienteTDMSteps() {constantes = new Constantes();}
    public static dataTDM_CC dataTDM = new dataTDM_CC();

    @Step("realiza la solicitud de CC")
    public void ResidenciaFiscal() {
        loginPage.ResidenciaFiscal();
        Serenity.takeScreenshot();
        dataTDM.obtenerDataCC();
    }

    @Y("el representante legal acepta LDPD1")
    public void elRepresentanteLegalAceptaLDPD1() {
        searchPage.SelectLDPDFactoring();
    }


    @Step
    public void obtenerDataCC(){
        dataTDM.obtenerDataCC();
    }



    //FLUJO CUENTA CORRIENTE RESERVA

    @Cuando("ingresa a la pagina a realizar la solicitud CC")
    public void ingresaALaPaginaARealizarLaSolicitud() {
        loginPage.ResidenciaFiscal();
        Serenity.takeScreenshot();
        dataTDM.obtenerDataCC();
    }

    @Y("el operador ingresa los datos de la empresa desde TDM_CC")
    public void elOperadorIngresaLosDatosDeLaEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        constantes.setTipoDocumento("RUC");
        constantes.setNumeroDocumento(list.get(0).get("RUC"));

        loginPage.completeDatos(list.get(0).get("RUC"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-30 > div > div.col-6.col-xs-12.data-verification-step-spinner > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.ValidarRuc();
        Serenity.takeScreenshot();
        Util.waitFor(5);
    }

    @Y("el operador ingresa sus datos personales desde TDM")
    public void elOperadorIngresaSusDatosPersonales(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);

        loginPage.SelectConfirmar();
        searchPage.selectDocumentTypeCC(list.get(0).get("Tipo Documento"));
        searchPage.completeDocNumCC(list.get(0).get("Numero Documento"));
        loginPage.completeDatos(list.get(0).get("Nombres"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-40.mt-xs-30.grid-input > div.col-xs-12.col-6.grid__right.mb-xs-30 > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.completeDatos(list.get(0).get("Apellidos"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-40.mt-xs-30.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.completeDatos(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div:nth-child(6) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.SelectTerminos();
        loginPage.SelectCaptcha();
        loginPage.SelectCaptcha();
        Serenity.takeScreenshot();
        loginPage.Comencemos();
    }

    

}
