package pe.interbank.hyperloop.automation.steps;


import io.cucumber.datatable.DataTable;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pe.interbank.hyperloop.automation.TDMCN.dataTDM_CN;
import pe.interbank.hyperloop.automation.ui.assi.LoginPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.SearchPage;
import pe.interbank.hyperloop.automation.utils.Constantes;


import java.util.List;
import java.util.Map;


public class CuentaNegocioTDMSteps {

    private Constantes constantes;
    @Steps
    SearchPage searchPage;
    LoginPage loginPage;

    public CuentaNegocioTDMSteps() {
        constantes = new Constantes();
    }
    public static dataTDM_CN dataTDM = new dataTDM_CN();

    @Step("realiza la solicitud de CN")
    public void SelectResidenciaFiscal() {
        loginPage.SelectResidenciaFiscal();
        Serenity.takeScreenshot();
        dataTDM.obtenerDataCN();
    }
    @Step
    public void obtenerDataCN(){
        dataTDM.obtenerDataCN();
    }
    @Step("Obtener datos")
    public void elUsuarioDeTipoCLIENTEIngresaSusDatos(String tipo, DataTable table) {

        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        constantes.setTipoCliente(tipo);
        constantes.setTipoEmpresa(list.get(0).get("Empresa"));
        //constantes.setTipoEmpresa(dataTDM.getClienteCN().getTipoEmpresa());
        loginPage.selectEmpresa(list.get(0).get("Empresa"));
        //loginPage.selectEmpresa(dataTDM.getClienteCN().getTipoEmpresa());
        //if(dataTDM.getClienteCN().getTipoEmpresa().equalsIgnoreCase("CONSTITUIDA")) {
        if(list.get(0).get("Empresa").equalsIgnoreCase("CONSTITUIDA")) {
            constantes.setTipoDocumento("RUC");
            constantes.setNumeroDocumento(dataTDM.getClienteCN().getNroDocEmp());
            loginPage.completeDatos(dataTDM.getClienteCN().getNroDocEmp(), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-30 > div > div.col-6.data-verification-step-spinner > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.buscarRuc();
            Serenity.takeScreenshot();
            searchPage.selectDocumentTypeHyper(list.get(0).get("Tipo Documento"));
            searchPage.completeDocNumHyper(list.get(0).get("Numero Documento"));
            //loginPage.completeDatosEmail(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div:nth-child(6) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.completeDatos(list.get(0).get("Email"), ".lowercase");
            loginPage.selectOperador(list.get(0).get("Operador"));
            loginPage.completeDatos(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-40.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.SelectTerminos();
            //loginPage.SelectCaptcha();
            //loginPage.SelectCaptcha();
            Serenity.takeScreenshot();
        }else{

            constantes.setTipoDocumento(list.get(0).get("Tipo Documento"));
            constantes.setNumeroDocumento(dataTDM.getClienteCN().getNroDocEmp());
            Serenity.takeScreenshot();

            searchPage.selectDocumentTypeHyper(list.get(0).get("Tipo Documento"));
            searchPage.completeDocNumHyper(list.get(0).get("Numero Documento"));
            loginPage.completeDatos(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div:nth-child(5) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.selectOperador(list.get(0).get("Operador"));
            loginPage.completeDatos(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-40.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.SelectTerminos();
            //loginPage.SelectCaptcha();
            //loginPage.SelectCaptcha();
            Serenity.takeScreenshot();
        }
        loginPage.BtnSiguiente();
    }


}
