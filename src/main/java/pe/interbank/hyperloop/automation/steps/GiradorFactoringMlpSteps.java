package pe.interbank.hyperloop.automation.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import pe.interbank.hyperloop.automation.ui.hyperloop.SearchPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.WorkInfoPage;
import pe.interbank.hyperloop.automation.utils.Util;

import java.util.List;
import java.util.Map;

public class GiradorFactoringMlpSteps {


    @Steps

    SearchPage searchPage;
    WorkInfoPage workInfoPage;

    @Y("el representante legal ingresa y busca el {string} de su empresa")
    public void elRepresentanteLegalIngresaYBuscaElRucDeSuEmpresa(String Ruc) {

        Util.waitFor(1);
        searchPage.completarDatosFactoring(Ruc, "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.business-view > form > div.grid.mt-10 > div > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.clickBuscarRucFactoring();

    }



    @Y("el representante legal completa las ventas anuales y de direccion de su empresa")
    public void elRepresentanteLegalCompletaLasVentasAnualesYDeDireccionDeSuEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(1);
        searchPage.completarDatosFactoring(list.get(0).get("Ventas anuales"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.business-view > form > div:nth-child(5) > div > div.group-tabInput__wrapper-tab > a-hyperloop-input-amount > div > input");
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Departamento"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.business-view > form > div:nth-child(7) > div.grid.mt-30.mt-xs-5 > div:nth-child(1) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Provincia"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.business-view > form > div:nth-child(7) > div.grid.mt-30.mt-xs-5 > div:nth-child(2) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Distrito"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.business-view > form > div:nth-child(7) > div.grid.mt-30.mt-xs-5 > div:nth-child(3) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        Serenity.takeScreenshot();
        Util.waitFor(1);
        workInfoPage.selectViaEmpresaFactoring(list.get(0).get("Tipo Via"));
        workInfoPage.getparametrosFactoring(list.get(0).get("Nombre Via"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.business-view > form > div:nth-child(7) > div:nth-child(3) > div.col-8.col-xs-12.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametrosFactoring(list.get(0).get("Numero"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.business-view > form > div:nth-child(7) > div:nth-child(4) > div:nth-child(1) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.clickContinuarNoclienteGiradorFactoring();
        Serenity.takeScreenshot();
    }

    @Y("el representante legal completa las ventas anuales de su empresa")
    public void elRepresentanteLegalCompletaLasVentasAnualesDeSuEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(3);
        searchPage.completarDatosFactoring(list.get(0).get("Ventas anuales"), ".HyperloopInputMask__input.m");
        workInfoPage.clickContinuarNoclienteGiradorFactoring();
    }
}

    

