package pe.interbank.hyperloop.automation.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import pe.interbank.hyperloop.automation.ui.hyperloop.HubFactoringMlpPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.SearchPage;
import pe.interbank.hyperloop.automation.utils.Util;

import java.util.List;
import java.util.Map;

public class HubFactoringMlpSteps {
    @Steps

    SearchPage searchPage;
    HubFactoringMlpPage hubFactoringMlpPage;

    @Y("el representante legal busca la razon social de su empresa")
    public void elRepresentanteLegalBuscaLaRazonSocialDeSuEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(1);
        hubFactoringMlpPage.completarHubFactoring(list.get(0).get("Razon social"));
        hubFactoringMlpPage.clickBuscarHubFactoring();
        Util.waitFor(3);
    }

    @Y("el representante legal completa los datos de su aceptante")
    public void elRepresentanteLegalCompletaLosDatosDeSuAceptante(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(1);


        hubFactoringMlpPage.selectMonedaFactoring(list.get(0).get("Moneda"));
        hubFactoringMlpPage.completarDatosHubFactoring(list.get(0).get("Monto mensual"), ".HyperloopInputMask__input.m");
        hubFactoringMlpPage.selectPlazoFactoring(list.get(0).get("Plazo mensual"),"a-hyperloop-select");

        hubFactoringMlpPage.clickContinuarHubFactoring();
        Serenity.takeScreenshot();

    }


    @Y("el representante legal completa los datos de las ventas mensuales en soles de su aceptante")
    public void elRepresentanteLegalCompletaLosDatosDeLasVentasMensualesEnSolesDeSuAceptante(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(1);

        hubFactoringMlpPage.selectMonedaFactoring(list.get(0).get("Moneda"));
        hubFactoringMlpPage.completarDatosHubFactoring(list.get(0).get("Monto mensual"), ".HyperloopInputMask__input");
        hubFactoringMlpPage.selectPlazoFactoring(list.get(0).get("Plazo mensual"),"a-hyperloop-select");


        Serenity.takeScreenshot();
        hubFactoringMlpPage.clickOtraMonedaHubFactoring();
    }

    @Y("el representante legal completa los datos de las ventas mensuales en dolares de su aceptante")
    public void elRepresentanteLegalCompletaLosDatosDeLasVentasMensualesEnDolaresDeSuAceptante(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);


        hubFactoringMlpPage.completarDatosHubFactoring(list.get(0).get("Monto mensual"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-purchaser-step > fact-purchaser-step-ui > section > div.business-view > form > div.container-formulario.mt-10 > div.ng-touched.ng-dirty.ng-invalid > fact-purchaser-sale-form:nth-child(2) > div > div.col-5.col-xs-12.mt-xs-20.tabInputGroup > div > div > a-hyperloop-input-amount > div > input");
        hubFactoringMlpPage.selectPlazoFactoring(list.get(0).get("Plazo mensual"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-purchaser-step > fact-purchaser-step-ui > section > div.business-view > form > div.container-formulario.mt-10 > div.ng-touched.ng-dirty.ng-invalid > fact-purchaser-sale-form:nth-child(2) > div > div.col-4.col-xs-12.mt-xs-20 > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        Util.waitFor(1);
        hubFactoringMlpPage.clickContinuarHubFactoring();
        Serenity.takeScreenshot();
    }
}
