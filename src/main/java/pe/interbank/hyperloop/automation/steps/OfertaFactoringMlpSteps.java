package pe.interbank.hyperloop.automation.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import pe.interbank.hyperloop.automation.ui.hyperloop.OfertaFactoringMlpPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.SearchPage;
import pe.interbank.hyperloop.automation.utils.Util;

import java.util.List;
import java.util.Map;

public class OfertaFactoringMlpSteps {

    @Steps
    OfertaFactoringMlpPage ofertaFactoringMlpPage;
    SearchPage searchPage;

    @Y("el representante legal acepta los TC e ingresa datos de contacto de su empresa")
    public void elRepresentanteLegalAceptaLosTCEIngresaDatosDeContactoDeSuEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(5);
        ofertaFactoringMlpPage.clickTerminosYCondiciones();

        searchPage.completarDatosFactoring(list.get(0).get("Nombre contacto"),"m-hyperloop-inputgroup-text[label='Nombres  y apellidos (*)']  .hydrated  .HyperloopInput__input.lowercase.m");
        searchPage.completarDatosFactoring(list.get(0).get("Email contacto"),"m-hyperloop-inputgroup-text[label='Correo electrónico (*)']  .hydrated  .HyperloopInput__input.lowercase.m");
        Util.waitFor(1);
        ofertaFactoringMlpPage.clickContinuarOfertaFactoring();
        Serenity.takeScreenshot();

    }


    @Y("el representante legal selecciona abrir una cuenta negocios")
    public void elRepresentanteLegalSeleccionaAbrirUnaCuentaNegocios() {
        Util.waitFor(2);
        ofertaFactoringMlpPage.clickAbrirCuentaNegociosFactoring();
        Serenity.takeScreenshot();

    }

    @Y("el representante legal elige un plan tarifario")
    public void elRepresentanteLegalEligeUnPlanTarifario(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(2);

        ofertaFactoringMlpPage.selectPlanFactoring(list.get(0).get("Plan tarifario"));
        Serenity.takeScreenshot();
        Util.waitFor(1);
        ofertaFactoringMlpPage.clickContinuarConfigurarCuentaFactoring();
        Serenity.takeScreenshot();
    }

    @Y("el representante legal ingresa el numero de partida registral y selecciona Principales Clientes")
    public void elRepresentanteLegalIngresaElNumeroDePartidaRegistralYSeleccionaPrincipalesClientes(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(1);
        searchPage.completarDatosFactoring(list.get(0).get("Partida Registral"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.grid.mt-30 > div.col-5.col-xs-12 > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        ofertaFactoringMlpPage.selectPrincipalesClientesFactoring(list.get(0).get("Principales clientes"));
    }

    @Y("el representante legal ingresa el correo de trabajo de su empresa")
    public void elRepresentanteLegalIngresaElCorreoDeTrabajoDeSuEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        searchPage.completarDatosFactoring(list.get(0).get("Correo empresa"), ".HyperloopInput__input.lowercase.m");
        Serenity.takeScreenshot();
    }

    @Y("el representante legal selecciona la agencia de operaciones mas frecuentes")
    public void elRepresentanteLegalSeleccionaLaAgenciaDeOperacionesMasFrecuentes(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(1);
        ofertaFactoringMlpPage.selectDepartamentoAgenciaFactoring(list.get(0).get("Agencia Departamento"));
        Util.waitFor(1);
        ofertaFactoringMlpPage.selectProvinciaAgenciaFactoring(list.get(0).get("Provincia Departamento"));
        ofertaFactoringMlpPage.selectTerminosYCondicionesCNFactoring();
        Serenity.takeScreenshot();

        ofertaFactoringMlpPage.clickContinuarResumenCreacionCNFactoring();
        Util.waitFor(1);
        ofertaFactoringMlpPage.clickModalConfirmacionFactoring();
        Util.waitFor(10);
        Serenity.takeScreenshot();
    }



    @Entonces("el representante legal acepta y envía la solicitud de afiliacion para su empresa")
    public void elrepresentanteLegalAceptaYEnviaLaSolicitudDeAfiliacionParaSuEmpresa() {
        Util.waitFor(1);
        ofertaFactoringMlpPage.clickModalConfirmacionFactoring();

        ofertaFactoringMlpPage.subirPantallResumen();
        Util.waitFor(10);
        Serenity.takeScreenshot();
    }

    @Y("el representante legal selecciona la oferta en la moneda")
    public void elRepresentanteLegalSeleccionaLaOfertaEnLaMoneda(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(5);
        ofertaFactoringMlpPage.selectMonedaCuentaFactoring(list.get(0).get("Moneda Cuenta"));
    }

    @Entonces("el representante legal apertura una cuenta negocios para su empresa")
    public void elRepresentanteLegalAperturaUnaCuentaNegociosParaSuEmpresa() {
        ofertaFactoringMlpPage.bajarpantallaResumen();
        Util.waitFor(8);
        Serenity.takeScreenshot();
    }


}
