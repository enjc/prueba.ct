package pe.interbank.hyperloop.automation.steps;

import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import pe.interbank.hyperloop.automation.utils.Parameters;
import pe.interbank.hyperloop.automation.utils.Util;

import static io.restassured.RestAssured.given;
import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;


public class OrquestadorCNSteps {
    private Parameters parameters;

    public OrquestadorCNSteps() {
        parameters = new Parameters();
    }

    public void consumirOrquestador(String Moneda, String Plan, String Tipodocumento, String Nrodocumento, String TipopersonaEmp, String TipopersonaRL, String TipodocumentoRL, String NumerodocumentoRL)
    {
        try {
            System.out.println("token: " + createAccessToken());
            if (TipodocumentoRL.equals("DNI")) {
                if (Moneda.equals("AMBAS")) {
                    String parametros = Util.getTemplate(parameters.TEMPLATE_BODY_ORQUESTADOR5)
                            .replace("{{Moneda1}}", "PEN")
                            .replace("{{Moneda2}}", "USD")
                            .replace("{{Plan}}", Plan)
                            .replace("{{Tipodocumento}}", Tipodocumento)
                            .replace("{{Nrodocumento}}", Nrodocumento)
                            .replace("{{TipopersonaEmp}}", TipopersonaEmp)
                            .replace("{{TipopersonaRL}}", TipopersonaRL)
                            .replace("{{TipodocumentoRL}}", TipodocumentoRL)
                            .replace("{{NumerodocumentoRL}}", NumerodocumentoRL);
                    SerenityRest.given()
                            .contentType(ContentType.JSON)
                            .header("Authorization", createAccessToken())
                            .header("Ocp-Apim-Subscription-Key", "ec989652e3ff438bab250d3e7642e459")
                            .header("processTraceId", "12313123123")
                            .body(parametros)
                            .when()
                            .post("https://eu2-ibk-apm-uat-ext-001.azure-api.net/business/v1/businessAccount/person");
                    System.out.println("body:" + parametros);
                } else {
                    String parametros1 = Util.getTemplate(parameters.TEMPLATE_BODY_ORQUESTADOR4)
                            .replace("{{Moneda}}", Moneda)
                            .replace("{{Plan}}", Plan)
                            .replace("{{Tipodocumento}}", Tipodocumento)
                            .replace("{{Nrodocumento}}", Nrodocumento)
                            .replace("{{TipopersonaEmp}}", TipopersonaEmp)
                            .replace("{{TipopersonaRL}}", TipopersonaRL)
                            .replace("{{TipodocumentoRL}}", TipodocumentoRL)
                            .replace("{{NumerodocumentoRL}}", NumerodocumentoRL);
                    SerenityRest.given()
                            .contentType(ContentType.JSON)
                            .header("Authorization", createAccessToken())
                            .header("Ocp-Apim-Subscription-Key", "ec989652e3ff438bab250d3e7642e459")
                            .header("processTraceId", "12313123123")
                            .body(parametros1)
                            .when()
                            .post("https://eu2-ibk-apm-uat-ext-001.azure-api.net/business/v1/businessAccount/person");

                    System.out.println("body:" + parametros1);
                }
            } else {
                switch (Moneda) {
                    case "AMBAS":
                        String parametros = Util.getTemplate(parameters.TEMPLATE_BODY_ORQUESTADOR2)
                                .replace("{{numpasaporte}}", "3333444")
                                .replace("{{paisNacimiento}}", "ARGENTINA")
                                .replace("{{paisNacionalidad}}", "BRASIL")
                                .replace("{{Moneda1}}", "PEN")
                                .replace("{{Moneda2}}", "USD")
                                .replace("{{Plan}}", Plan)
                                .replace("{{Tipodocumento}}", Tipodocumento)
                                .replace("{{Nrodocumento}}", Nrodocumento)
                                .replace("{{TipopersonaEmp}}", TipopersonaEmp)
                                .replace("{{TipopersonaRL}}", TipopersonaRL)
                                .replace("{{TipodocumentoRL}}", TipodocumentoRL)
                                .replace("{{NumerodocumentoRL}}", NumerodocumentoRL);
                        SerenityRest.given()
                                .contentType(ContentType.JSON)
                                .header("Authorization", createAccessToken())
                                .header("Ocp-Apim-Subscription-Key", "ec989652e3ff438bab250d3e7642e459")
                                .header("processTraceId", "12313123123")
                                .body(parametros)
                                .when()
                                .post("https://eu2-ibk-apm-uat-ext-001.azure-api.net/business/v1/businessAccount/person");
                        System.out.println("body:" + parametros);
                        break;
                    default:
                        String parametros1 = Util.getTemplate(parameters.TEMPLATE_BODY_ORQUESTADOR1)
                                .replace("{{numpasaporte}}", "3333444")
                                .replace("{{paisNacimiento}}", "ARGENTINA")
                                .replace("{{paisNacionalidad}}", "BRASIL")
                                .replace("{{Moneda}}", Moneda)
                                .replace("{{Plan}}", Plan)
                                .replace("{{Tipodocumento}}", Tipodocumento)
                                .replace("{{Nrodocumento}}", Nrodocumento)
                                .replace("{{TipopersonaEmp}}", TipopersonaEmp)
                                .replace("{{TipopersonaRL}}", TipopersonaRL)
                                .replace("{{TipodocumentoRL}}", TipodocumentoRL)
                                .replace("{{NumerodocumentoRL}}", NumerodocumentoRL);
                        SerenityRest.given()
                                .contentType(ContentType.JSON)
                                .header("Authorization", createAccessToken())
                                .header("Ocp-Apim-Subscription-Key", "ec989652e3ff438bab250d3e7642e459")
                                .header("processTraceId", "12313123123")
                                .body(parametros1)
                                .when()
                                .post("https://eu2-ibk-apm-uat-ext-001.azure-api.net/business/v1/businessAccount/person");
                        System.out.println("body:" + parametros1);
                }
            }
            System.out.println("response: " + lastResponse().body().prettyPrint());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public void consumirOrquestadorNoConstituida(String Moneda, String Plan, String TipopersonaRL,String TipodocumentoRL, String NumerodocumentoRL)
    {
        {
            try {
                System.out.println("token: " + createAccessToken());
                if (TipodocumentoRL.equals("DNI")) {
                    String parametros1 = Util.getTemplate(parameters.TEMPLATE_BODY_ORQUESTADOR6)
                            .replace("{{Moneda}}", Moneda)
                            .replace("{{Plan}}", Plan)
                            .replace("{{TipopersonaRL}}", TipopersonaRL)
                            .replace("{{TipodocumentoRL}}", TipodocumentoRL)
                            .replace("{{NumerodocumentoRL}}", NumerodocumentoRL);
                    SerenityRest.given()
                            .contentType(ContentType.JSON)
                            .header("Authorization", createAccessToken())
                            .header("Ocp-Apim-Subscription-Key", "ec989652e3ff438bab250d3e7642e459")
                            .header("processTraceId", "12313123123")
                            .body(parametros1)
                            .when()
                            .post("https://eu2-ibk-apm-uat-ext-001.azure-api.net/business/v1/businessAccount/person");

                    System.out.println("body:" + parametros1);
                } else {
                    String parametros = Util.getTemplate(parameters.TEMPLATE_BODY_ORQUESTADOR3)
                            .replace("{{numpasaporte}}", "3333444")
                            .replace("{{paisNacimiento}}", "ARGENTINA")
                            .replace("{{paisNacionalidad}}", "BRASIL")
                            .replace("{{Moneda}}", Moneda)
                            .replace("{{Plan}}", Plan)
                            .replace("{{TipopersonaRL}}", TipopersonaRL)
                            .replace("{{TipodocumentoRL}}", TipodocumentoRL)
                            .replace("{{NumerodocumentoRL}}", NumerodocumentoRL);
                    SerenityRest.given()
                            .contentType(ContentType.JSON)
                            .header("Authorization", createAccessToken())
                            .header("Ocp-Apim-Subscription-Key", "ec989652e3ff438bab250d3e7642e459")
                            .header("processTraceId", "12313123123")
                            .body(parametros)
                            .when()
                            .post("https://eu2-ibk-apm-uat-ext-001.azure-api.net/business/v1/businessAccount/person");
                    System.out.println("body:" + parametros);
                }
                System.out.println("response: " + lastResponse().body().prettyPrint());

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static String createAccessToken() {
        String body = "scope=all&grant_type=client_credentials";
        try {
            return "Bearer " + given().header("Authorization", "Basic QkNXOnh0WDZiejVKOTdQUkpYV2I")
                    .contentType(ContentType.URLENC)
                    .header("ocp-apim-subscription-key", "c9664a560c184e8cb857e5d2a7efabc4")
                    .body(body)
                    .post("https://apis.uat.interbank.pe/api/clientes/oauth/token")
                    .then()
                    .extract().response().jsonPath().getString("access_token");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public void respuestaServicio(int i) {
        System.out.println("status: " + lastResponse().getStatusCode());
        assertThat(lastResponse().statusCode(), is(i));
    }
}