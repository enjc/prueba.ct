package pe.interbank.hyperloop.automation.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import pe.interbank.hyperloop.automation.ui.hyperloop.PersonalInfoPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.ProductInfoPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.SearchPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.WorkInfoPage;
import pe.interbank.hyperloop.automation.ui.assi.LoginPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.OfertaFactoringMlpPage;
import pe.interbank.hyperloop.automation.utils.Constantes;
import pe.interbank.hyperloop.automation.utils.Util;

import java.util.List;
import java.util.Map;

public class PersonalInfoSteps {
    @Steps
    PersonalInfoPage personalInfoPage;
    WorkInfoPage workInfoPage;
    ProductInfoPage productInfoPage;
    SearchPage searchPage;
    LoginPage loginPage;
    OfertaFactoringMlpPage ofertaFactoringMlpPage;
    private Constantes constantes;
    public PersonalInfoSteps() {
        constantes = new Constantes();
    }


    @Y("el usuario completa los datos de la empresa$")
    public void elUsuarioDeTipoCLIENTECompletaLosDatosDeLaEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        workInfoPage.selectPrincipalCliente();
        workInfoPage.ingresoTexto(list.get(0).get("NUMERO DE PARTIDA REGISTRAL"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-company-info-step > div.View.block > cc-company-info-client > form > div:nth-child(2) > div.flex.col.right.content-link > ibk-inputgroup-text");
        workInfoPage.ingresoTexto(list.get(0).get("EMAIL"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-company-info-step > div.View.block > cc-company-info-client > form > div:nth-child(3) > div.flex.col.left > ibk-inputgroup-text");
        Serenity.takeScreenshot();
        workInfoPage.continuar("#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-company-info-step > div.Step-action.grid.grid-end.mt-30 > div > ibk-button");
    }

    @Y("el usuario completa los datos del representate legal de tipo (.*)$")
    public void elUsuarioCompletaLosDatosDelRepresentateLegal(String personType, DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        personalInfoPage.selectDocumentType(list.get(0).get("Tipo Documento"));
        workInfoPage.ingresoTexto(list.get(0).get("Numero Documento"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > cc-legal-representative-search-pj > form > div > div.flex-9.col.left > div > div.flex-7.col.right > ibk-inputgroup-text");
        personalInfoPage.clickSearch();
        Serenity.takeScreenshot();
        personalInfoPage.selectOperador(list.get(0).get("Operador"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(3) > div.flex.col.left > ibk-inputgroup-select");
        Serenity.takeScreenshot();
        workInfoPage.continuar("#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step-action.grid.grid-end.mt-30 > div.col.col-end > ibk-button");
    }

    @Y("el usuario completa los datos personales del representate legal de tipo (.*)$")
    public void elUsuarioCompletaLosDatosPersonalesDelRepresentateLegalDeTipoNOCLIENTE(String personType, DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        if(constantes.getTipoPersona().equals("PERSONA JURIDICA")) {
            personalInfoPage.selectDocumentType(list.get(0).get("Tipo Documento"));
            workInfoPage.ingresoTexto(list.get(0).get("Numero Documento"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > cc-legal-representative-search-pj > form > div > div.flex-9.col.left > div > div.flex-7.col.right > ibk-inputgroup-text");
            Serenity.takeScreenshot();
            personalInfoPage.clickSearch();
            workInfoPage.ingresoTexto(list.get(0).get("Primer Nombre"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(1) > div.flex.col.left > ibk-inputgroup-text");
            workInfoPage.ingresoTexto(list.get(0).get("Segundo Nombre"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(1) > div.flex.col.right > ibk-inputgroup-text");
            workInfoPage.ingresoTexto(list.get(0).get("Apellido Paterno"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(2) > div.flex.col.left > ibk-inputgroup-text");
            workInfoPage.ingresoTexto(list.get(0).get("Apellido Materno"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(2) > div.flex.col.right > ibk-inputgroup-text");
            Serenity.takeScreenshot();
            workInfoPage.ingresoTexto(list.get(0).get("Email"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(3) > ibk-inputgroup-text");
            personalInfoPage.selectOperador(list.get(0).get("Operador"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(4) > div.flex.col.left > ibk-inputgroup-select");
            workInfoPage.ingresoTexto(list.get(0).get("Celular"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(4) > div.flex.col.right > ibk-inputgroup-text");
            Serenity.takeScreenshot();
            workInfoPage.ingresoFecha("01 01 1990", "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(5) > div.flex.col.left > ibk-inputgroup-text");
            personalInfoPage.selectGenero();
            personalInfoPage.estadoCivil();
            Serenity.takeScreenshot();
            workInfoPage.continuar("#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step-action.grid.grid-end.mt-30 > div.col.col-end > ibk-button");
        }else{
            switch (personType) {
                case "CLIENTE":
                    Util.waitFor(3);
                    personalInfoPage.selectOperador(list.get(0).get("Operador"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(3) > div.flex.col.left > ibk-inputgroup-select");
                    workInfoPage.continuar("#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step-action.grid.grid-end.mt-30 > div.col.col-end > ibk-button");
                    break;
                case "NO CLIENTE":
                case "POTENCIAL":
                    Util.waitFor(3);
                    workInfoPage.ingresoTexto(list.get(0).get("Primer Nombre"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(1) > div.flex.col.left > ibk-inputgroup-text");
                    workInfoPage.ingresoTexto(list.get(0).get("Segundo Nombre"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(1) > div.flex.col.right > ibk-inputgroup-text");
                    workInfoPage.ingresoTexto(list.get(0).get("Apellido Paterno"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(2) > div.flex.col.left > ibk-inputgroup-text");
                    workInfoPage.ingresoTexto(list.get(0).get("Apellido Materno"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(2) > div.flex.col.right > ibk-inputgroup-text");
                    Serenity.takeScreenshot();
                    workInfoPage.ingresoTexto(list.get(0).get("Email"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(3) > ibk-inputgroup-text");
                    personalInfoPage.selectOperador(list.get(0).get("Operador"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(4) > div.flex.col.left > ibk-inputgroup-select");
                    workInfoPage.ingresoTexto(list.get(0).get("Celular"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(4) > div.flex.col.right > ibk-inputgroup-text");
                    Serenity.takeScreenshot();
                    workInfoPage.ingresoFecha("01 01 1990", "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(5) > div.flex.col.left > ibk-inputgroup-text");
                    personalInfoPage.selectGenero();
                    personalInfoPage.estadoCivil();
                    Serenity.takeScreenshot();
                    workInfoPage.ingresoTexto(list.get(0).get("Numero de pasaporte"), "#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(7) > div.flex.col.left > ibk-inputgroup-text");
                    personalInfoPage.paisNacimiento();
                    personalInfoPage.paisNacionalidad();
                    Serenity.takeScreenshot();
                    workInfoPage.continuar("#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step-action.grid.grid-end.mt-30 > div.col.col-end > ibk-button");
                    break;
            }
        }
    }

    @Y("el usuario completa los datos de domicilio del representate legal")
    public void elUsuarioCompletaLosDatosDeDomicilioDelRepresentateLegal(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        workInfoPage.ubigeo(list.get(0).get("Departamento"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(1) > div > cc-ubigeo-form > div > div > div > div:nth-child(1) > ibk-autocompletegroup-text");
        workInfoPage.ubigeo(list.get(0).get("Provincia"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(1) > div > cc-ubigeo-form > div > div > div > div:nth-child(2) > ibk-autocompletegroup-text");
        workInfoPage.ubigeo(list.get(0).get("Distrito"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(1) > div > cc-ubigeo-form > div > div > div > div:nth-child(3) > ibk-autocompletegroup-text");
        Serenity.takeScreenshot();
        workInfoPage.ubigeo(list.get(0).get("Tipo Via"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(2) > div > cc-address-form > div > form > div:nth-child(1) > div.col.col-4 > ibk-autocompletegroup-text");
        workInfoPage.ingresoTexto(list.get(0).get("Nombre Via"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(2) > div > cc-address-form > div > form > div:nth-child(1) > div.col.col-8 > ibk-inputgroup-text");
        workInfoPage.ingresoTexto(list.get(0).get("Numero"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(2) > div > cc-address-form > div > form > div:nth-child(2) > div:nth-child(1) > ibk-inputgroup-text");
        workInfoPage.ingresoTexto(list.get(0).get("Manzana"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(2) > div > cc-address-form > div > form > div:nth-child(2) > div:nth-child(2) > ibk-inputgroup-text");
        workInfoPage.ingresoTexto(list.get(0).get("Lote"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(2) > div > cc-address-form > div > form > div:nth-child(2) > div:nth-child(3) > ibk-inputgroup-text");
        workInfoPage.ingresoTexto(list.get(0).get("Interior"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(2) > div > cc-address-form > div > form > div:nth-child(2) > div:nth-child(4) > ibk-inputgroup-text");
        workInfoPage.ingresoTexto(list.get(0).get("Urbanizacion"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(2) > div > cc-address-form > div > form > div:nth-child(3) > div > ibk-inputgroup-text");
        workInfoPage.ingresoTexto(list.get(0).get("Referencia"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step > div > div:nth-child(2) > div > cc-address-form > div > form > div:nth-child(4) > div > ibk-inputgroup-text");
        Serenity.takeScreenshot();
        workInfoPage.continuar("#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-address-legal-representative-step > div.Step-action.grid.grid-end.mt-30 > div.col.col-end > ibk-button");
    }

    @Y("el usuario ingresa los datos de la empresa")
    public void elUsuarioIngresaLosDatosDeLaEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(5);
        workInfoPage.selectTipodeContribuyente(list.get(0).get("Tipo de contribuyente"));
        workInfoPage.ingresoRazonSocial(list.get(0).get("Razon social"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div:nth-child(3) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.ingresoFechaConstitucion("01011990");
        Serenity.takeScreenshot();
        workInfoPage.getparametros(list.get(0).get("Numero de Partida Registral"), "#registrationNumber > div > a-hyperloop-input > div > input");
        workInfoPage.getparametros(list.get(0).get("Correo electronico de la empresa"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div:nth-child(5) > div:nth-child(1) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");

        productInfoPage.selecRubroActividad(list.get(0).get("Ventas"));
        Serenity.takeScreenshot();

        workInfoPage.selectUbigeo(list.get(0).get("Departamento"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div.mt-40 > div:nth-child(2) > div:nth-child(1) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeo(list.get(0).get("Provincia"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div.mt-40 > div:nth-child(2) > div:nth-child(2) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeo(list.get(0).get("Distrito"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div.mt-40 > div:nth-child(2) > div:nth-child(3) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        Serenity.takeScreenshot();

        workInfoPage.selectUbigeo(list.get(0).get("Tipo de via"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div.mt-40 > div:nth-child(3) > div.col-4.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.getparametros(list.get(0).get("Nombre de via"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div.mt-40 > div:nth-child(3) > div.col-8.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametros(list.get(0).get("Numero"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div.mt-40 > div:nth-child(4) > div:nth-child(1) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        Serenity.takeScreenshot();

        Util.waitFor(5);



        //workInfoPage.getparametros(list.get(0).get("Actividad"), "#dropdown-select > div > div > input");







        workInfoPage.clicksiguiente();
    }

    @Y("el usuario ingresa los datos del representante legal")
    public void elUsuarioIngresaLosDatosDelRepresentanteLegal(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        workInfoPage.getparametros(list.get(0).get("Primer Nombre"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(3) > div:nth-child(1) > div.col-xs-12.col-6.grid__right > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametros(list.get(0).get("Segundo Nombre"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(3) > div:nth-child(1) > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametros(list.get(0).get("Apellido Paterno"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(3) > div:nth-child(2) > div.col-xs-12.col-6.grid__right > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametros(list.get(0).get("Apellido Materno"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(3) > div:nth-child(2) > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        Serenity.takeScreenshot();
        workInfoPage.ingresoFechaConstitucion("01011990");
        productInfoPage.selectSexo(list.get(0).get("Sexo"));
        productInfoPage.selectEstadoCivil();
        Serenity.takeScreenshot();
    }

    @Y("el usuario ingresa los datos de domicilio del representate legal")
    public void elUsuarioIngresaLosDatosDeDomicilioDelRepresentateLegal(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        if(constantes.getTipoCliente()=="POTENCIAL"){
            Util.waitFor(10);
        }
        workInfoPage.selectUbigeo(list.get(0).get("Departamento"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div.mt-40 > div.grid.mt-30 > div:nth-child(1) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeo(list.get(0).get("Provincia"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div.mt-40 > div.grid.mt-30 > div:nth-child(2) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeo(list.get(0).get("Distrito"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div.mt-40 > div.grid.mt-30 > div:nth-child(3) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        Serenity.takeScreenshot();
        workInfoPage.selectVia(list.get(0).get("Tipo Via"));
        workInfoPage.getparametros(list.get(0).get("Nombre Via"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(9) > div.col-xs-12.col-8.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametros(list.get(0).get("Numero"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(10) > div:nth-child(1) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        Serenity.takeScreenshot();
        workInfoPage.clickcontinuarnocliente();
    }

    @Y("el usuario completa datos de la empresa")
    public void elUsuarioCompletaDatosDeLaEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        workInfoPage.ingresoTextoRuc(list.get(0).get("RUC"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-company-info-step > div.View.block > cc-company-info-personpj > form > div > div.flex.col.left.search > ibk-inputgroup-text");
        Util.waitFor(10);
        workInfoPage.selectPrincipalCliente();
        workInfoPage.ingresoTexto(list.get(0).get("NUMERO DE PARTIDA REGISTRAL"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-company-info-step > div.View.block > cc-company-info-client > form > div:nth-child(2) > div.flex.col.right.content-link > ibk-inputgroup-text");
        workInfoPage.ingresoTexto(list.get(0).get("EMAIL"),"#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-company-info-step > div.View.block > cc-company-info-client > form > div:nth-child(3) > div.flex.col.left > ibk-inputgroup-text");
        Serenity.takeScreenshot();
        workInfoPage.continuar("#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-company-info-step > div.Step-action.grid.grid-end.mt-30 > div > ibk-button");
        workInfoPage.selectRepresentanteLegal();
    }

    //-----FLUJO FACTORING MLP------- //


    @Y("el representante legal completa sus datos personales basado en {string}")
    public void elRepresentanteLegalCompletaSusDatosPersonalesBasadoEn(String TipoDocumento,DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(2);
        searchPage.completarDatosFactoring(list.get(0).get("Primer Nombre"), "m-hyperloop-inputgroup-text[label='Primer nombre (*)']  .hydrated  .HyperloopInput__input.lowercase.m");
        searchPage.completarDatosFactoring(list.get(0).get("Segundo Nombre"), "m-hyperloop-inputgroup-text[label='Segundo nombre']  .hydrated  .HyperloopInput__input.lowercase.m");
        searchPage.completarDatosFactoring(list.get(0).get("Apellido Paterno"), "m-hyperloop-inputgroup-text[label='Apellido paterno (*)']  .hydrated  .HyperloopInput__input.lowercase.m");
        searchPage.completarDatosFactoring(list.get(0).get("Apellido Materno"), "m-hyperloop-inputgroup-text[label='Apellido materno']  .hydrated  .HyperloopInput__input.lowercase.m");
        searchPage.completarFechaNacimientoFactoring(list.get(0).get("Fecha Nacimiento"));
        searchPage.selectGeneroFactoring(list.get(0).get("Sexo"));
        searchPage.selectMaritalStatusFactoring(list.get(0).get("Estado Civil"));
        Serenity.takeScreenshot();
        if(TipoDocumento.equals("CE")){
            searchPage.completarDatosFactoring(list.get(0).get("Pasaporte"),"m-hyperloop-inputgroup-text[label='Número de pasaporte (*)']  .hydrated  .HyperloopInput__input.m");
            searchPage.selectCountryOfBirth(list.get(0).get("P Nacimiento"));
            searchPage.selectCountryOfNationality(list.get(0).get("P Nacionalidad"));
            Serenity.takeScreenshot();
        }
    }


    @Y("el representante legal completa sus datos de domicilio basado en {string}")
    public void elRepresentanteLegalCompletaSusDatosDeDomicilio(String TipoDocumento,DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);

        workInfoPage.selectUbigeoFactoring(list.get(0).get("Departamento"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div.mt-40 > div.grid.mt-30 > div:nth-child(1) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Provincia"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div.mt-40 > div.grid.mt-30 > div:nth-child(2) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Distrito"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div.mt-40 > div.grid.mt-30 > div:nth-child(3) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        if(TipoDocumento.equals("DNI")) {
            workInfoPage.selectViaFactoringDNI(list.get(0).get("Tipo Via"));
            workInfoPage.getparametrosFactoring(list.get(0).get("Nombre Via"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(9) > div.col-xs-12.col-8.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            workInfoPage.getparametrosFactoring(list.get(0).get("Numero"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(10) > div:nth-child(1) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            Serenity.takeScreenshot();
        }
        else{
            workInfoPage.selectViaFactoringCE(list.get(0).get("Tipo Via"));
            workInfoPage.getparametrosFactoring(list.get(0).get("Nombre Via"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(11) > div.col-xs-12.col-8.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            workInfoPage.getparametrosFactoring(list.get(0).get("Numero"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(12) > div:nth-child(1) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            Serenity.takeScreenshot();
        }
        workInfoPage.clickcontinuarnoclienteFactoring();
        Serenity.takeScreenshot();
    }

    // FLUJO CUENTA CORRIENTE RESERVA MVP
    @Y("el operador ingresa los datos personales del RL1")
    public void elOperadorIngresaLosDatosPersonalesDelRL1(DataTable table){
        List<Map<String, String>> list = table.asMaps(String.class, String.class);

        searchPage.selectDocumentTypeCC(list.get(0).get("Tipo Documento"));
        //searchPage.completeDocNumCCRL1(list.get(0).get("Numero Documento"));

        //agregado MN 14092021
        searchPage.completeDocNumeroCCRL1(list.get(0).get("Numero Documento"));
        Serenity.takeScreenshot();
        //agregado MN 14092021
        //loginPage.completeDatos(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.data-verification-view > form > div:nth-child(2) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.completeDatos(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.data-legal-representative-view > form > div:nth-child(3) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        //agregado MN 14092021
        //loginPage.completeDatos(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.data-verification-view > form > div.grid.mt-40.mt-xs-30.grid-input > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.completeDatos(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.data-legal-representative-view > form > div.grid.mt-40.mt-xs-30.grid-input > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        Serenity.takeScreenshot();
        loginPage.BtnSiguiente();
        Util.waitFor(8);
        productInfoPage.clickModalConfirmacionRLCC();
        Serenity.takeScreenshot();
        Util.waitFor(5);

    }

    @Y("el operador selecciona que no tiene facultades para aperturar cuenta a sola firma")
    public void elOperadorSeleccionaQueNoTieneFacultadesParaAperturarCuentaASolaFirma(){
        loginPage.RepresentanteLegalSinFacultades();
        Serenity.takeScreenshot();
    }

    @Y("el operador selecciona que si tiene facultades para aperturar cuenta a sola firma")
    public void elOperadorSeleccionaQueSiTieneFacultadesParaAperturarCuentaASolaFirma(){
        loginPage.RepresentanteLegalConFacultades();
        Serenity.takeScreenshot();
        Util.waitFor(5);
        loginPage.BtnSiguientePasoFirma();
    }
    @Y("el operador ingresa los datos personales del RL2")
    public void elOperadorIngresaLosDatosPersonalesDelRL2(DataTable table){
        List<Map<String, String>> list = table.asMaps(String.class, String.class);

        searchPage.selectDocumentTypeCC(list.get(0).get("Tipo Documento"));
        searchPage.completeDocNumCCRL2(list.get(0).get("Numero Documento"));
        loginPage.completeDatos(list.get(0).get("Email"), ".HyperloopInput__input.lowercase.m");
        //AGREGADO MN 14092021
        //loginPage.completeDatos(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-verification-step-container > busin-joint-signature-verification-step-ui > section > div.data-verification-view > form > div.ng-dirty.ng-invalid.ng-touched > div.grid.mt-40.mt-xs-30.grid-input > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.completeDatos(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-verification-step-container > busin-joint-signature-verification-step-ui > section > div.joint-signature-verification-view > form > div.ng-invalid.ng-touched.ng-dirty > div.grid.mt-40.mt-xs-30.grid-input > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");

        Serenity.takeScreenshot();
        //loginPage.BtnSiguiente();
        loginPage.BtnSiguientePaso2();
        Util.waitFor(8);
        productInfoPage.clickModalConfirmacionRLCC();
        Util.waitFor(8);
    }
    @Y("el operador no adjunta  certificado de vigencia de poder en PDF del RL1")
    public void elOperadorNoAdjuntaElCertificadoDeVigenciaDePoderEnPDFRL1() {
        Serenity.takeScreenshot();
        loginPage.BtnSiguientePaso3();
        Serenity.takeScreenshot();
        Util.waitFor(5);
        Serenity.takeScreenshot();

    }
    @Y("el operador adjunta el certificado de vigencia de poder en PDF del RL1")
    public void elOperadorAdjuntaElCertificadoDeVigenciaDePoderEnPDFRL1() {

       searchPage.SubirCertificadoCC1("documentos/CertificadoCCPDF.pdf");
       loginPage.BtnSiguiente();
    }
    @Y("el operador adjunta el certificado de vigencia de poder en PDF del RL2")
    public void elOperadorAdjuntaElCertificadoDeVigenciaDePoderEnPDFRL2() {

        searchPage.SubirCertificadoCC2("documentos/CertificadoCCPDF.pdf");
        loginPage.BtnSiguiente();
    }

    @Y("el operador adjunta la copia de estatuto vigente en PDF")
    public void elOperadorAdjuntaLaCopiaDeEstatutoVigenteEnPDF() {

        searchPage.SubirEstatutoCC("documentos/CertificadoCCPDF.pdf");
        Serenity.takeScreenshot();
    }

    @Y("el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente")
    public void elOperadorAdjuntaElCertificadoDeVigenciaDePoderEnPDFparaEmpresaNoCliente() {

        searchPage.SubirCertificadoCCEmpresaNoCliente("documentos/CertificadoCCPDF.pdf");
        Serenity.takeScreenshot();
    }

    @Y("el operador completa los datos personales del RL1")
    public void elOperadorCompletaLosDatosPersonalesDelRL1(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(2);
        loginPage.completeDatos(list.get(0).get("Primer Nombre"), "m-hyperloop-inputgroup-text[label='Primer nombre (*)']  .hydrated  .HyperloopInput__input.lowercase.m");
        loginPage.completeDatos(list.get(0).get("Segundo Nombre"), "m-hyperloop-inputgroup-text[label='Segundo nombre']  .hydrated  .HyperloopInput__input.lowercase.m");
        loginPage.completeDatos(list.get(0).get("Apellido Paterno"), "m-hyperloop-inputgroup-text[label='Apellido paterno (*)']  .hydrated  .HyperloopInput__input.lowercase.m");
        loginPage.completeDatos(list.get(0).get("Apellido Materno"), "m-hyperloop-inputgroup-text[label='Apellido materno']  .hydrated  .HyperloopInput__input.lowercase.m");
        searchPage.completarFechaNacimientoFactoring(list.get(0).get("Fecha Nacimiento"));
        searchPage.selectGeneroCCRL1(list.get(0).get("Sexo"));
        searchPage.selectMaritalStatusCCRL1(list.get(0).get("Estado Civil"));
        searchPage.selectCarrierCCRL1(list.get(0).get("Operador"));
        Serenity.takeScreenshot();

    /*    if(TipoDocumento.equals("CE")){
            searchPage.completarDatosFactoring(list.get(0).get("Pasaporte"),"m-hyperloop-inputgroup-text[label='Número de pasaporte (*)']  .hydrated  .HyperloopInput__input.m");
            searchPage.selectCountryOfBirth(list.get(0).get("P Nacimiento"));
            searchPage.selectCountryOfNationality(list.get(0).get("P Nacionalidad"));
            Serenity.takeScreenshot();
    }*/
   }

    @Y("el operador completa los datos de dirección del RL1")
    public void elOperadorCompletaLosDatosDeDirecciónDelRL1(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);

        workInfoPage.selectUbigeoFactoring(list.get(0).get("Departamento"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div.mt-40 > div.grid.mt-30 > div:nth-child(1) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Provincia"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div.mt-40 > div.grid.mt-30 > div:nth-child(2) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Distrito"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div.mt-40 > div.grid.mt-30 > div:nth-child(3) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectViaCCDNIRL1(list.get(0).get("Tipo Via"));
        workInfoPage.getparametrosFactoring(list.get(0).get("Nombre Via"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div:nth-child(9) > div.col-xs-12.col-8.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametrosFactoring(list.get(0).get("Numero"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div:nth-child(10) > div:nth-child(1) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        Serenity.takeScreenshot();
        loginPage.BtnSiguiente();
        Util.waitFor(5);
        Serenity.takeScreenshot();
    }

    @Y("el operador completa los datos personales del RL2")
    public void elOperadorCompletaLosDatosPersonalesDelRL2(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(1);
        loginPage.completeDatos(list.get(0).get("Primer Nombre"), "m-hyperloop-inputgroup-text[label='Primer nombre (*)']  .hydrated  .HyperloopInput__input.lowercase.m");
        loginPage.completeDatos(list.get(0).get("Segundo Nombre"), "m-hyperloop-inputgroup-text[label='Segundo nombre']  .hydrated  .HyperloopInput__input.lowercase.m");
        loginPage.completeDatos(list.get(0).get("Apellido Paterno"), "m-hyperloop-inputgroup-text[label='Apellido paterno (*)']  .hydrated  .HyperloopInput__input.lowercase.m");
        loginPage.completeDatos(list.get(0).get("Apellido Materno"), "m-hyperloop-inputgroup-text[label='Apellido materno']  .hydrated  .HyperloopInput__input.lowercase.m");
        searchPage.completarFechaNacimientoFactoring(list.get(0).get("Fecha Nacimiento"));
        searchPage.selectGeneroCCRL2(list.get(0).get("Sexo"));
        searchPage.selectMaritalStatusCCRL2(list.get(0).get("Estado Civil"));
        searchPage.selectCarrierCCRL2(list.get(0).get("Operador"));
        Serenity.takeScreenshot();

    /*    if(TipoDocumento.equals("CE")){
            searchPage.completarDatosFactoring(list.get(0).get("Pasaporte"),"m-hyperloop-inputgroup-text[label='Número de pasaporte (*)']  .hydrated  .HyperloopInput__input.m");
            searchPage.selectCountryOfBirth(list.get(0).get("P Nacimiento"));
            searchPage.selectCountryOfNationality(list.get(0).get("P Nacionalidad"));
            Serenity.takeScreenshot();
    }*/
    }

    @Y("el operador completa los datos de dirección del RL2")
    public void elOperadorCompletaLosDatosDeDirecciónDelRL2(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Departamento"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div.mt-40 > div.grid.mt-30 > div:nth-child(1) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Provincia"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div.mt-40 > div.grid.mt-30 > div:nth-child(2) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeoFactoring(list.get(0).get("Distrito"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div.mt-40 > div.grid.mt-30 > div:nth-child(3) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectViaCCDNIRL2(list.get(0).get("Tipo Via"));
        workInfoPage.getparametrosFactoring(list.get(0).get("Nombre Via"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div:nth-child(9) > div.col-xs-12.col-8.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametrosFactoring(list.get(0).get("Numero"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div:nth-child(10) > div:nth-child(1) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        Serenity.takeScreenshot();
        loginPage.BtnSiguiente();

    }

    @Y("el operador completa los datos de la empresa")
    public void elOperadorCompletaLosDatosDeLaEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(3);
        workInfoPage.getparametros(list.get(0).get("Razon Social"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametros(list.get(0).get("Actividad Economica"), "#dropdown-select > div > div > input");
        workInfoPage.ingresoFechaConstitucion ("01011990");
        workInfoPage.selectTipodeContribuyenteCC(list.get(0).get("Tipo de Contribuyente"));
        Serenity.takeScreenshot();

    }

    @Y("el operador completa los datos de dirección de la empresa")
    public void elOperadorCompletaLosDatosDeDirecciónDeLaEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        workInfoPage.selectUbigeo(list.get(0).get("Departamento"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.mt-40 > div > div:nth-child(2) > div > div:nth-child(1) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeo(list.get(0).get("Provincia"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.mt-40 > div > div:nth-child(2) > div > div:nth-child(2) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.selectUbigeo(list.get(0).get("Distrito"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.mt-40 > div > div:nth-child(2) > div > div:nth-child(3) > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        Serenity.takeScreenshot();
        workInfoPage.selectUbigeo(list.get(0).get("Tipo de via"),"body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.mt-40 > div > div:nth-child(3) > div > div.col-4.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select");
        workInfoPage.getparametros(list.get(0).get("Nombre Via"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.mt-40 > div > div:nth-child(3) > div > div.col-8.col-xs-12.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        workInfoPage.getparametros(list.get(0).get("Numero"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.mt-40 > div > div:nth-child(4) > div > div:nth-child(1) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        Serenity.takeScreenshot();
    }

    @Y("el operador ingresa los datos de celular y correo de la empresa")
    public void elOperadorIngresaLosDatosDeCelularYCorreoDeLaEmpres(DataTable table){
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        loginPage.completeDatos(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.mt-40 > div > div.col-xs-12.mt-30.col-6.grid__right > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.completeDatos(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.mt-40 > div > div.col-xs-12.mt-30.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        Serenity.takeScreenshot();
        loginPage.BtnSiguiente();

    }

}
