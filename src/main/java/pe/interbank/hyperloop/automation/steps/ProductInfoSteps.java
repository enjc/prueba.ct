package pe.interbank.hyperloop.automation.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import pe.interbank.hyperloop.automation.ui.assi.LoginPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.AuthenticationPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.ProductInfoPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.SearchPage;
import pe.interbank.hyperloop.automation.utils.Constantes;
import pe.interbank.hyperloop.automation.utils.Util;

import java.util.List;
import java.util.Map;

public class ProductInfoSteps {
    private Constantes constantes;
    @Steps
    ProductInfoPage productInfoPage;
    LoginPage loginPage;
    AuthenticationPage authenticationPage;
    SearchPage searchPage;

    public ProductInfoSteps() {
        constantes = new Constantes();
    }

    @Y("el usuario selecciona un tarifario")
    public void elUsuarioSeleccionaUnTarifario(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        productInfoPage.selectMoneda(list.get(0).get("Moneda"));
        Serenity.takeScreenshot();
        productInfoPage.selectPlan(list.get(0).get("Plan"));
        Serenity.takeScreenshot();
        productInfoPage.selectTerminos();
        Serenity.takeScreenshot();
    }

    @Y("el usuario selecciona ¿A quién le vendes? e ingresa el numero de partida registral")
    public void elUsuarioSeleccionaAQuiénLeVendesEIngresaElNumeroDePartidaRegistral(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(10);
        loginPage.completeDatos(list.get(0).get("Numero Partida Registral"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div:nth-child(4) > div.col-6.col-xs-12.mt-xs-20 > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        productInfoPage.selectVentas(list.get(0).get("Ventas"));
        Serenity.takeScreenshot();
        authenticationPage.siguiente();
    }

    @Y("el usuario ingresa los socios")
    public void elUsuarioingresalossocios (DataTable table){
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(1);
        searchPage.selectDocumentTypeHyper(list.get(0).get("Tipo Documento"));
        searchPage.completeDocNumHyper(list.get(0).get("Numero Documento"));
        //searchPage.selectDocumentTypeHyper(list.get(0).get("Tipo Documento"));
       // searchPage.completeDocNumHyper(list.get(0).get("Numero Documento"));
       // loginPage.completeDatos(list.get(0).get("Numero Partida Registral"), );
        Util.waitFor(10);
        Serenity.takeScreenshot();

    }

    @Y("el usuario selecciona un plan tarifario")
    public void elUsuarioSeleccionaUnPlanTarifario(DataTable table) {
        Util.waitFor(10);
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        constantes.setTipoMoneda(list.get(0).get("Moneda"));
        productInfoPage.selectMonedaHyperloop(list.get(0).get("Moneda"));
        Serenity.takeScreenshot();
        productInfoPage.selectPlanHyperloop(list.get(0).get("Plan"));
        Serenity.takeScreenshot();
        productInfoPage.selectDepartamento(list.get(0).get("Tienda Departamento"));
        productInfoPage.selectProvincia(list.get(0).get("Tienda Distrito"));
        constantes.setTiendaDistrito(list.get(0).get("Tienda Distrito"));

        Serenity.takeScreenshot();
    }

    @Y("el usuario selecciona un seguro")
    public void elUsuarioSeleccionaUnSeguro(DataTable table) {

        productInfoPage.selectTerminosHyperloop();

    }

    @Y("el operador elije la moneda y el envío de estado de cuenta")
    public void elOperadorElijeLaMonedaYElEnvíoDeEstadoDeCuenta(DataTable table) {
        Util.waitFor(8);
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        constantes.setTipoMoneda(list.get(0).get("Moneda"));
        productInfoPage.selectMonedaCC(list.get(0).get("Moneda"));
        Serenity.takeScreenshot();
        constantes.setTipoEnvioEECC(list.get(0).get("Estado de Cuenta"));
        productInfoPage.selectEnvioEECC(list.get(0).get("Estado de Cuenta"));

    }

    @Y("el operador elije la moneda y el envío de estado de cuenta 2")
    public void elOperadorElijeLaMonedaYElEnvíoDeEstadoDeCuenta2(DataTable table) {
        Util.waitFor(8);
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        constantes.setTipoMoneda(list.get(0).get("Moneda"));
        productInfoPage.selectMonedaCC(list.get(0).get("Moneda"));
        Serenity.takeScreenshot();
        constantes.setTipoEnvioEECC(list.get(0).get("Estado de Cuenta"));
        productInfoPage.selectEnvioEECC(list.get(0).get("Estado de Cuenta"));

    }
    @Y("el operador ingresa el correo para recibir el estado de cuenta")
    public void elOperadorIngresaElCorreoParaRecibirElEstadoDeCuenta(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        //agregado mn 15092021
        //loginPage.completeDatos(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.completeDatos(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.account-configuration-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");

    }

    @Y("el operador selecciona la agencia de operaciones mas frecuentes")
    public void elOperadorSeleccionaLaAgenciaDeOperacionesMasFrecuentes(DataTable table){

        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        Util.waitFor(1);
        productInfoPage.selectDepartamentoAgenciaCC(list.get(0).get("Agencia Departamento"));
        Util.waitFor(1);
        productInfoPage.selectProvinciaAgenciaCC(list.get(0).get("Agencia Provincia"));
        productInfoPage.selectTerminosYCondicionesCC();
        Serenity.takeScreenshot();
        productInfoPage.clickSiguienteResumenCreacionCC();
        Util.waitFor(10);
        Serenity.takeScreenshot();

    }

}
