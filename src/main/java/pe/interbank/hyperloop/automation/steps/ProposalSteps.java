package pe.interbank.hyperloop.automation.steps;

import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import io.restassured.http.ContentType;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import pe.interbank.hyperloop.automation.ui.hyperloop.AuthenticationPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.FinalPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.WorkInfoPage;
import pe.interbank.hyperloop.automation.utils.Constantes;
import pe.interbank.hyperloop.automation.utils.Security;
import pe.interbank.hyperloop.automation.utils.Util;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProposalSteps {

    @Steps
    WorkInfoPage workInfoPage;
    AuthenticationPage authenticationPage;
    FinalPage finalPage;
    private static String statusExpediente = "";
    private static String Expediente = "";
    private static String NumeroCuentaSoles = "";
    private static String NumeroCuentaDolares = "";
    private static String NumeroCuentaCciSoles = "";
    private static String NumeroCuentaCciDolares = "";
    private static String resumenCuentaSoles, resumenCuentaDolares, resumenCuentaCciSoles, resumenCuentaCciDolares, resumenLugar;
    private Constantes constantes;
    private static EnvironmentVariables environmentVariables;

    public ProposalSteps() {
        constantes = new Constantes();
    }

    @Entonces("el usuario crea la cuenta negocio")
    public void elUsuarioAperturaLaCuentaNegocio() {
        Serenity.takeScreenshot();
        workInfoPage.continuar("#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-account-setting-step > div.Step-action.grid.grid-end.mt-30 > div.col.col-end > ibk-button");
    }

    @Y("el usuario verifica que la cuenta se genero exitosamente")
    public void elUsuarioVerificaQueLaCreacionSeaExitosa() {
        Util.waitFor(10);
        Serenity.takeScreenshot();
        Expediente = finalPage.resumenExpediente().substring(10,16);
        statusExpediente = Util.consultaExpediente(Expediente);
        assertThat(statusExpediente, equalTo("CERRADO"));
    }

    @Entonces("el usuario abre la cuenta negocio")
    public void elUsuarioAbreLaCuentaNegocio() {
        authenticationPage.siguienteConfigurationStep();
        Serenity.takeScreenshot();
        Util.waitFor(5);
        //authenticationPage.switchTo();
        Serenity.takeScreenshot();
    }

    @Y("el usuario verifica que la cuenta negocio se genero exitosamente")
    public void elUsuarioVerificaQueLaCuentaNegocioSeGeneroExitosamente() {
        //PRIMERA VALIDACION - EXPEDIENTE
        Util.waitFor(10);

        finalPage.resumenDetalleCuenta();
        Util.waitFor(10);
        statusExpediente = Util.consultaExpedienteHyperloop(constantes.getNumeroDocumento());

        assertThat(statusExpediente, equalTo("CERRADO"));

        System.out.println(constantes.getTiendaDistrito());

        //SEGUNDA VALIDACION - APERTURA CUENTA DISTRITO
        switch (constantes.getTiendaDistrito()) {
            case "Huanuco":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("561"));
                assertThat(NumeroCuentaCciSoles, equalTo("561"));
                break;
            case "Trujillo":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("600"));
                assertThat(NumeroCuentaCciSoles, equalTo("600"));
                break;
            case "Tacna":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("342"));
                assertThat(NumeroCuentaCciSoles, equalTo("342"));
                break;
            case "Arequipa":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaDolares = finalPage.resumenCuentaDolares().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);
                NumeroCuentaCciDolares= finalPage.resumenCuentaCCIDolares().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("300"));
                assertThat(NumeroCuentaDolares, equalTo("300"));
                assertThat(NumeroCuentaCciSoles, equalTo("300"));
                assertThat(NumeroCuentaCciDolares, equalTo("300"));
                break;
            case "Tumbes":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("731"));
                assertThat(NumeroCuentaCciSoles, equalTo("731"));
                break;
            case "Pasco":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("590"));
                assertThat(NumeroCuentaCciSoles, equalTo("590"));
                break;
            case "Piura":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("720"));
                assertThat(NumeroCuentaCciSoles, equalTo("720"));
                break;
            case "Huaraz":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("622"));
                assertThat(NumeroCuentaCciSoles, equalTo("622"));
                break;
            case "Zarumilla":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaDolares = finalPage.resumenCuentaDolares().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);
                NumeroCuentaCciDolares= finalPage.resumenCuentaCCIDolares().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("771"));
                assertThat(NumeroCuentaDolares, equalTo("771"));
                assertThat(NumeroCuentaCciSoles, equalTo("771"));
                assertThat(NumeroCuentaCciDolares, equalTo("771"));
                break;
            case "Ilo":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("341"));
                assertThat(NumeroCuentaCciSoles, equalTo("341"));
                break;
            case "Paita":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaDolares = finalPage.resumenCuentaDolares().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);
                NumeroCuentaCciDolares= finalPage.resumenCuentaCCIDolares().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("736"));
                assertThat(NumeroCuentaDolares, equalTo("736"));
                assertThat(NumeroCuentaCciSoles, equalTo("736"));
                assertThat(NumeroCuentaCciDolares, equalTo("736"));
                break;
            case "Santa":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaDolares = finalPage.resumenCuentaDolares().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);
                NumeroCuentaCciDolares= finalPage.resumenCuentaCCIDolares().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("620"));
                assertThat(NumeroCuentaDolares, equalTo("620"));
                assertThat(NumeroCuentaCciSoles, equalTo("620"));
                assertThat(NumeroCuentaCciDolares, equalTo("620"));
                break;
            case "Pucallpa":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("760"));
                assertThat(NumeroCuentaCciSoles, equalTo("760"));
                break;
            case "Sullana":
                NumeroCuentaSoles = finalPage.resumenCuentaSoles().substring(0,3);
                NumeroCuentaCciSoles = finalPage.resumenCuentaCCISoles().substring(8,11);

                assertThat(NumeroCuentaSoles, equalTo("732"));
                assertThat(NumeroCuentaCciSoles, equalTo("732"));
                break;
        }

        System.out.println(constantes.getTipoDocumento());
        System.out.println(constantes.getNumeroDocumento());

        //TERCERA VALIDACION - SERVICIO(CONSULTAR CUENTAS)
        SerenityRest.given()
                .contentType(ContentType.JSON)
                .header("Authorization", Security.createAccessToken())
                .header("application", "UXRF")
                .header("company", "INTERBANK")
                .header("ocp-apim-subscription-key", "c9664a560c184e8cb857e5d2a7efabc4")
                .header("storecode", "898")
                .header("user", "S7302")
                .when()
                .pathParam("identityDocumentType",constantes.getTipoDocumento())
                .pathParam("identityDocumentNumber", constantes.getNumeroDocumento())
                .get("https://eu2-ibk-apm-uat-ext-001.azure-api.net/vision-productos/uat/account?identityDocumentType={identityDocumentType}&identityDocumentNumber={identityDocumentNumber}");

        restAssuredThat(response -> response.statusCode(200));

        /*
        if(constantes.getTipoMoneda().equals("AMBOS")){
            resumenCuentaSoles      = SerenityRest.lastResponse().body().path("accounts.accountNumber");
            resumenCuentaCciSoles   = SerenityRest.lastResponse().body().path("accounts.cci");
            resumenCuentaDolares    = SerenityRest.lastResponse().body().path("accounts.accountNumber");
            resumenCuentaCciDolares = SerenityRest.lastResponse().body().path("accounts.cci");
            resumenLugar            = SerenityRest.lastResponse().body().path("accounts.finantialInstitution");

            assertThat(resumenCuentaSoles, equalTo(finalPage.resumenCuentaSoles()));
            assertThat(resumenCuentaCciSoles, equalTo(finalPage.resumenCuentaCCISoles()));
            assertThat(resumenCuentaDolares, equalTo(finalPage.resumenCuentaDolares()));
            assertThat(resumenCuentaCciDolares, equalTo(finalPage.resumenCuentaCCIDolares()));
            assertThat(resumenLugar, equalTo(finalPage.resumenLugarBimoneda()));

        }else{
            if(constantes.getTipoMoneda().equals(" SOLES ")){
                resumenCuentaSoles      = SerenityRest.lastResponse().body().path("accounts.accountNumber");
                resumenCuentaCciSoles   = SerenityRest.lastResponse().body().path("accounts.cci");
                resumenLugar            = SerenityRest.lastResponse().body().path("accounts.finantialInstitution");

                assertThat(resumenCuentaSoles, equalTo(finalPage.resumenCuentaSoles()));
                assertThat(resumenCuentaCciSoles, equalTo(finalPage.resumenCuentaCCISoles()));
                assertThat(resumenLugar, equalTo(finalPage.resumenLugar()));

            }else{
                if(constantes.getTipoMoneda().equals(" DOLARES ")) {
                    resumenCuentaDolares = SerenityRest.lastResponse().body().path("accounts.accountNumber");
                    resumenCuentaCciDolares = SerenityRest.lastResponse().body().path("accounts.cci");
                    resumenLugar = SerenityRest.lastResponse().body().path("accounts.finantialInstitution");

                    assertThat(resumenCuentaDolares, equalTo(finalPage.resumenCuentaDolares()));
                    assertThat(resumenCuentaCciDolares, equalTo(finalPage.resumenCuentaCCIDolares()));
                    assertThat(resumenLugar, equalTo(finalPage.resumenLugar()));
                }
            }
        }
        */

        //authenticationPage.clickTerminar();
    }

    @Y("el operador realiza la solicitud para la apertura de cuenta corriente")
    public void elOperadorRealizaLaSolicitudParaLaAperturaDeCuentaCorriente() {

        Util.waitFor(5);
        Serenity.takeScreenshot();
        workInfoPage.clickCopiarEnlace();
        Util.waitFor(3);
    }

}
