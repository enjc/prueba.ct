package pe.interbank.hyperloop.automation.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Screenshots;
import net.thucydides.core.annotations.Steps;
import pe.interbank.hyperloop.automation.ui.assi.LoginPage;
import pe.interbank.hyperloop.automation.ui.hyperloop.SearchPage;
import pe.interbank.hyperloop.automation.utils.Constantes;
import pe.interbank.hyperloop.automation.utils.Util;

import java.util.List;
import java.util.Map;

public class SearchPersonSteps {
    private Constantes constantes;
    @Steps
    SearchPage searchPage;
    LoginPage loginPage;

    public SearchPersonSteps() {
        constantes = new Constantes();
    }


    @Y("ingresa el tipo de documento y numero de documento de una (.*)$")
    public void ingresaElTipoDeDocumentoYNúmeroDeDocumentoDelCliente(String tipo, DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        constantes.setTipoPersona(tipo);
        searchPage.selectDocumentType(list.get(0).get("Tipo Documento"));
        searchPage.completeDocNum(list.get(0).get("Numero Documento"));
        Serenity.takeScreenshot();
        searchPage.clickInicaSesion();
    }

    @Cuando("selecciona abrir una cuenta negocio")
    public void seleccionaAbrirUnaCuentaNegocio() {
        if(constantes.getTipoPersona().equals("PERSONA JURIDICA")){
            searchPage.clickProduct();
            Serenity.takeScreenshot();
            searchPage.clickAbrirCuenta();
        }else{
            searchPage.products("Negocios");
            Util.waitFor(2);
            //searchPage.swipe();
            searchPage.clickProduct();
            Serenity.takeScreenshot();
            searchPage.selectPersonaNatural();
            Serenity.takeScreenshot();
            searchPage.clickAbrirCuenta();
            //searchPage.clicResidenciaFiscal();
        }

        Serenity.takeScreenshot();
    }

    @Cuando("realiza la solicitud test")
    public void validaResidenciaConstitucion() {

        Serenity.takeScreenshot();
    }

    @Cuando("realiza la solicitud")
    public void realizaLaSolicitud() {
        loginPage.SelectResidenciaFiscal();
        Serenity.takeScreenshot();
    }

    @Y("el usuario de tipo (.*) ingresa sus datos$")
    public void elUsuarioDeTipoCLIENTEIngresaSusDatos(String tipo, DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        constantes.setTipoCliente(tipo);
        constantes.setTipoEmpresa(list.get(0).get("Empresa"));
        loginPage.selectEmpresa(list.get(0).get("Empresa"));

        if(list.get(0).get("Empresa").equals("CONSTITUIDA")) {

            constantes.setTipoDocumento("RUC");
            constantes.setNumeroDocumento(list.get(0).get("RUC"));
            loginPage.completeDatos(list.get(0).get("RUC"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-30 > div > div.col-6.data-verification-step-spinner > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.buscarRuc();
            Serenity.takeScreenshot();
            searchPage.selectDocumentTypeHyper(list.get(0).get("Tipo Documento"));
            searchPage.completeDocNumHyper(list.get(0).get("Numero Documento"));
            //loginPage.completeDatosEmail(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div:nth-child(6) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.completeDatos(list.get(0).get("Email"), ".lowercase");
            loginPage.selectOperador(list.get(0).get("Operador"));
            loginPage.completeDatos(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-40.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.SelectTerminos();
            //loginPage.SelectCaptcha();
            //loginPage.SelectCaptcha();
            Serenity.takeScreenshot();
        }else{

            constantes.setTipoDocumento(list.get(0).get("Tipo Documento"));
            constantes.setNumeroDocumento(list.get(0).get("Numero Documento"));
            Serenity.takeScreenshot();

            searchPage.selectDocumentTypeHyper(list.get(0).get("Tipo Documento"));
            searchPage.completeDocNumHyper(list.get(0).get("Numero Documento"));
            loginPage.completeDatos(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div:nth-child(5) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.selectOperador(list.get(0).get("Operador"));
            loginPage.completeDatos(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-40.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
            loginPage.SelectTerminos();
            //loginPage.SelectCaptcha();
            //loginPage.SelectCaptcha();
            Serenity.takeScreenshot();
        }
        loginPage.BtnSiguiente();
    }

    //FLUJO FACTORING MLP

    @Cuando("el usuario completa los datos del representante legal {string}, {string}")
    @Screenshots(forEachAction=true)
    public void elUsuarioCompletaLosDatosDelRepresentanteLegalDeTipoNOCLIENTE(String TipoDocumento,String NumeroDocumento, DataTable table) {
        Util.waitFor(10);
        List<Map<String, String>> list = table.asMaps(String.class, String.class);

        searchPage.selectDocumentTypeFactoring(TipoDocumento);
        searchPage.completeDocNumFactoring(NumeroDocumento);
        searchPage.completarDatosFactoring(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-step > fact-legal-representative-step-ui > section > div.legal-representative-view > form > div:nth-child(2) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        Serenity.takeScreenshot();
        searchPage.selectCarrierFactoring(list.get(0).get("Operador"));
        searchPage.completarDatosFactoring(list.get(0).get("Celular"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-step > fact-legal-representative-step-ui > section > div.legal-representative-view > form > div.grid.mt-40.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");

        searchPage.SelectTerminosFactoring();
        searchPage.SelectCaptchaFactoring();
        searchPage.BtnSiguiente();

    }


    @Y("el representante legal acepta LDPD")
    public void elRepresentanteLegalAceptaLDPD() {
        searchPage.SelectLDPDFactoring();
    }

    //FLUJO CUENTA CORRIENTE RESERVA MVP

    @Cuando("ingresa a la pagina a realizar la solicitud")
    public void ingresaALaPaginaARealizarLaSolicitud() {
        loginPage.ResidenciaFiscal();
        Serenity.takeScreenshot();
    }

    @Y("el operador ingresa los datos de la empresa")
    public void elOperadorIngresaLosDatosDeLaEmpresa(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        constantes.setTipoDocumento("RUC");
        constantes.setNumeroDocumento(list.get(0).get("RUC"));

        loginPage.completeDatos(list.get(0).get("RUC"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-30 > div > div.col-6.col-xs-12.data-verification-step-spinner > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.ValidarRuc();
        Serenity.takeScreenshot();
        Util.waitFor(5);
    }

    @Y("el operador ingresa sus datos personales")
    public void elOperadorIngresaSusDatosPersonales(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);

        loginPage.SelectConfirmar();
        searchPage.selectDocumentTypeCC(list.get(0).get("Tipo Documento"));
        searchPage.completeDocNumCC(list.get(0).get("Numero Documento"));
        loginPage.completeDatos(list.get(0).get("Nombres"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-40.mt-xs-30.grid-input > div.col-xs-12.col-6.grid__right.mb-xs-30 > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.completeDatos(list.get(0).get("Apellidos"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-40.mt-xs-30.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.completeDatos(list.get(0).get("Email"), "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div:nth-child(6) > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input");
        loginPage.SelectTerminos();
        loginPage.SelectCaptcha();
        loginPage.SelectCaptcha();
        Serenity.takeScreenshot();
        loginPage.BtnSiguiente();
    }


}
