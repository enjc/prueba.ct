package pe.interbank.hyperloop.automation.ui.assi;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.hyperloop.automation.utils.Util;

public class LoginPage extends PageObject {

    @FindBy(css = "#recaptcha > div > div > iframe")
    WebElement cbRecaptchaIframe;
    //
    @FindBy(css = "#recaptcha-anchor")
    WebElement cbRecaptchaAnchor;


    public void typeUserName(String userName) {
        Util.waitFor(4);
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("app-custom-input[id='username'] ibk-inputgroup-text")))
                .findElement(By.cssSelector("ibk-input")))
                .findElement(By.cssSelector("div input")).sendKeys(userName);
    }

    public void typePassword(String password) {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("app-custom-input[id='password'] ibk-inputgroup-text")))
                .findElement(By.cssSelector("ibk-input")))
                .findElement(By.cssSelector("div input")).sendKeys(password);
    }

    public void clickLoginButton() {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("app-login ibk-button")))
                .findElement(By.cssSelector("button ibk-font")))
                .findElement(By.cssSelector("p")).click();
    }

    public void SelectResidenciaFiscal() {
        Util.waitFor(5);
        //((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.tabInputGroup.mt-20 > div.tabInputGroup__content > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-step1-0').click()");

        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('form m-hyperloop-inputgroup-radio').shadowRoot.querySelector('label[for=\"styled-radio-step1-0\"]').click()");

    }

    public void selectEmpresa(String empresa) {
        Util.waitFor(1);
        if(empresa.equals("CONSTITUIDA")){
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.tabInputGroup__content > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-step1-0').click()");
        }else{
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.tabInputGroup__content > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-step1-1').click()");
        }
    }

    public void completeDatos(String parametro,String xpath) {
        Util.waitFor(2);
        int c = 0;
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector(xpath)).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void completeDatosEmail(String parametro,String xpath) {
        Util.waitFor(2);
        getDriver().findElement(By.cssSelector(".lowercase")).sendKeys(parametro);

    }



    public void buscarRuc() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-30 > div > div.col-5.btn-verficar-ruc.ml-10 > a-hyperloop-button > button').click()");
    }

    public void selectOperador(String operador) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-40.grid-input > div.col-xs-12.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+operador+"').click()");
    }

    public void SelectTerminos() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-pdp')" +
                ".shadowRoot.querySelector('#styled-checkbox-pdp').click()");
        //Util.waitFor(1);
        //((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-lpdp')" +
        //        ".shadowRoot.querySelector('#styled-checkbox-lpdp').click()");
    }
    public void SelectConfirmar() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-limit')" +
                ".shadowRoot.querySelector('div > label').click()");
        //Util.waitFor(1);
        //((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-lpdp')" +
        //        ".shadowRoot.querySelector('#styled-checkbox-lpdp').click()");
    }

    public void SelectCaptcha() {
        Util.scrollToElementVisible(cbRecaptchaIframe, "0");
        getDriver().switchTo().frame(cbRecaptchaIframe);
        cbRecaptchaAnchor.click();
        Util.waitFor(1);
        getDriver().switchTo().parentFrame();
    }

    public void BtnSiguiente() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);

        // agregado 14092021
       //((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-step__wrapper-button.grid.content-center > a-hyperloop-button > button > span').click()");
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.data-legal-representative-step__wrapper-button > a-hyperloop-button > button').click()");

    }

    public void BtnSiguientePaso2() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);

        // agregado MN 14092021
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-verification-step-container > busin-joint-signature-verification-step-ui > section > div.joint-signature-verification-step__wrapper-button > a-hyperloop-button > button > span').click()");

    }

    public void BtnSiguientePaso3() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);

        // agregado MN 15092021
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-step-container > busin-data-company-step-ui > section > div.data-company-step__wrapper-button > a-hyperloop-button > button > span').click()");

    }
    public void BtnSiguientePasoFirma() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);

        // agregado 14092021
        //((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-step__wrapper-button.grid.content-center > a-hyperloop-button > button > span').click()");
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-verification-step-container > busin-joint-signature-verification-step-ui > section > div.joint-signature-verification-step__wrapper-button > a-hyperloop-button > button').click()");

    }
    public void Comencemos() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);

        ((JavascriptExecutor)getDriver()).executeScript("return  document.querySelector('busin-data-verification-step-ui div.data-verification-step__wrapper-button button').click()");

    }

    public void ResidenciaFiscal() {
        Util.waitFor(10);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('a-hyperloop-tab')" +
                ".shadowRoot.querySelector('div > button:nth-child(1)').click()");
    }

    public void ValidarRuc() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-30 > div > div.btn-verficar-ruc.col-5.ml-10.mt-xs-30 > a-hyperloop-button > button').click()");
    }

    public void RepresentanteLegalSinFacultades() {
        Util.waitFor(3);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('a-hyperloop-tab')" +
                ".shadowRoot.querySelector('div > button:nth-of-type(2)').click()");
    }

    public void RepresentanteLegalConFacultades() {
        Util.waitFor(3);
        //agregado 15092021
        //((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('a-hyperloop-tab')" +
             //   ".shadowRoot.querySelector('div > button:nth-of-type(1)').click()");
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('busin-joint-signature-verification-step-ui a-hyperloop-tab').shadowRoot.querySelector('div button:nth-child(1)').click()");

    }

    public void BtnSiguienteEmpresaNoCliente() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('.hydrated > .hyperloop-button.hyperloop-button__default.hyperloop-button__m.hyperloop-button__primary.hyperloop-button__primary--m').click()");
    }

}
