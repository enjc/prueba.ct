package pe.interbank.hyperloop.automation.ui.hyperloop;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;
import pe.interbank.hyperloop.automation.utils.Util;

public class AuthenticationFactoringMlpPage extends PageObject {
    public void clickPrimeraRespuesta() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-authentication-equifax > fact-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#only-label').click()");
    }

    public void clickSegundaRespuesta() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-authentication-equifax > fact-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#only-label').click()");
    }

    public void clickTerceraRespuesta() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-authentication-equifax > fact-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#only-label').click()");
    }

    public void clickContinuarEquifaxFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-authentication-equifax > fact-authentication-equifax-ui > section > div.authentication-equifax-step__wrapper-button.grid-xs-reverse-col > a-hyperloop-button:nth-child(2) > button').click()");
    }

    public void clickEstosDatosNoSonMios() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#li-terms-conditions').click()");
    }

}
