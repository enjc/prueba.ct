package pe.interbank.hyperloop.automation.ui.hyperloop;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.hyperloop.automation.utils.Util;


public class AuthenticationPage extends PageObject {
    @FindBy(css = "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(1) > div > a-hyperloop-paragraph > p")
    WebElement txtPrimeraPregunta;

    @FindBy(css = "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(3) > div > a-hyperloop-paragraph > p")
    WebElement txtSegundaPregunta;

    @FindBy(css = "body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(5) > div > a-hyperloop-paragraph > p")
    WebElement txtTerceraPregunta;

    public void SelectEquifax() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#li-terms-conditions').click()");
    }

    public void validarEquifax(String rpt1, String rpt2, String rpt3, String rpt4, String rpt5, String rpt6, String rpt7, String rpt8, String rpt9, String rpt10, String rpt11, String rpt12, String rpt13, String rpt14) {
       // if (txtbloqueado.isVisible()){
       //     Serenity.takeScreenshot();
       //     Assert.fail("Tu DNI "+ constantes.getDni()  +" se encuentra bloqueado por una semana!");
       // }else{
            //Thread.sleep(2000);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();

        EvaluaPrimeraPregunta(rpt1, rpt2, rpt3, rpt4, rpt5, rpt6, rpt7, rpt8, rpt9, rpt10, rpt11, rpt12, rpt13, rpt14);
        Serenity.takeScreenshot();
        js.executeScript("javascript:window.scrollBy(350,450)");

        EvaluaSegundaPregunta(rpt1, rpt2, rpt3, rpt4, rpt5, rpt6, rpt7, rpt8, rpt9, rpt10, rpt11, rpt12, rpt13, rpt14);
        Serenity.takeScreenshot();
        js.executeScript("javascript:window.scrollBy(350,450)");

        EvaluaTerceraPregunta(rpt1, rpt2, rpt3, rpt4, rpt5, rpt6, rpt7, rpt8, rpt9, rpt10, rpt11, rpt12, rpt13, rpt14);
        Serenity.takeScreenshot();
        js.executeScript("javascript:window.scrollBy(350,450)");

    }

    private void EvaluaTerceraPregunta(String rpt1, String rpt2, String rpt3, String rpt4, String rpt5, String rpt6, String rpt7, String rpt8, String rpt9, String rpt10, String rpt11, String rpt12, String rpt13, String rpt14) {
        switch (txtTerceraPregunta.getText()) {
            case "3. ¿Tiene o ha tenido una tarjeta de crédito activa a tu nombre (entre julio-2020 y septiembre-2020)?":
                validaTerceraRespuesta(rpt1);
                break;
            case "3. ¿Cuál es el nombre de su padre/madre?":
                validaTerceraRespuesta(rpt2);
                break;
            case "3. ¿Cuál es el nombre de su hijo/hija?":
                validaTerceraRespuesta(rpt3);
                break;
            case "3. ¿Cuál es el nombre de su hermano/hermana?":
                validaTerceraRespuesta(rpt4);
                break;
            case "3. ¿Cuál es el nombre de su abuelo/abuela materna/paterno?":
                validaTerceraRespuesta(rpt5);
                break;
            case "3. ¿Tiene o ha tenido un crédito hipotecario (entre julio-2020 y septiembre-2020)?":
                validaTerceraRespuesta(rpt6);
                break;
            case "3. ¿Está o has estado casado(a) con alguna de la siguientes personas?":
                validaTerceraRespuesta(rpt7);
                break;
            case "3. ¿Tiene o ha tenido crédito vehicular (entre julio-2020 y septiembre-2020)?":
                validaTerceraRespuesta(rpt8);
                break;
            case "3. ¿Tiene o ha tenido un préstamo personal (entre julio-2020 y septiembre-2020)?":
                validaTerceraRespuesta(rpt9);
                break;
            case "3. ¿Tiene o ha tenido algún carro de los siguientes modelos?":
                validaTerceraRespuesta(rpt10);
                break;
            case "3. ¿La empresa a la que representa fue inscrita en lima o provincia?":
                validaTerceraRespuesta(rpt11);
                break;
            case "3. ¿Es o ha sido representante legal de alguna de estas empresas?":
                validaTerceraRespuesta(rpt12);
                break;
            case "3. ¿Cuál de las siguientes opciones es o ha sido su dirección de correspondencia?":
                validaTerceraRespuesta(rpt13);
                break;
            case "3. ¿En cuál de las siguientes empresas trabaja o ha trabajado directamente?":
                validaTerceraRespuesta(rpt14);
                break;
        }
    }

    private void validaTerceraRespuesta(String respuesta) {
        if (thirdAnswerRB1().equals(respuesta)) {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-2-0').click()");
        } else {
            if (thirdAnswerRB2().equals(respuesta)){
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-2-1').click()");
            }else{
                if (thirdAnswerRB3().equals(respuesta)) {
                    ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-2-2').click()");
                } else {
                    if (thirdAnswerRB4().equals(respuesta)) {
                        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-2-3').click()");
                    }else{
                        if (thirdAnswerRB5().equals(respuesta)) {
                            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-2-4').click()");
                        }
                    }
                }
            }
        }
    }
    private void validaTerceraRespuestaEspecial(String respuesta) {
        if (thirdAnswerRB1().equals(respuesta)) {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-2-0').click()");
        } else {
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-2-1').click()");
        }
    }

    private void EvaluaSegundaPregunta(String rpt1, String rpt2, String rpt3, String rpt4, String rpt5, String rpt6, String rpt7, String rpt8, String rpt9, String rpt10, String rpt11, String rpt12, String rpt13, String rpt14) {
        switch (txtSegundaPregunta.getText()) {
            case "2. ¿Tiene o ha tenido una tarjeta de crédito activa a tu nombre (entre julio-2020 y septiembre-2020)?":
                validaSegundaRespuesta(rpt1);
                break;
            case "2. ¿Cuál es el nombre de su padre/madre?":
                validaSegundaRespuesta(rpt2);
                break;
            case "2. ¿Cuál es el nombre de su hijo/hija?":
                validaSegundaRespuesta(rpt3);
                break;
            case "2. ¿Cuál es el nombre de su hermano/hermana?":
                validaSegundaRespuesta(rpt4);
                break;
            case "2. ¿Cuál es el nombre de su abuelo/abuela materna/paterno?":
                validaSegundaRespuesta(rpt5);
                break;
            case "2. ¿Tiene o ha tenido un crédito hipotecario (entre julio-2020 y septiembre-2020)?":
                validaSegundaRespuestaEspecial(rpt6);
                break;
            case "2. ¿Está o has estado casado(a) con alguna de la siguientes personas?":
                validaSegundaRespuesta(rpt7);
                break;
            case "2. ¿Tiene o ha tenido crédito vehicular (entre julio-2020 y septiembre-2020)?":
                validaSegundaRespuestaEspecial(rpt8);
                break;
            case "2. ¿Tiene o ha tenido un préstamo personal (entre julio-2020 y septiembre-2020)?":
                validaSegundaRespuestaEspecial(rpt9);
                break;
            case "2. ¿Tiene o ha tenido algún carro de los siguientes modelos?":
                validaSegundaRespuesta(rpt10);
                break;
            case "2. ¿La empresa a la que representa fue inscrita en lima o provincia?":
                validaSegundaRespuesta(rpt11);
                break;
            case "2. ¿Es o ha sido representante legal de alguna de estas empresas?":
                validaSegundaRespuesta(rpt12);
                break;
            case "2. ¿Cuál de las siguientes opciones es o ha sido su dirección de correspondencia?":
                validaSegundaRespuesta(rpt13);
                break;
            case "2. ¿En cuál de las siguientes empresas trabaja o ha trabajado directamente?":
                validaSegundaRespuesta(rpt14);
                break;
        }
    }

    private void validaSegundaRespuesta(String respuesta) {
        if (secondAnswerRB1().equals(respuesta)) {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-1-0').click()");
        } else {
            if (secondAnswerRB2().equals(respuesta)){
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-1-1').click()");
            }else{
                if (secondAnswerRB3().equals(respuesta)) {
                    ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-1-2').click()");
                } else {
                    if (secondAnswerRB4().equals(respuesta)) {
                        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-1-3').click()");
                    }else{
                        if (secondAnswerRB5().equals(respuesta)) {
                            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-1-4').click()");
                        }
                    }
                }
            }
        }
    }
    private void validaSegundaRespuestaEspecial(String respuesta) {
        if (secondAnswerRB1().equals(respuesta)) {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-1-0').click()");
        } else {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-1-1').click()");
        }
    }
    void EvaluaPrimeraPregunta(String rpt1, String rpt2, String rpt3, String rpt4, String rpt5,String rpt6, String rpt7, String rpt8, String rpt9, String rpt10, String rpt11, String rpt12, String rpt13, String rpt14){
        switch (txtPrimeraPregunta.getText()) {
            case "1. ¿Tiene o ha tenido una tarjeta de crédito activa a tu nombre (entre julio-2020 y septiembre-2020)?":
                validaPrimeraRespuesta(rpt1);
                break;
            case "1. ¿Cuál es el nombre de su padre/madre?":
                validaPrimeraRespuesta(rpt2);
                break;
            case "1. ¿Cuál es el nombre de su hijo/hija?":
                validaPrimeraRespuesta(rpt3);
                break;
            case "1. ¿Cuál es el nombre de su hermano/hermana?":
                validaPrimeraRespuesta(rpt4);
                break;
            case "1. ¿Cuál es el nombre de su abuelo/abuela materna/paterno?":
                validaPrimeraRespuesta(rpt5);
                break;
            case "1. ¿Tiene o ha tenido un crédito hipotecario (entre julio-2020 y septiembre-2020)?":
                validaPrimeraRespuestaEspecial(rpt6);
                break;
            case "1. ¿Está o has estado casado(a) con alguna de la siguientes personas?":
                validaPrimeraRespuesta(rpt7);
                break;
            case "1. ¿Tiene o ha tenido crédito vehicular (entre julio-2020 y septiembre-2020)?":
                validaPrimeraRespuestaEspecial(rpt8);
                break;
            case "1. ¿Tiene o ha tenido un préstamo personal (entre julio-2020 y septiembre-2020)?":
                validaPrimeraRespuestaEspecial(rpt9);
                break;
            case "1. ¿Tiene o ha tenido algún carro de los siguientes modelos?":
                validaPrimeraRespuesta(rpt10);
                break;
            case "1. ¿La empresa a la que representa fue inscrita en lima o provincia?":
                validaPrimeraRespuesta(rpt11);
                break;
            case "1. ¿Es o ha sido representante legal de alguna de estas empresas?":
                validaPrimeraRespuesta(rpt12);
                break;
            case "1. ¿Cuál de las siguientes opciones es o ha sido su dirección de correspondencia?":
                validaPrimeraRespuesta(rpt13);
                break;
            case "1. ¿En cuál de las siguientes empresas trabaja o ha trabajado directamente?":
                validaPrimeraRespuesta(rpt14);
                break;
        }
    }


    public void continuarEquifax() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.grid.grid-end.mt-50.mt-xs-40.text-xs-center > a-hyperloop-button > button').click()");
    }

    public void siguiente() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.content-client-data-step__wrapper-button > a-hyperloop-button > button').click()");
    }
    public void siguienteConfigurationStep() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.content-account-configuration-step__wrapper-button.grid > div > a-hyperloop-button > button > span').click()");
    }

    void validaPrimeraRespuesta(String respuesta){
        if (firstAnswerRB1().equals(respuesta)) {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-0-0').click()");
        } else {
            if (firstAnswerRB2().equals(respuesta)){
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-0-1').click()");
            }else{
                if (firstAnswerRB3().equals(respuesta)) {
                    ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-0-2').click()");
                } else {
                    if (firstAnswerRB4().equals(respuesta)) {
                        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-0-3').click()");
                    }else{
                        if (firstAnswerRB5().equals(respuesta)) {
                            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-0-4').click()");
                        }
                    }
                }
            }
        }
    }
    void validaPrimeraRespuestaEspecial(String respuesta){
        if (firstAnswerRB1().equals(respuesta)) {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-0-0').click()");
        } else {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio').shadowRoot.querySelector('#styled-radio-equifax-0-1').click()");
        }
    }

    public String firstAnswerRB1() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-0-0]').innerText");
        return (String) objText;
    }
    public String firstAnswerRB2() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-0-1]').innerText");
        return (String) objText;
    }
    public String firstAnswerRB3() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-0-2]').innerText");
        return (String) objText;
    }
    public String firstAnswerRB4() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-0-3]').innerText");
        return (String) objText;
    }
    public String firstAnswerRB5() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(2) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-0-4]').innerText");
        return (String) objText;
    }
    public String secondAnswerRB1() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-1-0]').innerText");
        return (String) objText;
    }
    public String secondAnswerRB2() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-1-1]').innerText");
        return (String) objText;
    }
    public String secondAnswerRB3() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-1-2]').innerText");
        return (String) objText;
    }
    public String secondAnswerRB4() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-1-3]').innerText");
        return (String) objText;
    }
    public String secondAnswerRB5() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(4) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-1-4]').innerText");
        return (String) objText;
    }
    public String thirdAnswerRB1() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-2-0]').innerText");
        return (String) objText;
    }
    public String thirdAnswerRB2() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-2-1]').innerText");
        return (String) objText;
    }
    public String thirdAnswerRB3() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-2-2]').innerText");
        return (String) objText;
    }
    public String thirdAnswerRB4() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-2-3]').innerText");
        return (String) objText;
    }
    public String thirdAnswerRB5() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-authentication-equifax > busin-authentication-equifax-ui > section > div.authentication-equifax-view > form > div > div:nth-child(6) > m-hyperloop-inputgroup-radio')" +
                ".shadowRoot.querySelector('label[for=styled-radio-equifax-2-4]').innerText");
        return (String) objText;
    }

    public void switchTo() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(600,600)");
    }

    public void clickTerminar() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-summary-step > busin-summary-step-ui > section > div.content_button.mt-30 > a-hyperloop-button > button').click()");

    }
}
