package pe.interbank.hyperloop.automation.ui.hyperloop;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pe.interbank.hyperloop.automation.utils.Util;
import org.openqa.selenium.Alert;

import java.util.concurrent.TimeUnit;

public class AutorizacionPage extends PageObject {

    static String url;
    @FindBy(css = "iframe[role='presentation']")
    WebElement cbRecaptchaIframe1;
    //
    @FindBy(css = "#recaptcha-anchor")
    WebElement cbRecaptchaAnchor1;

    public void ingresarLinkDeLaReserva(){
        //url = "https://bancacomercial-uat.interbank.pe/cuenta-corriente/solicitud/autorizacion-representante-legal?tokenId=BOUB1iUStCmhEBRJi1U9sxSh0tIg8j+eHuE+i6YHNuQJNUfc5LxNcLMTUMhU1hL0";//pegado del link
        //getDriver().get(url);
       // Util.waitFor(5);
        waiting(7);
        System.out.println("paso 1");
    }
    public static void waiting(int second){
        try {
            Thread.sleep(second * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void seleccionarTipoDocumento(String tipoDoc){
        waiting(10);
        System.out.println("paso 2");
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+tipoDoc+"').click()");
       /*((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('m-hyperloop-inputgroup-select')" +
                ".shadowRoot.querySelector('a-hyperloop-select')" +
                ".shadowRoot.querySelector('p-hyperloop-svg').click()");

        if(tipoDoc.equalsIgnoreCase("DNI")){
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('m-hyperloop-inputgroup-select')" +
                    ".shadowRoot.querySelector('a-hyperloop-select')" +
                    ".shadowRoot.querySelector('ul li[id='DNI']').click()");
        }else{
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('m-hyperloop-inputgroup-select')" +
                    ".shadowRoot.querySelector('a-hyperloop-select')" +
                    ".shadowRoot.querySelector('ul li[id='CE']').click()");
        }*/

    }
    public void ingresarNumeroDocumento(String numero_documento){
        Util.waitFor(1);
        int c = 0;
        while (c < numero_documento.length()) {
            char varC = numero_documento.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-verification-document-step > busin-verification-document-step-ui > section > div.verification-document-view > div > form > div.grid.mt-20 > div.col-xs-12.col-6.grid__left.mt-20 > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input")).sendKeys(Character.toString(varC));
            c++;
        }
    }
    public void selectPoliticaPrivacidadCC() {
        Util.waitFor(1);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");

        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#check-pdp')" +
                ".shadowRoot.querySelector('div > label').click()");

    }
    public void SelctCaptchaCC() {
        Util.scrollToElementVisible(cbRecaptchaIframe1, "0");
        getDriver().switchTo().frame(cbRecaptchaIframe1);
        cbRecaptchaAnchor1.click();
        Util.waitFor(1);
        getDriver().switchTo().parentFrame();
    }



    public void BtSiguienteCC() {

        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('.hydrated > .hyperloop-button.hyperloop-button__default.hyperloop-button__m.hyperloop-button__primary.hyperloop-button__primary--m').click()");
    }

    public void obtenerResumenCC() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0, -window.innerHeight)");
        Util.waitFor(4);
        js.executeScript("javascript:window.scrollBy(0, 500)");
        Util.waitFor(4);

    }

    public void obtenerResumenValid() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0, -window.innerHeight)");
        Util.waitFor(4);
        js.executeScript("javascript:window.scrollBy(0, 500)");
        Util.waitFor(4);
    }

    public void aceptarFrima() {
        Util.waitFor(1);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");

        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#check-termsBiometria')" +
                ".shadowRoot.querySelector('div > label').click()");
    }

    public void aceptaTerminosyCondiciones() {
        Util.waitFor(1);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");

        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#check-termsCC')" +
                ".shadowRoot.querySelector('div > label').click()");
    }

    public void residenciaFiscal() {
        Util.waitFor(1);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");

        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#check-companyResidentPeru')" +
                ".shadowRoot.querySelector('div > label').click()");
    }

    public void BtnComenzarBiometría() {
        Util.waitFor(2);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-accept-terms-biometria-step-container > busin-accept-terms-biometria-step-ui > section > div.accept-terms-biometria-step__wrapper-button > a-hyperloop-button > button').click()");
    }

   public void solicitudBiometria() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0, -window.innerHeight)");
        Util.waitFor(4);
        js.executeScript("javascript:window.scrollBy(0, 500)");
        Util.waitFor(4);
       Util.waitFor(1);
       //getDriver().switchTo().window(getDriver().getWindowHandle());
       Util.waitFor(1);
      // Alert alert = getDriver().switchTo().alert();
       //alert.accept();
    }


    public void BtnTerminar() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0, -window.innerHeight)");
        Util.waitFor(4);
        js.executeScript("javascript:window.scrollBy(0, 500)");
        Util.waitFor(4);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-final-summary-step-container > busin-final-summary-step-ui > section > div.active-view__wrapper-button > a-hyperloop-button > button').click()");
    }

    public void resumenCC() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0, -window.innerHeight)");
        Util.waitFor(4);
        js.executeScript("javascript:window.scrollBy(0, 500)");
        Util.waitFor(4);
    }
}

