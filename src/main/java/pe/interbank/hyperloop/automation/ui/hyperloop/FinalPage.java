package pe.interbank.hyperloop.automation.ui.hyperloop;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.hyperloop.automation.utils.Util;


public class FinalPage extends PageObject {

    public void resumenDetalleCuenta() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-summary-step > busin-summary-step-ui > section > div > div.grid.header > div.col-7.col-xs-12.mt-xs-20.grid.grid-center > div > div.col-12.mt-20 > a-hyperloop-link-button:nth-child(1) > button').click()");
    }

    public void resumenDetalleSeguro() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-summary-step > busin-summary-step-ui > section > div > div.grid.header > div.col-7.col-xs-12.mt-xs-20.grid.grid-center > div > div.col-12.mt-20 > a-hyperloop-link-button:nth-child(2) > button').click()");
    }

    public String resumenExpediente() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-summary-step > div.Step > div > div > div:nth-child(1) > div:nth-child(2) > ibk-font')" +
                    ".shadowRoot.querySelector('p').innerText");
        return (String) objText;
    }

    public String resumenTitularCuenta() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-summary-step > div.Step > div > div > div:nth-child(1) > div:nth-child(2) > ibk-font')" +
                ".shadowRoot.querySelector('p').innerText");
        return (String) objText;
    }

    public String resumenTipoPersona() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-summary-step > div.Step > div > div > div:nth-child(1) > div:nth-child(2) > ibk-font')" +
                ".shadowRoot.querySelector('p').innerText");
        return (String) objText;
    }

    public String resumenPlanTarifario() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-summary-step > div.Step > div > div > div:nth-child(1) > div:nth-child(2) > ibk-font')" +
                ".shadowRoot.querySelector('p').innerText");
        return (String) objText;
    }

    public String resumenCuentaSoles() {

        //Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('div:nth-of-type(4) > .center-text.content_title.grid.mt-10 > div:nth-of-type(2) > a-hyperloop-paragraph:nth-of-type(1) > .color-grey-dark.p3').innerText");
        //Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > busin-summary-detail-modal-container > busin-summary-detail-modal-ui > section > div > section.modal-body > div > div > div:nth-child(3) > div > div.col-6.col-xs-12 > a-hyperloop-paragraph:nth-child(1) > p')");
        WebElement cuenta = getDriver().findElement(By.xpath("/html/body/busin-summary-detail-modal-container/busin-summary-detail-modal-ui/section/div/section[3]/div/div/div[3]/div/div[2]/a-hyperloop-paragraph[1]/p"));
        return cuenta.getText();

    }
    public String resumenCuentaDolares() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('div:nth-of-type(5) > .center-text.content_title.grid.mt-10 > div:nth-of-type(2) > a-hyperloop-paragraph:nth-of-type(1) > .color-grey-dark.p3').innerText");
        return (String) objText;
    }

    public String resumenCuentaCCISoles() {
        WebElement cuenta = getDriver().findElement(By.xpath("/html/body/busin-summary-detail-modal-container/busin-summary-detail-modal-ui/section/div/section[3]/div/div/div[3]/div/div[2]/a-hyperloop-paragraph[2]/p"));
        return cuenta.getText();
    }
    public String resumenCuentaCCIDolares() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('div:nth-of-type(5) > .center-text.content_title.grid.mt-10 > div:nth-of-type(2) > a-hyperloop-paragraph:nth-of-type(2) > .color-grey-dark.p3').innerText");
        return (String) objText;
    }

    public String resumenLugar() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-summary-step > div.Step > div > div > div:nth-child(1) > div:nth-child(2) > ibk-font')" +
                ".shadowRoot.querySelector('p').innerText");
        return (String) objText;
    }

    public String resumenLugarBimoneda() {
        Object objText = ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-summary-step > div.Step > div > div > div:nth-child(1) > div:nth-child(2) > ibk-font')" +
                ".shadowRoot.querySelector('p').innerText");
        return (String) objText;
    }


}
