package pe.interbank.hyperloop.automation.ui.hyperloop;


import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import pe.interbank.hyperloop.automation.utils.Util;

public class HubFactoringMlpPage extends PageObject {

    public void completarHubFactoring(String parameter) {
        Util.waitFor(1);

        int c = 0;
        while (c < parameter.length()) {
            char varC = parameter.toUpperCase().charAt(c);
            Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("m-inputgroup-autocomplete")))
                    .findElement(By.cssSelector("div input")).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void clickBuscarHubFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-purchaser-step > fact-purchaser-step-ui > section > div.business-view > form > div > div > m-inputgroup-autocomplete').shadowRoot.querySelector('div > a-hyperloop-autocomplete > div > div.IbkInput.IbkInputWithIcon.right > div > p-hyperloop-svg').click()");


    }

    public void selectMonedaFactoring(String moneda) {
        Util.waitFor(1);
        switch (moneda) {
            case "SOLES":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-purchaser-step > fact-purchaser-step-ui > section > div.business-view > form > div.container-formulario.mt-10 > div.ng-untouched.ng-pristine.ng-invalid > fact-purchaser-sale-form > div > div.col-3.col-xs-12.tabInputGroup > div > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button.hyperloop-tab__button.hyperloop-tab__button--selected.disabled-style').click()");
                break;
            case "DOLARES":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-purchaser-step > fact-purchaser-step-ui > section > div.business-view > form > div.container-formulario.mt-10 > div.ng-untouched.ng-pristine.ng-invalid > fact-purchaser-sale-form > div > div.col-3.col-xs-12.tabInputGroup > div > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button:nth-child(2)').click()");
                break;

         }
    }


    public void selectPlazoFactoring(String plazo,String xpath) {
        Util.waitFor(1);
        switch (plazo) {
            case "30":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('" + xpath + "')[0]" +
                        ".shadowRoot.querySelectorAll('li')[0].click()");
                break;
            case "60":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('" + xpath + "')[0]" +
                        ".shadowRoot.querySelectorAll('li')[1].click()");
                break;
            case "90":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('" + xpath + "')[0]" +
                        ".shadowRoot.querySelectorAll('li')[2].click()");
                break;
            case "120":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('" + xpath + "')[0]" +
                        ".shadowRoot.querySelectorAll('li')[3].click()");
                break;
            case "150":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('" + xpath + "')[0]" +
                        ".shadowRoot.querySelectorAll('li')[4].click()");
                break;
            case "180":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('" + xpath + "')[0]" +
                        ".shadowRoot.querySelectorAll('li')[5].click()");
                break;
        }
    }

    public void completarDatosHubFactoring(String parametro,String xpath) {
        Util.waitFor(1);
        int c = 0;
        //getDriver().switchTo().frame(getDriver().findElement(By.xpath(".//iframe[@src='https://www.googletagmanager.com/ns.html?id=GTM-TZTWHSF']")));
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector(xpath)).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void clickContinuarHubFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-purchaser-step > fact-purchaser-step-ui > section > div.content-center.business-step__wrapper-button.grid-xs-reverse-col > a-hyperloop-button:nth-child(2) > button').click()");
    }



    public void clickOtraMonedaHubFactoring() {

        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('.hydrated > .color-green1.commontitle-2.hyperloop-link-button').click()");
    }
}
