package pe.interbank.hyperloop.automation.ui.hyperloop;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;
import pe.interbank.hyperloop.automation.utils.Util;

public class OfertaFactoringMlpPage extends PageObject {

    public void clickTerminosYCondiciones() {
        Util.waitFor(3);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-pdp').shadowRoot.querySelector('div > label').click()");
    }

    public void clickContinuarOfertaFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-active-accounts-step > fact-active-accounts-step-ui > section > div.active-view__wrapper-button.grid-xs-reverse-col > a-hyperloop-button:nth-child(2) > button').click()");
    }

    public void clickAbrirCuentaNegociosFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('.grid-xs-column.modal-body__buttons > a-hyperloop-button:nth-of-type(2)  span').click()");
    }

    public void selectPlanFactoring(String plan) {

       Util.waitFor(3);

        if (plan.equals("TRADICIONAL")){

            JavascriptExecutor js = (JavascriptExecutor) getDriver();
            js.executeScript("javascript:window.scrollBy(350,450)");
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#TRADICIONAL').click()");
        }else{
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#DIGITAL').click()");
        }

    }

    public void clickContinuarConfigurarCuentaFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('a-hyperloop-button:nth-of-type(2) > .hyperloop-button.hyperloop-button__default.hyperloop-button__m.hyperloop-button__primary.hyperloop-button__primary--m').click()");
    }

    public void selectDepartamentoAgenciaFactoring(String departamento) {

        Util.waitFor(1);

        switch (departamento){
            case "ANCASH":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div:nth-child(6) > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[0].click()");
                break;

            case "APURIMAC":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[1].click()");
                break;

            case "AREQUIPA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[2].click()");
                break;
            case "AYACUCHO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[3].click()");
                break;
            case "CAJAMARCA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[4].click()");
                break;
            case "CALLAO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[5].click()");
                break;
            case "CUSCO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[6].click()");
                break;
            case "HUANUCO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[7].click()");
                break;
            case "ICA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[8].click()");
                break;
            case "JUNIN":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[9].click()");
                break;
            case "LA LIBERTAD":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[10].click()");
                break;
            case "LAMBAYEQUE":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[11].click()");
                break;
            case "LIMA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[12].click()");
                break;
            case "LORETO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[13].click()");
                break;
            case "MADRE DE DIOS":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[14].click()");
                break;
            case "MOQUEGUA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[15].click()");
                break;
            case "PASCO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[16].click()");
                break;
            case "PIURA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[17].click()");
                break;
            case "PUNO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[18].click()");
                break;
            case "SAN MARTIN":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[19].click()");
                break;
            case "TACNA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[20].click()");
                break;
            case "TUMBES":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[21].click()");
                break;
            case "UCAYALI":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[22].click()");
                break;

        }
    }


    public void selectProvinciaAgenciaFactoring(String provincia) {
        Util.waitFor(1);

        switch(provincia){
            case "HUARAZ":
            case "ABANCAY":
            case "AREQUIPA":
            case "HUAMANGA":
            case "CAJAMARCA":
            case "CALLAO":
            case "CUSCO":
            case "HUANUCO":
            case "ICA":
            case "HUANCAYO":
            case "TRUJILLO":
            case "CHICLAYO":
            case "LIMA":
            case "MAYNAS":
            case "TAMBOPATA":
            case "MARISCAL NIETO":
            case "PASCO":
            case "PIURA":
            case "PUNO":
            case "MOYOBAMBA":
            case "TACNA":
            case "TUMBES":
            case "CORONEL PORTILLO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[0].click()");
                break;

            case "SANTA":
            case "COTABAMBAS":
            case "CAMANA":
            case "JAEN":
            case "CHINCHA":
            case "CHANCHAMAYO":
            case "PATAZ":
            case "BARRANCA":
            case "ILO":
            case "PAITA":
            case "SAN ROMAN":
            case "SAN MARTIN":
            case "ZARUMILLA":
            case "PUCALLPA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[1].click()");
                break;

            case "NAZCA":
            case "SATIPO":
            case "CAÑETE":
            case "SULLANA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[2].click()");
                break;

            case "PISCO":
            case "HUARAL":
            case "TALARA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[3].click()");
                break;

            case "HUAURA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-account-configuration-step > fact-account-configuration-step-ui > section > div.account-configuration-step > form > div > div.col-12.mt-50 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[4].click()");
                break;
        }

    }

    public void selectTerminosYCondicionesCNFactoring() {
        Util.waitFor(1);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");

        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-pdp')" +
                ".shadowRoot.querySelector('div > label').click()");

        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-lpdp')" +
                ".shadowRoot.querySelector('div > label').click()");

        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-statement')" +
                ".shadowRoot.querySelector('div > label').click()");


    }


    public void selectPrincipalesClientesFactoring(String principales_clientes) {
        Util.waitFor(1);
        if (principales_clientes.equals("Empresas")){
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('a-hyperloop-tab')" +
                    ".shadowRoot.querySelector('div > button:nth-child(1)').click()");
        }else{
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('a-hyperloop-tab')" +
                    ".shadowRoot.querySelector('div > button:nth-child(2)').click()");
        }
    }

    public void clickContinuarResumenCreacionCNFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('a-hyperloop-button:nth-of-type(2) > .hyperloop-button.hyperloop-button__default.hyperloop-button__m.hyperloop-button__primary.hyperloop-button__primary--m').click()");
    }

    public void clickModalConfirmacionFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > fact-affiliate-end-modal > fact-affiliate-end-modal-component-ui > section > div > section > div.modal-body__buttons.grid-xs-column > a-hyperloop-button:nth-child(3) > button').click()");
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0, -window.innerHeight)");

    }

    public void subirPantallResumen() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0, -window.innerHeight)");
        Util.waitFor(4);
        js.executeScript("javascript:window.scrollBy(0, 500)");

    }

    public void selectMonedaCuentaFactoring(String monedaCuenta) {
        Util.waitFor(4);
        switch (monedaCuenta) {
            case "SOLES":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-active-accounts-step > fact-active-accounts-step-ui > section > div.active-view > form > div:nth-child(6) > div.mt-30.soles > m-hyperloop-accounts-card > section > div:nth-child(2) > div.col-1 > m-hyperloop-checkbox')" +
                        ".shadowRoot.querySelector('div > label').click()");
                break;
            case "DOLARES":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-active-accounts-step > fact-active-accounts-step-ui > section > div.active-view > form > div:nth-child(6) > div > m-hyperloop-accounts-card > section > div:nth-child(2) > div.col-1 > m-hyperloop-checkbox')" +
                        ".shadowRoot.querySelector('div > label').click()");
                break;
            case "AMBAS":

                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-active-accounts-step > fact-active-accounts-step-ui > section > div.active-view > form > div:nth-child(6) > div.mt-30.soles > m-hyperloop-accounts-card > section > div:nth-child(2) > div.col-1 > m-hyperloop-checkbox')" +
                        ".shadowRoot.querySelector('div > label').click()");
                Util.waitFor(1);

                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-active-accounts-step > fact-active-accounts-step-ui > section > div.active-view > form > div:nth-child(6) > div.mt-30.dolares > m-hyperloop-accounts-card > section > div:nth-child(2) > div.col-1 > m-hyperloop-checkbox')" +
                        ".shadowRoot.querySelector('div > label').click()");
                break;

        }

    }

    public void bajarpantallaResumen() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();

        Util.waitFor(4);
        js.executeScript("javascript:window.scrollBy(0, 500)");
    }

}

