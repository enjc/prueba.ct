package pe.interbank.hyperloop.automation.ui.hyperloop;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.*;
import pe.interbank.hyperloop.automation.utils.Util;

public class PersonalInfoPage extends PageObject {

    public void selectDocumentType(String tipoDocumento) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > cc-legal-representative-search-pj > form > div > div.flex-9.col.left > div > div.flex-5.col.left > ibk-inputgroup-select')" +
                ".shadowRoot.querySelector('ibk-select')" +
                ".shadowRoot.querySelector('#"+tipoDocumento+"').click()");
    }

    public void clickSearch() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > cc-legal-representative-search-pj > form > div > div.flex-3.col.right > div > ibk-button')" +
                ".shadowRoot.querySelector('ibk-font').click()");
        Util.waitFor(9);
    }

    public void selectOperador(String operador, String xpath) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('"+xpath+"')" +
                ".shadowRoot.querySelector('ibk-select')" +
                ".shadowRoot.querySelector('#"+operador+"').click()");
    }

    public void selectGenero() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(5) > div.flex.col.right > ibk-inputgroup-tabs')" +
                ".shadowRoot.querySelector('div > div > div > ibk-tab')" +
                ".shadowRoot.querySelector('div > div:nth-child(1) > button').click()");
    }
    public void estadoCivil() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(6) > div.flex.col.left > ibk-inputgroup-select')" +
                ".shadowRoot.querySelector('div > ibk-select')" +
                ".shadowRoot.querySelector('#SOLTERO').click()");
    }

    public void paisNacimiento() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(8) > div.flex.col.left > ibk-inputgroup-select')" +
                ".shadowRoot.querySelector('ibk-select')" +
                ".shadowRoot.querySelector('#ARGENTINA').click()");
    }

    public void paisNacionalidad() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > div > cc-legal-representative > form > div:nth-child(8) > div.flex.col.right > ibk-inputgroup-select')" +
                ".shadowRoot.querySelector('ibk-select')" +
                ".shadowRoot.querySelector('#ARGENTINA').click()");
    }
}
