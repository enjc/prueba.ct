package pe.interbank.hyperloop.automation.ui.hyperloop;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;
import pe.interbank.hyperloop.automation.utils.Util;

public class ProductInfoPage extends PageObject {

    public void selectMoneda(String moneda) {
        Util.waitFor(1);
        if (moneda.equals("SOLES")) {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-account-setting-step > div.Step > div > cc-plan-form > form > div:nth-child(1) > div > ibk-inputgroup-tabs')" +
                    ".shadowRoot.querySelector('ibk-tab')" +
                    ".shadowRoot.querySelector('div > div:nth-child(1) > button').click()");
        } else {
            if (moneda.equals("DOLARES")) {
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-account-setting-step > div.Step > div > cc-plan-form > form > div:nth-child(1) > div > ibk-inputgroup-tabs')" +
                        ".shadowRoot.querySelector('ibk-tab')" +
                        ".shadowRoot.querySelector('div > div:nth-child(2) > button').click()");
            } else {
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-account-setting-step > div.Step > div > cc-plan-form > form > div:nth-child(1) > div > ibk-inputgroup-tabs')" +
                        ".shadowRoot.querySelector('ibk-tab')" +
                        ".shadowRoot.querySelector('div > div.Tab-element.selected.md.col > button').click()");
            }
        }
    }

    public void selectPlan(String plan) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");
        Util.waitFor(1);
        if (plan.equals("TRADICIONAL")) {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-account-setting-step > div.Step > div > cc-plan-form > form > div:nth-child(2) > div > div.mt-10 > ibk-plan-card')" +
                    ".shadowRoot.querySelector('div > div.content > div.radio-box > div').click()");
        } else {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-account-setting-step > div.Step > div > cc-plan-form > form > div:nth-child(2) > div > div:nth-child(2) > ibk-plan-card')" +
                    ".shadowRoot.querySelector('div > div.content > div.radio-box > div').click()");
        }
    }

    public void selectTerminos() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#authorizeCharge')" +
                ".shadowRoot.querySelector('#styled-checkbox-6').click()");
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#suarp')" +
                ".shadowRoot.querySelector('#styled-checkbox-7').click()");
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");
    }

    public void selectVentas(String ventas) {
        Util.waitFor(1);

        if (ventas.equals("Empresas")) {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div:nth-child(4) > div.col-12.mt-20 > div:nth-child(1) > m-hyperloop-inputgroup-select > div > a-hyperloop-select').shadowRoot.querySelector('#\\\\30 7 > span').click()");
            Util.waitFor(1);
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div:nth-child(4) > div.col-12.mt-20 > div:nth-child(2) > m-hyperloop-inputgroup-select > div > a-hyperloop-select').shadowRoot.querySelector('#\\\\30 701 > span').click()");
        } else {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('a-hyperloop-tab')" +
                    ".shadowRoot.querySelector('div > button:nth-child(2)').click()");
        }
    }

    public void selecRubroActividad(String ventas) {
        Util.waitFor(1);
        if (ventas.equals("Empresas")) {

            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div:nth-child(5) > div:nth-child(4) > div:nth-child(1) > m-hyperloop-inputgroup-select > div > a-hyperloop-select').shadowRoot.querySelector('#\\\\30 7 > span').click()");
            Util.waitFor(1);
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div:nth-child(5) > div:nth-child(4) > div:nth-child(2) > m-hyperloop-inputgroup-select > div > a-hyperloop-select').shadowRoot.querySelector('#\\\\30 701 > span').click()");
            Util.waitFor(1);
        }
    }

    public void selectMonedaHyperloop(String moneda) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0,0)");

        Util.waitFor(1);

        if (moneda.equals("SOLES")) {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(1) > div.grid.grid-start.mt-5 > a-hyperloop-tab')" +
                    ".shadowRoot.querySelector('div > button:nth-child(2)').click()");
        } else {
            if (moneda.equals("DOLARES")) {
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(1) > div.grid.grid-start.mt-5 > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button:nth-child(1)').click()");
            } else {
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(1) > div.grid.grid-start.mt-5 > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button:nth-child(3)').click()");
            }
        }
    }

    public void selectPlanHyperloop(String plan) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");

        Util.waitFor(1);

        if (plan.equals("TRADICIONAL")) {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#TRADICIONAL').click()");
        } else {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#DIGITAL').click()");
        }
    }

    public void selectTerminosHyperloop() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#check-pdp')" +
                ".shadowRoot.querySelector('#styled-checkbox-pdp').click()");

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");

        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#check-lpdp')" +
                ".shadowRoot.querySelector('#styled-checkbox-lpdp').click()");

        /*Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#check-statement')" +
                ".shadowRoot.querySelector('#styled-checkbox-statement').click()");*/


    }

    public void selectDepartamento(String departamento) {
        Util.waitFor(1);
        switch (departamento) {
            case "Huanuco":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[7].click()");
                break;
            case "La Libertad":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[10].click()");
                break;
            case "Tacna":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[20].click()");
                break;
            case "Arequipa":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[2].click()");
                break;
            case "Tumbes":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[21].click()");
                break;
            case "Moquegua":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[15].click()");
                break;
            case "Pasco":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[16].click()");
                break;
            case "Piura":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[17].click()");
                break;
            case "Ancash":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[0].click()");
                break;
            case "Ucayali":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[22].click()");
                break;
        }

    }

    public void selectProvincia(String distrito) {
        Util.waitFor(1);
        switch (distrito) {
            case "Huanuco":
            case "Trujillo":
            case "Tacna":
            case "Arequipa":
            case "Tumbes":
            case "Pasco":
            case "Piura":
            case "Huaraz":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[0].click()");
                break;
            case "Zarumilla":
            case "Ilo":
            case "Paita":
            case "Santa":
            case "Pucallpa":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[1].click()");
                break;
            case "Sullana":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-account-configuration-step > busin-account-configuration-step-ui > section > div.account-configuration-step > form > div > div:nth-child(3) > div > div.col-6.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[2].click()");
                break;
        }
    }

    public void selectSexo(String sexo) {
        Util.waitFor(1);
        if (sexo.equals("MASCULINO")) {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(4) > div.col-xs-12.col-6.grid__left > div.tabInputGroup__content.grid.grid-start > a-hyperloop-tab')" +
                    ".shadowRoot.querySelector('div > button:nth-child(1)').click()");
        } else {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(4) > div.col-xs-12.col-6.grid__left > div.tabInputGroup__content.grid.grid-start > a-hyperloop-tab')" +
                    ".shadowRoot.querySelector('div > button:nth-child(2)').click()");
        }
    }

    public void selectEstadoCivil() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(5) > div > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('li').click()");
    }

    //FLUJO RESERVA CC MVP
    public void selectMonedaCC(String moneda) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0,0)");

        Util.waitFor(1);

        if (moneda.equals("SOLES")) {
            //agregado mn 15092021
           // ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.account-configuration-view > form > div > div.col-12.mt-40 > div.grid.grid-start.mt-20 > a-hyperloop-tab')" +
                  //  ".shadowRoot.querySelector('div > button:nth-child(1)').click()");
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui a-hyperloop-tab:nth-child(1)').shadowRoot.querySelector('div button:nth-child(1)').click()");
        } else {
            if (moneda.equals("DOLARES")) {
               // ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-40 > div.grid.grid-start.mt-20 > a-hyperloop-tab')" +
                    //    ".shadowRoot.querySelector('div > button:nth-child(2)').click()");
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui a-hyperloop-tab:nth-child(1)').shadowRoot.querySelector('div button:nth-child(2)').click()");
            } else {
               // ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-40 > div.grid.grid-start.mt-20 > a-hyperloop-tab')" +
                      //  ".shadowRoot.querySelector('div > button:nth-child(3)').click()");
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui a-hyperloop-tab:nth-child(1)').shadowRoot.querySelector('div button:nth-child(3)').click()");
            }
        }
    }

    /*public void selectEnvioEECC(String tipoEnvioEECC) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0,0)");

        Util.waitFor(1);

        if (tipoEnvioEECC.equals("DIGITAL")) {
            // ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-20 > div.grid.grid-start.mt-20 > a-hyperloop-tab')" +
            // ".shadowRoot.querySelector('div > button:nth-child(1)').click()");
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui div[class='col-12 mt-20'] a-hyperloop-tab').shadowRoot.querySelector('div button:nth-child(1)').click()");
        } else {
           // System.out.println("Hola");
            //((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui div[class='col-12 mt-20'] a-hyperloop-tab').shadowRoot.querySelector('div button:nth-child(2)').click()");
             }
    }*/

    public void selectEnvioEECC(String tipoEnvioEECC){

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0,0)");

        Util.waitFor(1);

        if (tipoEnvioEECC.equals("DIGITAL")){

            /*((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.account-configuration-view > form > div > div.col-12.mt-20 > div.grid.grid-start.mt-20 > a-hyperloop-tab')" +
                    ".shadowRoot.querySelector('div > button:nth-child(1)').click()");*/
        }
        else {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui section div form div div.col-12.mt-20 div a-hyperloop-tab').shadowRoot.querySelector('div button:nth-child(2)').click()");



        }


    }


    public void selectDepartamentoAgenciaCC(String departamento) {

        Util.waitFor(1);

        switch (departamento) {
            case "ANCASH":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[0].click()");
                break;

            case "APURIMAC":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[1].click()");
                break;

            case "AREQUIPA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[2].click()");
                break;
            case "AYACUCHO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[3].click()");
                break;
            case "CAJAMARCA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[4].click()");
                break;
            case "CALLAO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[5].click()");
                break;
            case "CUSCO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[6].click()");
                break;
            case "HUANUCO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[7].click()");
                break;
            case "ICA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[8].click()");
                break;
            case "JUNIN":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[9].click()");
                break;
            case "LA LIBERTAD":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[10].click()");
                break;
            case "LAMBAYEQUE":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[11].click()");
                break;
            case "LIMA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[12].click()");
                break;
            case "LORETO":
                //((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                       // ".shadowRoot.querySelectorAll('li')[13].click()");
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui a-hyperloop-select').shadowRoot.querySelectorAll('li')[13].click()");
                break;
            case "MADRE DE DIOS":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[14].click()");
                break;
            case "MOQUEGUA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[15].click()");
                break;
            case "PASCO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[16].click()");
                break;
            case "PIURA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[17].click()");
                break;
            case "PUNO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[18].click()");
                break;
            case "SAN MARTIN":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[19].click()");
                break;
            case "TACNA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[20].click()");
                break;
            case "TUMBES":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[21].click()");
                break;
            case "UCAYALI":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[22].click()");
                break;

        }
    }

    public void selectProvinciaAgenciaCC(String provincia) {
        Util.waitFor(1);

        switch (provincia) {
            case "HUARAZ":
            case "ABANCAY":
            case "AREQUIPA":
            case "HUAMANGA":
            case "CAJAMARCA":
            case "CALLAO":
            case "CUSCO":
            case "HUANUCO":
            case "ICA":
            case "HUANCAYO":
            case "TRUJILLO":
            case "CHICLAYO":
            case "LIMA":
            case "MAYNAS":
               // ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui div[class='col-6 col-xs-12 grid__left'] a-hyperloop-select').shadowRoot.querySelectorAll('li')[0].click()");
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.account-configuration-view > form > div > div:nth-child(6) > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[0].click()");
                break;
            case "MAYNAS2":
                // ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui div[class='col-6 col-xs-12 grid__left'] a-hyperloop-select').shadowRoot.querySelectorAll('li')[0].click()");
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.account-configuration-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[0].click()");
                break;
            case "TAMBOPATA":
            case "MARISCAL NIETO":
            case "PASCO":
            case "PIURA":
            case "PUNO":
            case "MOYOBAMBA":
            case "TACNA":
            case "TUMBES":
            case "CORONEL PORTILLO":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[0].click()");
                break;

            case "SANTA":
            case "COTABAMBAS":
            case "CAMANA":
            case "JAEN":
            case "CHINCHA":
            case "CHANCHAMAYO":
            case "PATAZ":
            case "BARRANCA":
            case "ILO":
            case "PAITA":
            case "SAN ROMAN":
            case "SAN MARTIN":
            case "ZARUMILLA":
            case "PUCALLPA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[1].click()");
                break;

            case "NAZCA":
            case "SATIPO":
            case "CAÑETE":
            case "SULLANA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[2].click()");
                break;

            case "PISCO":
            case "HUARAL":
            case "TALARA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[3].click()");
                break;

            case "HUAURA":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelectorAll('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-account-configuration-step-container > busin-account-configuration-step-ui > section > div.data-verification-view > form > div > div.col-12.mt-30 > div > div.col-6.col-xs-12.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')[0]" +
                        ".shadowRoot.querySelectorAll('li')[4].click()");
                break;
        }

    }

    public void selectTerminosYCondicionesCC() {
        Util.waitFor(1);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");

        //((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#check-pdp')" +
            //    ".shadowRoot.querySelector('div > label').click()");
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('hyperloop-currentaccount-ui m-hyperloop-checkbox').shadowRoot.querySelector('div > label').click()");

    }

    public void clickSiguienteResumenCreacionCC() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('.hydrated > .hyperloop-button.hyperloop-button__default.hyperloop-button__m.hyperloop-button__primary.hyperloop-button__primary--m').click()");
       // ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('busin-account-configuration-step-ui a-hyperloop-button').click()");

    }

    public void clickModalConfirmacionRLCC() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > busin-current-legal-representative-modal-container > current-legal-representative-modal-component-ui > section > div > section > div.modal-body__buttons.grid-xs-column > a-hyperloop-button:nth-child(2) > button').click()");
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(0, -window.innerHeight)");

    }
}

