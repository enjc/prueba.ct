package pe.interbank.hyperloop.automation.ui.hyperloop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import pe.interbank.hyperloop.automation.utils.Util;


public class SearchPage extends PageObject {

    @FindBy(css = "iframe[role='presentation']")
    WebElement cbRecaptchaIframe;
    //
    @FindBy(css = "#recaptcha-anchor")
    WebElement cbRecaptchaAnchor;

    public void selectDocumentType(String documentType) {
        Util.waitFor(10);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('ibk-inputgroup-select')" +
                ".shadowRoot.querySelector('ibk-select')" +
                ".shadowRoot.querySelector('#"+documentType+"').click()");
    }

    public void completeDocNum(String NumeroDocumento) {
        Util.waitFor(1);

        int c = 0;
        while (c < NumeroDocumento.length()) {
            char varC = NumeroDocumento.toUpperCase().charAt(c);
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("ibk-inputgroup-text")))
                    .findElement(By.cssSelector("ibk-input")))
                    .findElement(By.cssSelector("div input")).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void clickInicaSesion() {
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('ibk-button')" +
                ".shadowRoot.querySelector('button').click()");
        Util.waitFor(10);
    }

    public void clickProduct() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('ibk-product-card')" +
                ".shadowRoot.querySelector('ibk-icon-svg').click()");
    }

    public void clickAbrirCuenta() {
        Util.waitFor(1);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(300,400)");
        Util.waitFor(3);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('ibk-product-card')" +
                ".shadowRoot.querySelector('ibk-button')" +
                ".shadowRoot.querySelector('button').click()");
        Util.waitFor(13);
    }

    public void products(String parametro) {
        Util.waitFor(2);
        int c = 0;
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#search > ibk-inputgroup-text")))
                    .findElement(By.cssSelector("ibk-input")))
                    .findElement(By.cssSelector("div input")).sendKeys(Character.toString(varC));
            c++;
        }

        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#search > ibk-inputgroup-text")))
                .findElement(By.cssSelector("ibk-input")))
                .findElement(By.cssSelector("div input")).sendKeys(Keys.TAB);
    }

    public void selectPersonaNatural() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelectorAll('ibk-product-card')[0]" +
                ".shadowRoot.querySelectorAll('ibk-select')[0]" +
                ".shadowRoot.querySelectorAll('li')[0].click()");
    }

    public void selectDocumentTypeHyper(String tipo_documento) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+tipo_documento+"').click()");
    }

    public void completeDocNumHyper(String numero_documento) {
        Util.waitFor(1);
        int c = 0;
        while (c < numero_documento.length()) {
            char varC = numero_documento.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-10.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input")).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void swipe() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);
        js.executeScript("javascript:window.scrollBy(350,450)");
        Util.waitFor(1);
        js.executeScript("javascript:window.scrollBy(350,450)");
    }

    public void clicResidenciaFiscal() {
        Util.waitFor(2);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('ibk-inputgroup-radio')" +
                ".shadowRoot.querySelector('#styled-radio-affidavitResponse-1').click()");
    }

    public void selectDocumentTypeFactoring(String tipo_documento) {
        Util.waitFor(6);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-step > fact-legal-representative-step-ui > section > div.legal-representative-view > form > div.grid.mt-10.grid-input > div.col-xs-12.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+tipo_documento+"').click()");
    }

    public void completeDocNumFactoring(String numero_documento) {
        Util.waitFor(1);
        int c = 0;
        while (c < numero_documento.length()) {
            char varC = numero_documento.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector("m-hyperloop-inputgroup-text[label='Número de documento (*)']  .hydrated  .HyperloopInput__input.m")).sendKeys(Character.toString(varC));
            c++;
        }
        }


    public void completarDatosFactoring(String parametro, String xpath) {
        Util.waitFor(1);
        int c = 0;
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector(xpath)).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void selectCarrierFactoring(String operador) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-step > fact-legal-representative-step-ui > section > div.legal-representative-view > form > div.grid.mt-40.grid-input > div.col-xs-12.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+operador+"').click()");
    }

    public void SelectTerminosFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-pdp')" +
                ".shadowRoot.querySelector('label').click()");
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("javascript:window.scrollBy(250,350)");
    }

    public void SelectCaptchaFactoring() {
        Util.scrollToElementVisible(cbRecaptchaIframe, "0");
        getDriver().switchTo().frame(cbRecaptchaIframe);
        cbRecaptchaAnchor.click();
        Util.waitFor(1);
        getDriver().switchTo().parentFrame();
    }

    public void BtnSiguiente() {

        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('.hydrated > .hyperloop-button.hyperloop-button__default.hyperloop-button__m.hyperloop-button__primary.hyperloop-button__primary--m').click()");
    }

    public void selectGeneroFactoring(String genero) {
        switch (genero) {
            case "HOMBRE":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(4) > div.col-xs-12.col-6.grid__left > div.tabInputGroup__content.grid.grid-start > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button:nth-child(1)').click()");
                break;
            case "MUJER":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(4) > div.col-xs-12.col-6.grid__left > div.tabInputGroup__content.grid.grid-start > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button:nth-child(2)').click()");
                break;
        }
    }

    public void selectMaritalStatusFactoring(String estado_civil) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(5) > div > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+estado_civil+"').click()");
    }

    public void completarFechaNacimientoFactoring(String parameter) {
        Util.waitFor(1);

        int c = 0;
        while (c < parameter.length()) {
            char varC = parameter.toUpperCase().charAt(c);
            Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("inputgroup-text-v2")))
                    //inputgroup-text-v2
                .findElement(By.cssSelector("div input")).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void SelectLDPDFactoring() {

        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#check-lpdp')" +
                ".shadowRoot.querySelector('label').click()");
       }

    public void selectCountryOfBirth(String p_nacimiento) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(9) > div.col-xs-12.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+p_nacimiento+"').click()");
    }

    public void selectCountryOfNationality(String p_nacionalidad) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(9) > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+p_nacionalidad+"').click()");
    }

    public void selectDocumentTypeCC(String tipo_documento) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+tipo_documento+"').click()");
    }

    public void completeDocNumCC(String numero_documento) {
        Util.waitFor(1);
        int c = 0;
        while (c < numero_documento.length()) {
            char varC = numero_documento.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8 > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-verification-step > busin-data-verification-step-ui > section > div.data-verification-view > form > div.grid.mt-10.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input")).sendKeys(Character.toString(varC));
            //getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.data-legal-representative-view > form > div.grid.mt-10.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input")).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void completeDocNumCCRL1(String numero_documento) {
        Util.waitFor(1);
        int c = 0;
        while (c < numero_documento.length()) {
            char varC = numero_documento.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.data-verification-view > form > div.grid.mt-10.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input")).sendKeys(Character.toString(varC));
            c++;
        }
    }

    //agregado MN 14092021
    public void completeDocNumeroCCRL1(String numero_documento) {
        Util.waitFor(1);
        int c = 0;
        while (c < numero_documento.length()) {
            char varC = numero_documento.toUpperCase().charAt(c);
            //getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.data-verification-view > form > div.grid.mt-10.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input")).sendKeys(Character.toString(varC));
            getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.data-legal-representative-view > form > div.grid.mt-10.grid-input > div.col-xs-12.col-6.grid__left > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > input")).sendKeys(Character.toString(varC));
            c++;
        }
    }
    public void completeDocNumCCRL2(String numero_documento) {
        Util.waitFor(1);
        int c = 0;
        while (c < numero_documento.length()) {
            char varC = numero_documento.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector("m-hyperloop-inputgroup-text[label='Número de documento (*)']  .hydrated  .HyperloopInput__input.m")).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void SubirCertificadoCC1(String filename) {
        //agregado mn 14092021
        //WebElement root=getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-step-container > busin-data-company-step-ui > section > div.data-verification-view > form > div > div.grid > div:nth-child(4) > div.mt-10 > hyperloop-input-file"));
        WebElement root=getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-step-container > busin-data-company-step-ui > section > div.data-company-view > form > div > div.grid > div.col-12.mt-30.grid > div.mt-10 > hyperloop-input-file"));
        WebElement shadowRoot=Util.expandRootElement(root);
        WebElement UploadFile=shadowRoot.findElement(By.cssSelector("#file"));
        upload(filename).to(UploadFile);

    }

    public void SubirCertificadoCC2(String filename) {
// agregado mn 15092021
        //WebElement root=getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-step-container > busin-data-company-step-ui > section > div.data-verification-view > form > div > div.grid > div:nth-child(5) > div.mt-10 > hyperloop-input-file"));
        WebElement root=getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-step-container > busin-data-company-step-ui > section > div.data-company-view > form > div > div.grid > div.col-12.mt-20.grid > div.mt-10 > hyperloop-input-file"));
        WebElement shadowRoot=Util.expandRootElement(root);
        WebElement UploadFile=shadowRoot.findElement(By.cssSelector("#file"));
        upload(filename).to(UploadFile);

    }

    public void SubirEstatutoCC(String filename) {

        WebElement root=getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.grid > div:nth-child(5) > hyperloop-input-file"));
        WebElement shadowRoot=Util.expandRootElement(root);
        WebElement UploadFile=shadowRoot.findElement(By.cssSelector("#file"));
        upload(filename).to(UploadFile);

    }

    public void SubirCertificadoCCEmpresaNoCliente(String filename) {

        WebElement root=getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div.grid > div:nth-child(3) > hyperloop-input-file"));
        WebElement shadowRoot=Util.expandRootElement(root);
        WebElement UploadFile=shadowRoot.findElement(By.cssSelector("#file"));
        upload(filename).to(UploadFile);

    }

    public void selectGeneroCCRL1(String genero) {
        switch (genero) {
            case "HOMBRE":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div:nth-child(4) > div.col-xs-12.col-6.grid__left > div.tabInputGroup__content.grid.grid-start > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button:nth-child(1)').click()");
                break;
            case "MUJER":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div:nth-child(4) > div.col-xs-12.col-6.grid__left > div.tabInputGroup__content.grid.grid-start > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button:nth-child(2)').click()");
                break;
        }
    }

    public void selectMaritalStatusCCRL1(String estadocivil) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div:nth-child(5) > div > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+estadocivil+"').click()");
    }

    public void selectCarrierCCRL1(String operador) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div.grid.mt-30.grid-input > div.col-xs-12.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+operador+"').click()");
    }

    public void selectGeneroCCRL2(String genero) {
        switch (genero) {
            case "HOMBRE":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div:nth-child(4) > div.col-xs-12.col-6.grid__left > div.tabInputGroup__content.grid.grid-start > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button:nth-child(1)').click()");
                break;
            case "MUJER":
                ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div:nth-child(4) > div.col-xs-12.col-6.grid__left > div.tabInputGroup__content.grid.grid-start > a-hyperloop-tab')" +
                        ".shadowRoot.querySelector('div > button:nth-child(2)').click()");
                break;
        }
    }
    public void selectMaritalStatusCCRL2(String estadocivil) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div:nth-child(5) > div > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+estadocivil+"').click()");
    }
    public void selectCarrierCCRL2(String operador) {
        Util.waitFor(1);
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div.grid.mt-30.grid-input > div.col-xs-12.col-6.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#"+operador+"').click()");
    }

}
