package pe.interbank.hyperloop.automation.ui.hyperloop;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.*;
import pe.interbank.hyperloop.automation.utils.Util;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import static pe.interbank.hyperloop.automation.utils.Util.expandRootElement;

public class WorkInfoPage extends PageObject {

    public void selectPrincipalCliente() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('ibk-inputgroup-tabs')" +
                ".shadowRoot.querySelector('ibk-tab')" +
                ".shadowRoot.querySelector('button').click()");
    }

    public void ingresoTexto(String parametro, String xpath) {
        Util.waitFor(1);
        int c = 0;
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            expandRootElement(expandRootElement(getDriver()
                    .findElement(By.cssSelector(xpath)))
                    .findElement(By.cssSelector("ibk-input")))
                    .findElement(By.cssSelector("div input")).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void ingresoTextoRuc(String parametro, String xpath) {
        Util.waitFor(1);
        int c = 0;
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            expandRootElement(expandRootElement(getDriver()
                    .findElement(By.cssSelector(xpath)))
                    .findElement(By.cssSelector("ibk-input")))
                    .findElement(By.cssSelector("div input")).sendKeys(Character.toString(varC));
            c++;
        }
        Util.waitFor(1);
        expandRootElement(expandRootElement(getDriver()
                .findElement(By.cssSelector(xpath)))
                .findElement(By.cssSelector("ibk-input")))
                .findElement(By.cssSelector("div input")).sendKeys(Keys.ENTER);
    }

    public void ingresoFecha(String parametro, String xpath) {
        Util.waitFor(1);
        int c = 0;
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            expandRootElement(expandRootElement(getDriver()
                    .findElement(By.cssSelector(xpath)))
                    .findElement(By.cssSelector("ibk-input-date")))
                    .findElement(By.cssSelector("div input")).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void continuar(String xpath) {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('" + xpath + "')" +
                ".shadowRoot.querySelector('ibk-font').click()");
    }

    public void ubigeo(String parametro, String xpath) {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('" + xpath + "')" +
                ".shadowRoot.querySelector('div > ibk-textfield-autocomplete')" +
                ".shadowRoot.querySelector('#" + parametro + "').click()");
    }

    public void getparametros(String parametro, String xpath) {
        Util.waitFor(1);
        int c = 0;
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector(xpath)).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void ingresoRazonSocial(String parametro, String xpath) {
        Util.waitFor(1);
        int c = 0;
        WebElement razonSocial = getDriver().findElement(By.cssSelector(xpath));
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            razonSocial.sendKeys(Character.toString(varC));
            c++;
        }
        razonSocial.sendKeys(Keys.TAB);
    }


    public void ingresoFechaConstitucion(String fechaConstitucion) {
        Util.waitFor(1);
        WebElement txtFechaConstitucion = getDriver().findElement(By.cssSelector("body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div.grid.col-top > div:nth-child(1) > div.mt-10 > inputgroup-text-v2"));

        int c = 0;
        while (c < fechaConstitucion.length()) {
            char varC = fechaConstitucion.toUpperCase().charAt(c);
            txtFechaConstitucion.sendKeys(Character.toString(varC));
            c++;
        }
    }



    public void selectTipodeContribuyente(String tipo_de_contribuyente) {
        Util.waitFor(1);
        switch (tipo_de_contribuyente) {
            case "(SA) Sociedad anonima":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.client-data-step > form > div.grid.grid-between > div.col-6.col-xs-12.mt-20 > m-hyperloop-inputgroup-select > div > a-hyperloop-select').shadowRoot.querySelector('#SA').click()");
                break;
        }
    }

    public void selectUbigeo(String parametro, String xpath) {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('" + xpath + "')" +
                ".shadowRoot.querySelector('li').click()");
    }

    public void clicksiguiente() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-company-data-step > busin-company-data-step-ui > section > div.content-client-data-step__wrapper-button > a-hyperloop-button > button').click()");
    }


    public void selectVia(String nombre_via) {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.client-data-step > form > div:nth-child(9) > div.col-xs-12.col-4.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('li').click()");
    }

    public void clickcontinuarnocliente() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-businnessaccount > hyperloop-businnessaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-businessaccount-app > busin-business-account > busin-business-account-ui > div > busin-data-legal-representative-step > busin-data-legal-representative-step-ui > section > div.content-client-data-step__wrapper-button > a-hyperloop-button > button').click()");
    }

    public void selectRepresentanteLegal() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('#wrapper > div > app-portal > div > div > div > ng-component > micro-outlet > business-account-app > cc-workflow > div > div.Workflow-right.col.hidden-scroll.pb-40 > div > div.Section > cc-legal-representative-step > div.Step > div > cc-representative-view-step > cc-legal-representative-search > form > div > div.flex-3.col.right > div > ibk-button')" +
                ".shadowRoot.querySelector('button').click()");
    }

    //---FLUJO FACTORING MLP ----//

    public void selectUbigeoFactoring(String parametro, String xpath) {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('" + xpath + "')" +

                ".shadowRoot.querySelector('#" + parametro + "').click()");
    }

    public void selectViaFactoringCE(String tipo_via) {
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(11) > div.col-xs-12.col-4.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#" + tipo_via + "').click()");
    }

    public void selectViaFactoringDNI(String tipo_via) {

        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.client-data-step > form > div:nth-child(9) > div.col-xs-12.col-4.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#" + tipo_via + "').click()");
    }

    public void getparametrosFactoring(String parametro, String xpath) {
        Util.waitFor(1);
        int c = 0;
        while (c < parametro.length()) {
            char varC = parametro.toUpperCase().charAt(c);
            getDriver().findElement(By.cssSelector(xpath)).sendKeys(Character.toString(varC));
            c++;
        }
    }

    public void clickcontinuarnoclienteFactoring() {

        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-legal-representative-not-client-step > fact-legal-representative-notclient-step-ui > section > div.content-client-data-step__wrapper-button > a-hyperloop-button > button').click()");
    }

    public void clickBuscarRucFactoring() {

        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.business-view > form > div > div > div > m-hyperloop-inputgroup-text > div > a-hyperloop-input > div > div > p-hyperloop-svg').click()");

    }

    public void selectViaEmpresaFactoring(String tipo_via) {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.business-view > form > div:nth-child(7) > div:nth-child(3) > div.col-4.col-xs-12.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('li').click()");

    }

    public void clickContinuarNoclienteGiradorFactoring() {
        Util.waitFor(1);
        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-factoring > hyperloop-factoring-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-factoring-app > fact-factoring > fact-factoring-iu > div > fact-business-step > fact-business-step-ui > section > div.content-center.business-step__wrapper-button > div > div > a-hyperloop-button > button').click()");
    }

    //FLUJO CUENTA CORRIENTE RESERVA MVP

    public void bajarpantallaResumenCC() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();

        Util.waitFor(4);
        js.executeScript("javascript:window.scrollBy(0, 500)");
    }

    public void clickCopiarEnlace() {

       //WebElement url = getDriver().findElement(By.xpath("//div[@id='copy_link']/a-hyperloop-link-button/button"));
       WebElement url = getDriver().findElement(By.xpath("//*[@id='copy_link']/a-hyperloop-link-button/button"));
        url.click();
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        try {
            String textCopied = (String) clipboard.getData(DataFlavor.stringFlavor);
            System.out.println("TEXTO COPIADO: " + textCopied);
        } catch (UnsupportedFlavorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void selectViaCCDNIRL1(String tipo_via) {

        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-legal-representative-noclient-step-container > busin-legal-representative-noclient-step-ui > section > div:nth-child(1) > form > div:nth-child(9) > div.col-xs-12.col-4.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#" + tipo_via + "').click()");
    }

    public void selectViaCCDNIRL2(String tipo_via) {

        ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-joint-signature-noclient-step-container > busin-join-signature-noclient-step-ui > section > div.data-verification-view > form > div:nth-child(9) > div.col-xs-12.col-4.grid__right > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                ".shadowRoot.querySelector('#" + tipo_via + "').click()");
    }

    public void selectTipodeContribuyenteCC(String tipo_de_contribuyente) {
        Util.waitFor(1);
        switch (tipo_de_contribuyente) {
            case "(SA) Sociedad anonima":
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('body > hyperloop-root > hyperloop-home-ui > div.home_body > hyperloop-currentaccount > hyperloop-currentaccount-ui > section > div > div.aside-left.col-xs-12.col-8.background-layout__b > ng-component > micro-outlet > hyperloop-currentaccount-app > current-account > ckecking-account-ui > div > busin-data-company-noclient-step-container > busin-data-company-noclient-step-ui > section > div.data-verification-view > form > div > div:nth-child(5) > m-hyperloop-inputgroup-select > div > a-hyperloop-select')" +
                        ".shadowRoot.querySelector('li').click()");
                break;
        }
    }
}
