package pe.interbank.hyperloop.automation.utils;

public class Constantes {

    private static String tipoDocumento;
    private static String numeroDocumento;
    private static String tipoCliente;
    private static String tipoEmpresa;
    private static String tipoMoneda;
    private static String tipoPersona;
    private static String tiendaDistrito;
    private static String tipoEnvioEECC;

    public  String getTipoDocumento() { return tipoDocumento; }
    public  String getNumeroDocumento() { return numeroDocumento; }
    public  String getTipoCliente() { return tipoCliente; }
    public  String getTipoEmpresa() { return tipoEmpresa; }
    public  String getTipoMoneda() { return tipoMoneda; }
    public  String getTipoPersona() { return tipoPersona; }
    public  String getTiendaDistrito() { return tiendaDistrito; }
    public  String getTipoEnvioEECC() { return tipoEnvioEECC; }

    public void setTipoDocumento(String TipoDocumento) { tipoDocumento = TipoDocumento; }
    public void setNumeroDocumento(String NumeroDocumento) { numeroDocumento = NumeroDocumento; }
    public void setTipoCliente(String TipoCliente) { tipoCliente = TipoCliente; }
    public void setTipoEmpresa(String TipoEmpresa) { tipoEmpresa = TipoEmpresa; }
    public void setTipoMoneda(String TipoMoneda) { tipoMoneda = TipoMoneda; }
    public void setTipoPersona(String TipoPersona) { tipoPersona = TipoPersona; }
    public void setTiendaDistrito(String TiendaDistrito) { tiendaDistrito = TiendaDistrito; }
    public void setTipoEnvioEECC(String TipoEnvioEECC) { tipoEnvioEECC = TipoEnvioEECC; }
}
