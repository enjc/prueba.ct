package pe.interbank.hyperloop.automation.utils;

import java.sql.*;

public class DataBase {
    private static final String URL = PropertyManager.getInstance().getUrlDataBase();
    private static final String USER = PropertyManager.getInstance().getUserDataBase();
    private static final String PASS = PropertyManager.getInstance().getPassDataBase();

    public static Connection conn = null;
    public static ResultSet result = null;

    public static Connection getConnection() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(URL, USER, PASS);
        } catch (Exception e) {
            e.getMessage();
        }
        return conn;
    }

    public static ResultSet getResult(String selectSql, Connection conn) {
        try {
            Statement stmt = conn.createStatement();
            return result = stmt.executeQuery(selectSql);
        } catch (SQLException e) {
            e.getMessage();
        }
        return result;
    }
}
