package pe.interbank.hyperloop.automation.utils;

/**
 * General Parameters
 **/
public class Parameters {
    public static final String columnRunTest = "Run Test?";
    public static final String columnTestCase = "Test Case";
    public static final String YES_ROW = "Yes";
    public static final String NO_ROW = "No";

    public static final String TEMPLATE_BODY = "request.json/CrearCliente.json";
    public static final String TEMPLATE_BODY_ORQUESTADOR1 = "request.json/OrquestadorCNCeMoneda.json";
    public static final String TEMPLATE_BODY_ORQUESTADOR2 = "request.json/OrquestadorCNClienteCeBimoneda.json";
    public static final String TEMPLATE_BODY_ORQUESTADOR3 = "request.json/OrquestadorCNCeNoConstituida.json";
    public static final String TEMPLATE_BODY_ORQUESTADOR4 = "request.json/OrquestadorCNDniMoneda.json";
    public static final String TEMPLATE_BODY_ORQUESTADOR5 = "request.json/OrquestadorCNClienteDniBimoneda.json";
    public static final String TEMPLATE_BODY_ORQUESTADOR6 = "request.json/OrquestadorCNDniNoConstituida.json";
}
