package pe.interbank.hyperloop.automation.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {

    private static PropertyManager propertyManager;
    private static final Object lock = new Object();
    private static final String APPLICATION_PROPERTIES = "application.properties";
    private String tokenUser;
    private String tokenPass;
    private String baseUrl;
    private String securitySubscriptionKey;
    private String urlDataBase;
    private String userDataBase;
    private String passDataBase;

    public static PropertyManager getInstance() {
        if (propertyManager == null) {
            synchronized (lock) {
                propertyManager = new PropertyManager();
                propertyManager.loadData();
            }
        }
        return propertyManager;
    }

    private void loadData() {
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(System.getProperty("user.home") + File.separator + APPLICATION_PROPERTIES));
        } catch (IOException e) {
        }

        tokenUser = prop.getProperty("token.user");
        tokenPass = prop.getProperty("token.pass");
        baseUrl = prop.getProperty("base.url");
        securitySubscriptionKey = prop.getProperty("security.subscription.key");
        urlDataBase=prop.getProperty("url.database.janus");
        userDataBase=prop.getProperty("user.database.janus");
        passDataBase=prop.getProperty("password.database.janus");
    }

    public String getTokenUser() {
        return tokenUser;
    }
    public String getTokenPass() {
        return tokenPass;
    }
    public String getBaseUrl() {
        return baseUrl;
    }
    public String getSecuritySubscriptionKey() {
        return securitySubscriptionKey;
    }
    public String getUrlDataBase() {
        return urlDataBase;
    }
    public String getUserDataBase() {
        return userDataBase;
    }
    public String getPassDataBase() {
        return passDataBase;
    }


}