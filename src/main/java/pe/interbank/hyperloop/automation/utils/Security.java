package pe.interbank.hyperloop.automation.utils;

import io.restassured.http.ContentType;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;

import static io.restassured.RestAssured.given;

public class Security {

    private static EnvironmentVariables environmentVariables;

    public static String createAccessToken() {
        String body = "{\"username\":\"XT8052\",\"password\":\"Interbank$2\"}";
        try {
            return "Bearer " + given().header("Authorization", "Basic dXhyZmFzc2k6V0QyVzNFNUxVdmpt")
                    .contentType(ContentType.JSON)
                    .header("ocp-apim-subscription-key", "2bf94a64237c45f7a88b865777de9b69")
                    .body(body)
                    .post( "https://eu2-ibk-apm-uat-ext-001.azure-api.net/api/uat/login")
                    .then()
                    .extract().response().jsonPath().getString("access_token");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
