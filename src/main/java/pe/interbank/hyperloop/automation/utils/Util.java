package pe.interbank.hyperloop.automation.utils;

import org.apache.commons.io.IOUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class Util {

    public static WebElement expandRootElement(WebElement element) {
        return (WebElement) ((JavascriptExecutor)getDriver()).executeScript("return arguments[0].shadowRoot", element);
    }

    public static void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void scrollToElementVisible(WebElement element, String pixels) {
        try {
            do {
                ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy(0, "+pixels+")");
            } while (!isElementVisible(element));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean isElementVisible(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public static String getCurrentDateFormatted() {
         DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.278Z'");
         Date date = new Date();
         return dateFormat.format(date);
    }

    public static String consultaExpediente(String numeroExpediente) {
        String expedientStatus = "";
        String historyExpedientStatus = "";
        String productApplicationStatus = "";
        String resultExpediente = "";

        String connectionUrl = "jdbc:sqlserver://asqleu2c001assiuat01.database.windows.net:1433;databaseName=dsqleu2c001assiuat01;user=admindb;password=Peru123.;";

        try(Connection conn = DriverManager.getConnection(connectionUrl)) {

            String Expedient = "SELECT status FROM Expedient.Expedient WHERE id = '"+numeroExpediente+"'";
            ResultSet rs1 = DataBase.getResult(Expedient, conn);
            while (rs1.next()) {
                expedientStatus = rs1.getString("status");
            }

            String HistoryExpedient = "SELECT TOP 1 status from Expedient.HistoryExpedient WHERE expedientId = '"+numeroExpediente+"'";
            ResultSet rs2 = DataBase.getResult(HistoryExpedient, conn);
            while (rs2.next()) {
                historyExpedientStatus = rs2.getString("status");
            }

            String ProductApplication = "SELECT TOP 1 status from Expedient.ProductApplication WHERE expedientId = '"+numeroExpediente+"'";
            ResultSet rs3 = DataBase.getResult(ProductApplication, conn);
            while (rs3.next()) {
                productApplicationStatus = rs3.getString("status");
            }

            if(expedientStatus.equals("CERRADO") && historyExpedientStatus.equals("CERRADO") && productApplicationStatus.equals("CERRADO")){
                resultExpediente = "CERRADO";
            }else{
                resultExpediente = "CREADO";
            }

            conn.close();

            return resultExpediente;
        } catch (Exception e) {
            e.getMessage();
            return "No se obtuvo el Status del Expediente";
        }
    }

    public static String consultaExpedienteHyperloop(String numeroDocumento) {
        String idPerson = "";
        String numeroExpediente = "";
        String expedientStatus = "";
        String historyExpedientStatus = "";
        String productApplicationStatus = "";
        String resultExpediente = "";

        String connectionUrl = "jdbc:sqlserver://asqleu2c001assiuat01.database.windows.net:1433;databaseName=dsqleu2c001assiuat01;user=admindb;password=Peru123.;";

        try(Connection conn = DriverManager.getConnection(connectionUrl)) {

            String Person = "SELECT id FROM Expedient.Person WHERE documentNumber = '"+numeroDocumento+"'";
            ResultSet rsperson = DataBase.getResult(Person, conn);
            while (rsperson.next()) {
                idPerson = rsperson.getString("id");
            }

            String PersonExpedient = "SELECT top 1 expedientId FROM Expedient.PersonExpedient WHERE personId = '"+idPerson+"' ORDER BY createdOn DESC";
            ResultSet rsExpedient = DataBase.getResult(PersonExpedient, conn);
            while (rsExpedient.next()) {
                numeroExpediente = rsExpedient.getString("expedientId");
            }

            String Expedient = "SELECT status FROM Expedient.Expedient WHERE id = '"+numeroExpediente+"'";
            ResultSet rs1 = DataBase.getResult(Expedient, conn);
            while (rs1.next()) {
                expedientStatus = rs1.getString("status");
            }

            String HistoryExpedient = "SELECT TOP 1 status from Expedient.HistoryExpedient WHERE expedientId = '"+numeroExpediente+"'";
            ResultSet rs2 = DataBase.getResult(HistoryExpedient, conn);
            while (rs2.next()) {
                historyExpedientStatus = rs2.getString("status");
            }

            String ProductApplication = "SELECT TOP 1 status from Expedient.ProductApplication WHERE expedientId = '"+numeroExpediente+"'";
            ResultSet rs3 = DataBase.getResult(ProductApplication, conn);
            while (rs3.next()) {
                productApplicationStatus = rs3.getString("status");
            }

            if(expedientStatus.equals("CERRADO") && historyExpedientStatus.equals("CERRADO") && productApplicationStatus.equals("CERRADO")){
                resultExpediente = "CERRADO";
            }else{
                resultExpediente = "CREADO";
            }

            conn.close();

            return resultExpediente;
        } catch (Exception e) {
            e.getMessage();
            return "No se obtuvo el Status del Expediente";
        }
    }
    public static String getTemplate(String templatePath) {
        try {
            return IOUtils.toString(new ClassPathResource(templatePath).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
