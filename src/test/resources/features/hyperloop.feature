# language: es
@regresion-hyperloop
Característica: CUENTA NEGOCIO - HYPERLOOP

  @empresa-constituida
  @plan-digital-soles-hyperloop
  @CH001
  Escenario: FLUJO CLIENTE CON MONEDA SOLES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo CLIENTE ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento |  Email  | Operador  | Celular   |
      | CONSTITUIDA | 20259249679 | DNI             | 00540289         |  EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario selecciona ¿A quién le vendes? e ingresa el numero de partida registral
      | Ventas | Numero Partida Registral | Rubro |  Actividad |
      | Empresas | 00540289                 | Servicios y/o ventas minoristas |  Distribución, suministros ó mercancías  |
    * el usuario selecciona un plan tarifario
      | Moneda  | Plan    | Tienda Departamento | Tienda Distrito |
      | SOLES   | DIGITAL | Huanuco             | Huanuco         |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-constituida
  @plan-tradicional-dolares-hyperloop
  @CH002
  Escenario: FLUJO CLIENTE CON MONEDA DOLARES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo CLIENTE ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | CONSTITUIDA | 20528085378 | CE              | CE010107         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario selecciona ¿A quién le vendes? e ingresa el numero de partida registral
      | Ventas | Numero Partida Registral | Rubro |  Actividad |
      | Empresas | 00642289                 | Servicios y/o ventas minoristas |  Distribución, suministros ó mercancías  |
    * el usuario selecciona un plan tarifario
      | Moneda  | Plan        | Tienda Departamento | Tienda Distrito |
      | DOLARES | TRADICIONAL | La Libertad         | Trujillo         |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-constituida
  @plan-tradicional-soles-hyperloop
  @CH003
  @DataNueva
  Escenario: FLUJO CLIENTE CON MONEDA SOLES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo NO CLIENTE ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | CONSTITUIDA | 20552335431 | DNI             | 00540289         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario ingresa los datos de la empresa
      | Razon social                      | Actividad                                           | Numero de Partida Registral | Tipo de contribuyente | Ventas   | Correo electronico de la empresa | Departamento | Provincia | Distrito    | Tipo de via | Nombre de via | Numero |
      | CODRALUX S.A. SUCURSAL DEL PERU   | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | 523432234                   | (SA) Sociedad anonima | Empresas | EVERIS.QA.TESTING@GMAIL.COM      | LA LIBERTAD  |  TRUJILLO | EL PORVENIR | AVENIDA     | LOS PORTALES  | 163    |
    * el usuario selecciona un plan tarifario
      | Moneda  | Plan        | Tienda Departamento | Tienda Distrito |
      | SOLES   | TRADICIONAL | Tacna               | Tacna           |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-constituida
  @plan-tradicional-bimoneda-hyperloop
  @CH004
  Escenario: FLUJO NO CLIENTE CON MONEDA BIMONEDA EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo NO CLIENTE ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | CONSTITUIDA | 20259249679 | DNI             | 49990343         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario ingresa los datos del representante legal
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Sexo      |Email                       | Operador | Celular    |
      | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | MASCULINO |EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario ingresa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | LIMA     | AVENIDA  | MEXICO     | 545    |
    * el usuario selecciona ¿A quién le vendes? e ingresa el numero de partida registral
      | Ventas    | Numero Partida Registral |
      | Empresas  | 00540289                 |
    * el usuario selecciona un plan tarifario
      | Moneda    | Plan        | Tienda Departamento | Tienda Distrito |
      | BIMONEDA  | TRADICIONAL | Arequipa            | Arequipa        |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-constituida
  @plan-tradicional-dolares-hyperloop
  @CH005
  Escenario: FLUJO NO CLIENTE CON MONEDA DOLARES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo NO CLIENTE ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | CONSTITUIDA | 20552978430 | DNI             | 49990344         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario ingresa los datos del representante legal
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Sexo      |Email                       | Operador | Celular    |
      | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | MASCULINO |EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario ingresa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | LIMA     | AVENIDA  | MEXICO     | 545    |
    * el usuario ingresa los datos de la empresa
      | Razon social                      | Actividad                                           | Numero de Partida Registral | Tipo de contribuyente | Ventas   | Correo electronico de la empresa | Departamento | Provincia | Distrito    | Tipo de via | Nombre de via | Numero |
      | CODRALUX S.A. SUCURSAL DEL PERU   | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | 523432234                   | (SA) Sociedad anonima | PERSONAS | EVERIS.QA.TESTING@GMAIL.COM      | LA LIBERTAD  |  TRUJILLO | EL PORVENIR | AVENIDA     | LOS PORTALES  | 163    |
    * el usuario selecciona un plan tarifario
      | Moneda  | Plan        | Tienda Departamento | Tienda Distrito |
      | DOLARES | TRADICIONAL | Tumbes              | Tumbes          |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-constituida
  @plan-digital-dolares-hyperloop
  @CH006
  Escenario: FLUJO POTENCIAL CON MONEDA DOLARES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo POTENCIAL ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | CONSTITUIDA | 20259249679 | DNI             | 49990322         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario ingresa los datos del representante legal
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Sexo      |Email                       | Operador | Celular    |
      | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | MASCULINO |EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario ingresa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | LIMA     | AVENIDA  | MEXICO     | 545    |
    * el usuario selecciona ¿A quién le vendes? e ingresa el numero de partida registral
      | Ventas    | Numero Partida Registral |
      | Empresas  | 00540289                 |
    * el usuario selecciona un plan tarifario
      | Moneda    | Plan        | Tienda Departamento | Tienda Distrito |
      | DOLARES   | DIGITAL     | Moquegua            | Ilo             |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-constituida
  @plan-tradicional-soles-hyperloop
  @CH007
  Escenario: FLUJO POTENCIAL CON MONEDA SOLES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo POTENCIAL ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | CONSTITUIDA | 20515690086 | DNI             | 49990343         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario ingresa los datos del representante legal
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Sexo      |Email                       | Operador | Celular    |
      | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | MASCULINO |EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario ingresa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | LIMA     | AVENIDA  | MEXICO     | 545    |
    * el usuario ingresa los datos de la empresa
      | Razon social                      | Actividad                                           | Numero de Partida Registral | Tipo de contribuyente | Ventas   | Correo electronico de la empresa | Departamento | Provincia | Distrito    | Tipo de via | Nombre de via | Numero |
      | CODRALUX S.A. SUCURSAL DEL PERU   | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | 523432234                   | (SA) Sociedad anonima | PERSONAS | EVERIS.QA.TESTING@GMAIL.COM      | LA LIBERTAD  |  TRUJILLO | EL PORVENIR | AVENIDA     | LOS PORTALES  | 163    |
    * el usuario selecciona un plan tarifario
      | Moneda  | Plan      | Tienda Departamento | Tienda Distrito |
      | SOLES | TRADICIONAL | Pasco               | Pasco           |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-no-constituida
  @plan-digital-soles-hyperloop
  @CH008
  Escenario: FLUJO CLIENTE CON MONEDA SOLES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO NO CLIENTE NO CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo CLIENTE ingresa sus datos
      | Empresa        | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | NO CONSTITUIDA | 20515644050 | DNI             | 00540289         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario ingresa los datos de la empresa
      | Razon social                      | Actividad                                           | Numero de Partida Registral | Tipo de contribuyente | Ventas   | Correo electronico de la empresa | Departamento | Provincia | Distrito    | Tipo de via | Nombre de via | Numero |
      | CODRALUX S.A. SUCURSAL DEL PERU   | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | 523432234                   | (SA) Sociedad anonima | PERSONAS | EVERIS.QA.TESTING@GMAIL.COM      | LA LIBERTAD  |  TRUJILLO | EL PORVENIR | AVENIDA     | LOS PORTALES  | 163    |
    * el usuario selecciona un plan tarifario
      | Moneda  | Plan     | Tienda Departamento | Tienda Distrito |
      | SOLES   | DIGITAL  | Piura               | Piura           |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-no-constituida
  @plan-tradicional-bimoneda-hyperloop
  @CH009
  Escenario: FLUJO NO CLIENTE CON MONEDA BIMONEDA EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE NO CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo NO CLIENTE ingresa sus datos
      | Empresa        | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | NO CONSTITUIDA | 20515633872 | CE              | 00540295         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario ingresa los datos del representante legal
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Sexo      |Email                       | Operador | Celular    |
      | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | MASCULINO |EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario ingresa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | LIMA     | AVENIDA  | MEXICO     | 545    |
    * el usuario ingresa los datos de la empresa
      | Razon social                      | Actividad                                           | Numero de Partida Registral | Tipo de contribuyente | Ventas   | Correo electronico de la empresa | Departamento | Provincia | Distrito    | Tipo de via | Nombre de via | Numero |
      | CODRALUX S.A. SUCURSAL DEL PERU   | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | 523432234                   | (SA) Sociedad anonima | PERSONAS | EVERIS.QA.TESTING@GMAIL.COM      | LA LIBERTAD  |  TRUJILLO | EL PORVENIR | AVENIDA     | LOS PORTALES  | 163    |
    * el usuario selecciona un plan tarifario
      | Moneda    | Plan        | Tienda Departamento | Tienda Distrito |
      | BIMONEDA  | TRADICIONAL | Piura               | Paita           |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-no-constituida
  @plan-tradicional-soles-hyperloop
  @CH0010
  Escenario: FLUJO CLIENTE CON MONEDA SOLES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE NO CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo CLIENTE ingresa sus datos
      | Empresa        | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | NO CONSTITUIDA | 20515614738 | CE              | 00540289         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario ingresa los datos de la empresa
      | Razon social                      | Actividad                                           | Numero de Partida Registral | Tipo de contribuyente | Ventas   | Correo electronico de la empresa | Departamento | Provincia | Distrito    | Tipo de via | Nombre de via | Numero |
      | CODRALUX S.A. SUCURSAL DEL PERU   | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | 523432234                   | (SA) Sociedad anonima | PERSONAS | EVERIS.QA.TESTING@GMAIL.COM      | LA LIBERTAD  |  TRUJILLO | EL PORVENIR | AVENIDA     | LOS PORTALES  | 163    |
    * el usuario selecciona un plan tarifario
      | Moneda | Plan        |  Tienda Departamento | Tienda Distrito |
      | SOLES  | TRADICIONAL |  Piura               | Sullana         |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-no-constituida
  @plan-digital-bimoneda-hyperloop
  @CH0011
  Escenario: FLUJO NO CLIENTE CON MONEDA BIMONEDA EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO NO CLIENTE NO CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo NO CLIENTE ingresa sus datos
      | Empresa        | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | NO CONSTITUIDA | 20551578551 | CE              | 00540298         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario ingresa los datos del representante legal
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Sexo      |Email                       | Operador | Celular    |
      | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | MASCULINO |EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario ingresa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | LIMA     | AVENIDA  | MEXICO     | 545    |
    * el usuario ingresa los datos de la empresa
      | Razon social                      | Actividad                                           | Numero de Partida Registral | Tipo de contribuyente | Ventas   | Correo electronico de la empresa | Departamento | Provincia | Distrito    | Tipo de via | Nombre de via | Numero |
      | CODRALUX S.A. SUCURSAL DEL PERU   | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | 523432234                   | (SA) Sociedad anonima | PERSONAS | EVERIS.QA.TESTING@GMAIL.COM      | LA LIBERTAD  |  TRUJILLO | EL PORVENIR | AVENIDA     | LOS PORTALES  | 163    |
    * el usuario selecciona un plan tarifario
      | Moneda    | Plan     | Tienda Departamento | Tienda Distrito |
      | BIMONEDA  | DIGITAL  | Tumbes              | Zarumilla       |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-constituida
  @plan-digital-bimoneda-hyperloop
  @CH0012
  Escenario: FLUJO CLIENTE CON MONEDA BIMONEDA EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo CLIENTE ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | CONSTITUIDA | 20259249679 | DNI             | 00540289         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario selecciona ¿A quién le vendes? e ingresa el numero de partida registral
      | Ventas    | Numero Partida Registral |
      | Empresas  | 00540289                 |
    * el usuario selecciona un plan tarifario
      | Moneda    | Plan    | Tienda Departamento | Tienda Distrito |
      | BIMONEDA  | DIGITAL | Ancash              | Santa           |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-constituida
  @plan-tradicional-soles-hyperloop
  @CH0013
  Escenario: FLUJO CLIENTE CON MONEDA SOLES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo CLIENTE ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | CONSTITUIDA | 20259249679 | DNI             | 00540289         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario selecciona ¿A quién le vendes? e ingresa el numero de partida registral
      | Ventas    | Numero Partida Registral |
      | Empresas  | 00540289                 |
    * el usuario selecciona un plan tarifario
      | Moneda | Plan        | Tienda Departamento | Tienda Distrito |
      | SOLES  | TRADICIONAL | Ucayali             | Pucallpa        |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente

  @empresa-constituida
  @plan-tradicional-dolares-hyperloop
  @CH0014
  Escenario: FLUJO CLIENTE CON MONEDA DOLARES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud
    * el usuario de tipo CLIENTE ingresa sus datos
      | Empresa     | RUC         | Tipo Documento  | Numero Documento | Email                        | Operador  | Celular   |
      | CONSTITUIDA | 20259249679 | DNI             | 00540289         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario selecciona ¿A quién le vendes? e ingresa el numero de partida registral
      | Ventas    | Numero Partida Registral |
      | Empresas  | 00540289                 |
    * el usuario selecciona un plan tarifario
      | Moneda  | Plan        | Tienda Departamento | Tienda Distrito |
      | DOLARES | TRADICIONAL | Ancash              |Huaraz           |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente
