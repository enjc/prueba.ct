# language: es
@regresion-hyperloopTDM
Característica: CUENTA NEGOCIO - HYPERLOOP TDM


  @CH001_TDM
  Escenario: FLUJO CLIENTE CON MONEDA SOLES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA
    Dado que el usuario apertura de una cuenta negocio
    Cuando realiza la solicitud de CN
    * el usuario de tipo CLIENTE ingresa sus datos desde TDM
      |Empresa    | Tipo Documento  | Numero Documento |  Email  | Operador  | Celular   |
      |CONSTITUIDA | DNI             | 00540289         |  EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR  | 942126940 |
    * el usuario valida su identidad en EQUIFAX
      | Tarjeta de Credito Activa | Nombre Padre/Madre | Nombre Hijo/Hija | Nombre Hermano/Hermana | Nombre Abuelo/Abuela | Credito Hipotecario | Casado(a) | Credito Vehicular | Prestamo Personal | Ha tenido Carro | Representa Empresa Lima/Provincia | Representante Empresa | Direccion Correspondencia | Empresa Trabaja |
      | N/A                       | N/A                | N/A              | N/A                    | N/A                  | NO                  | N/A       | NO                | NO                | N/A             | NO ES REPRESENTANTE               | N/A                   | N/A                       | N/A             |
    * el usuario selecciona ¿A quién le vendes? e ingresa el numero de partida registral
      | Ventas | Numero Partida Registral | Rubro |  Actividad |
      | Empresas | 00540289                 | Alquileres |  Alquiler de autos  |
    * el usuario ingresa los socios
      | Tipo Documento  | Numero Documento | Tipo Documento  | Numero Documento |
      | DNI             | 02548789         |DNI              | 01258985       |
    * el usuario selecciona un plan tarifario
      | Moneda  | Plan    | Tienda Departamento | Tienda Distrito |
      | SOLES   | DIGITAL | Huanuco             | Huanuco         |
    * el usuario selecciona un seguro
      | Seguro  |
      | Plan Familiar   |
    Entonces el usuario abre la cuenta negocio
    * el usuario verifica que la cuenta negocio se genero exitosamente
