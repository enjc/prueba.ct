# language: es
@regresion-hyperloop-CC-Autorizacion
Característica: CUENTA CORRIENTE - HYPERLOOP

  @CC-DosRepresentantesLegales_CC
  @CC-Clientes_CC
  @CCA001
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales_CC
  @CCA002
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491          |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-1RepresentanteLegale_CC
  @CCA003
  Escenario: FLUJO RL1 CLIENTE  MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | CE            | 001559271        |


  @CC-DosRepresentantesLegales
  @CC004
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC005
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC006
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC007
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC-NoClientes
  @CC008
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC-Clientes
  @CC009
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC010
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC011
  Escenario: FLUJO RL1 CLIENTE Y RL2 NO CLIENTE CON MONEDA DOLARES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC012
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA DOLARES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC013
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA DOLARES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC014
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA DOLARES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC015
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que los RLs quieren autorizar la reserva
    Cuando ingresa al link de la reserva
    Y el RL1 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 72877043         |
    Y ingresa al link de la reserva
    Y el RL2 ingresa los datos
      | Tipo Documento |  Numero Documento |
      | DNI             | 46605491         |

    Entonces   se realiza la autorización para la apertura de cuenta corriente


