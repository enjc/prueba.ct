# language: es
@regresion-hyperloop-CC-Reserva
Característica: CUENTA CORRIENTE - HYPERLOOP

  @CC-DosRepresentantesLegales
  @CC-Clientes
  @CC001
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 46605491         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LORETO               | MAYNAS            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC002
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20452826276 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223301         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223302         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos de la empresa
      | Razon Social                | Actividad Economica                                 | Tipo de Contribuyente  |
      | Inversiones ALAMOS DEL SUR  | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | HUAURA            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC003
  Escenario: FLUJO RL1 CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20111000019 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  45563300         | Raul       |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | CE             | A550014          |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770001         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | FERNANDO      | DAMIAN         | ROJAS            | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC004
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  75646490         | Omar       |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | CE              | A550012          |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770011         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | HUGO          | DAMIAN         | SOTO             | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador completa los datos de la empresa
      | Razon Social            | Actividad Economica                                 | Tipo de Contribuyente  |
      | Inversiones AutoGarra  | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | MADRE DE DIOS        | TAMBOPATA         |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC005
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20527663033 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  98663300         | Romina     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | PABLO         | LUIS           | SOTELO           | LEON             | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223302         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC006
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Norma      |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770005         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | PABLO         | LUIS           | SOTELO           | LEON             | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223303         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos de la empresa
      | Razon Social            | Actividad Economica                                 | Tipo de Contribuyente  |
      | Reparaciones Autocia    | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC007
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770001         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | HUGO          | DAMIAN         | SOTO             | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | ICA                  | NAZCA             |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC-NoClientes
  @CC008
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770001         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | HUGO          | DAMIAN         | SOTO             | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador completa los datos de la empresa
      | Razon Social            | Actividad Economica                                 | Tipo de Contribuyente  |
      | Inversiones AutoGarcia  | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      | Moneda   | Estado de Cuenta |
      | SOLES    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | MOQUEGUA             | ILO               |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC-Clientes
  @CC009
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223301         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223302         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | AYACUCHO             | HUAMANGA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC010
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20452826276 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223301         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | CE              | A550010          |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos de la empresa
      | Razon Social                | Actividad Economica                                 | Tipo de Contribuyente  |
      | Inversiones ALAMOS DEL SUR  | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | LIMA              |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC011
  Escenario: FLUJO RL1 CLIENTE Y RL2 NO CLIENTE CON MONEDA DOLARES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20111000019 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  45563300         | Raul       |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223311         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770007         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | FERNANDO      | DAMIAN         | ROJAS            | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | SAN MARTIN           | MOYOBAMBA         |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC012
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA DOLARES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  78646490         | Talia      |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223315         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770016         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | TOMAS         | ROBERTO        | LOPEZ             | TRINIDAD        | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador completa los datos de la empresa
      | Razon Social            | Actividad Economica                                 | Tipo de Contribuyente  |
      | Inversiones AutoGarra  | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | MADRE DE DIOS        | TAMBOPATA         |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC013
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA DOLARES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20527663033 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  98555300         | Romina     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | PABLO         | LUIS           | SOTELO           | LEON             | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223302         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC014
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA DOLARES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | PABLO         | LUIS           | SOTELO           | LEON             | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223300         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos de la empresa
      | Razon Social            | Actividad Economica                                 | Tipo de Contribuyente  |
      | Reparaciones Autocia    | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC015
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770001         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | HUGO          | DAMIAN         | SOTO             | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | JUNIN                | SATIPO            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC-NoClientes
  @CC016
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 NO CLIENTE CON MONEDA DOLARES PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770001         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | HUGO          | DAMIAN         | SOTO             | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador completa los datos de la empresa
      | Razon Social            | Actividad Economica                                 | Tipo de Contribuyente  |
      | Inversiones AutoGarcia  | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | PUNO                 | SAN ROMAN         |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC-Clientes
  @CC017
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES Y DOLARES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223301         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223302         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | AREQUIPA             | CAMANA            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC018
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES Y DOLARES PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20452826276 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | CE              | A550011          |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | CE              | A550010          |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos de la empresa
      | Razon Social                | Actividad Economica                                 | Tipo de Contribuyente  |
      | Inversiones ALAMOS DEL SUR  | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | LIMA              |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC019
  Escenario: FLUJO RL1 CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES Y DOLARES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20111000019 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  45563300         | Raul       |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223311         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770007         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | FERNANDO      | DAMIAN         | ROJAS            | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | SAN MARTIN           | MOYOBAMBA         |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC020
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA SOLES Y DOLARES PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  75646490         | Gloria     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223315          |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770011         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | HUGO          | DAMIAN         | SOTO             | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador completa los datos de la empresa
      | Razon Social            | Actividad Economica                                 | Tipo de Contribuyente  |
      | Socios AutoGarra  | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | MADRE DE DIOS        | TAMBOPATA         |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC021
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA SOLES Y DOLARES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20527663033 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  98555300         | Romina     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | PABLO         | LUIS           | SOTELO           | LEON             | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | CE              | A550013          |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC022
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 CLIENTE CON MONEDA SOLES Y DOLARES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | PABLO         | LUIS           | SOTELO           | LEON             | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223300         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos de la empresa
      | Razon Social            | Actividad Economica                                 | Tipo de Contribuyente  |
      | Reparaciones Autocia    | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC023
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES Y DOLARES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  89223300         | Roberto    |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | JOAN          | MANUEL         | TORRES           | DE LA CRUZ       | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 47010983         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | KELLY         | LILIANA        | CHACON           | SOTO             | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | JUNIN                | SATIPO            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-DosRepresentantesLegales
  @CC-NoClientes
  @CC024
  Escenario: FLUJO RL1 NO CLIENTE Y RL2 NO CLIENTE CON MONEDA SOLES Y DOLARES  PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  59223300         | Maria      |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770001         |  EVERIS.MAIL.BOX@GMAIL.COM  | 989491752 |
    Y el operador completa los datos personales del RL2
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | HUGO          | DAMIAN         | SOTO             | CACEDA           | 25051993         | HOMBRE | CASADO       | CLARO     |
    Y el operador completa los datos de dirección del RL2
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | ATE      | CALLE    | LOS CEREZOS| 123    |
    Y el operador completa los datos de la empresa
      | Razon Social                 | Actividad Economica                                 | Tipo de Contribuyente  |
      | Reparaciones Garcia Hermanos | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | PIURA                | TALARA            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC-Clientes
  @CC025
  Escenario: FLUJO RL1 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  66223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223320         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LORETO               | MAYNAS            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC-Clientes
  @CC026
  Escenario: FLUJO RL1 CLIENTE CON MONEDA DOLARES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  66223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223320         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | LIMA              |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC-Clientes
  @CC027
  Escenario: FLUJO RL1 CLIENTE CON MONEDA SOLES Y DOLARES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  66223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 46944242         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | PUNO                 | PUNO              |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC028
  Escenario: FLUJO RL1 CLIENTE CON MONEDA SOLES PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  75646490         | Gloria     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 46944242         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador completa los datos de la empresa
      | Razon Social        | Actividad Economica                                 | Tipo de Contribuyente  |
      | Socios AutoGarra II | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | MADRE DE DIOS        | TAMBOPATA         |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC029
  Escenario: FLUJO RL1 CLIENTE CON MONEDA DOLARES PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  75646490         | Gloria     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223312         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador completa los datos de la empresa
      | Razon Social        | Actividad Economica                                 | Tipo de Contribuyente  |
      | Socios AutoGarra    | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | MADRE DE DIOS        | TAMBOPATA         |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC030
  Escenario: FLUJO RL1 CLIENTE CON MONEDA SOLES Y DOLARES PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  75646490         | Gloria     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 99223316         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador completa los datos de la empresa
      | Razon Social        | Actividad Economica                                 | Tipo de Contribuyente  |
      | Socios AutoGarra    | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | MADRE DE DIOS        | TAMBOPATA         |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC-NoClientes
  @CC031
  Escenario: FLUJO RL1 NO CLIENTE CON MONEDA SOLES PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | JOAN          | MANUEL         | TORRES           | DE LA CRUZ       | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador completa los datos de la empresa
      | Razon Social                | Actividad Economica                                 | Tipo de Contribuyente  |
      | Reparaciones Bravo Verde    | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC-NoClientes
  @CC032
  Escenario: FLUJO RL1 NO CLIENTE CON MONEDA DOLARES PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | PABLO         | LUIS           | SOTELO           | LEON             | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador completa los datos de la empresa
      | Razon Social                | Actividad Economica                                 | Tipo de Contribuyente  |
      | Reparaciones Bravo Verde    | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC-NoClientes
  @CC033
  Escenario: FLUJO RL1 NO CLIENTE CON MONEDA SOLES Y DOLARES PARA UNA EMPRESA DE TIPO NO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20571131464 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 54770000         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | PABLO         | LUIS           | SOTELO           | LEON             | 25051993         | HOMBRE | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador completa los datos de la empresa
      | Razon Social                | Actividad Economica                                 | Tipo de Contribuyente  |
      | Reparaciones Bravo Verde    | CULTIVO DE CEREALES Y OTROS CULTIVOS N.C.P. (0111)  | (SA) Sociedad anonima  |
    Y el operador adjunta el certificado de vigencia de poder en PDF para Empresa No cliente
    Y el operador adjunta la copia de estatuto vigente en PDF
    Y el operador completa los datos de dirección de la empresa
      | Departamento | Provincia    | Distrito     | Tipo Via   | Nombre Via | Numero |
      | AMAZONAS     | CHACHAPOYAS  | CHACHAPOYAS  | AVENIDA    | LOS CEREZOS| 123    |
    Y el operador ingresa los datos de celular y correo de la empresa
      | Celular   | Email                      |
      | 989491752 | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC034
  Escenario: FLUJO RL1 NO CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20528234985 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 47010983         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | KELLY         | LILIANA        | CHACON           | SOTO             | 25051993         | MUJER  | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC035
  Escenario: FLUJO RL1 NO CLIENTE CON MONEDA DOLARES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA FISICO
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20528234985 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 47010983         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | KELLY         | LILIANA        | CHACON           | SOTO             | 25051993         | MUJER  | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES  | FISICO           |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC-UnRepresentanteLegal
  @CC036
  Escenario: FLUJO RL1 NO CLIENTE CON MONEDA SOLES Y DOLARES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud
    Y el operador ingresa los datos de la empresa
      | RUC         |
      | 20528234985 |
    Y el operador ingresa sus datos personales
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 47010983         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador completa los datos personales del RL1
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Operador  |
      | KELLY         | LILIANA        | CHACON           | SOTO             | 25051993         | MUJER  | SOLTERO      | CLARO     |
    Y el operador completa los datos de dirección del RL1
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |AMBAS    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | EVERIS.MAIL.BOX@GMAIL.COM  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LIMA                 | BARRANCA          |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente