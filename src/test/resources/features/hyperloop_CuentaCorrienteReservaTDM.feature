# language: es
@regresion-hyperloop-CC-TDM
Característica: CUENTA CORRIENTE - HYPERLOOP TDM


  @CC001_CC
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud CC
    Y el operador ingresa los datos de la empresa desde TDM_CC
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales desde TDM
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 46605491         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL2
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LORETO               | MAYNAS            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC002_CC
  Escenario: FLUJO RL1 CLIENTE CON MONEDA SOLES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud CC
    Y el operador ingresa los datos de la empresa desde TDM_CC
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales desde TDM
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador adjunta el certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LA LIBERTAD               | TRUJILLO            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC003_CC
  Escenario: FLUJO RL1 CLIENTE CON MONEDA SOLES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
            (NO ADJUNTA CERtIFICADO PDF) - PARA RIVIEW OK1
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud CC
    Y el operador ingresa los datos de la empresa desde TDM_CC
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales desde TDM
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador no adjunta  certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |DOLARES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LORETO               | MAYNAS            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC004_CC
  Escenario: FLUJO RL1 CLIENTE CON MONEDA SOLES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
  (NO ADJUNTA CERtIFICADO PDF) - PARA RIVIEW OK3
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud CC
    Y el operador ingresa los datos de la empresa desde TDM_CC
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales desde TDM
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador no adjunta  certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta 2
      |Moneda   | Estado de Cuenta |
      |DOLARES    | FISICO          |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LORETO               | MAYNAS2            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC005_CC
  Escenario: FLUJO RL1 CLIENTE CON MONEDA SOLES PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
  (NO ADJUNTA CERtIFICADO PDF) - PARA RIVIEW OK2
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud CC
    Y el operador ingresa los datos de la empresa desde TDM_CC
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales desde TDM
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que si tiene facultades para aperturar cuenta a sola firma
    Y el operador no adjunta  certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LORETO               | MAYNAS            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC006_CC
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud CC
    Y el operador ingresa los datos de la empresa desde TDM_CC
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales desde TDM
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 46605491         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador no adjunta  certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | DIGITAL          |
    Y el operador ingresa el correo para recibir el estado de cuenta
      | Email                      |
      | everis.mail.box@gmail.com  |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LORETO               | MAYNAS            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente

  @CC007_CC
  Escenario: FLUJO RL1 CLIENTE Y RL2 CLIENTE CON MONEDA SOLES  PARA UNA EMPRESA DE TIPO CLIENTE CON ENVÍO DE ESTADO DE CUENTA DIGITAL
    Dado que el operador desea reservar la solicitud de una cuenta corriente
    Cuando ingresa a la pagina a realizar la solicitud CC
    Y el operador ingresa los datos de la empresa desde TDM_CC
      | RUC         |
      | 20259249679 |
    Y el operador ingresa sus datos personales desde TDM
      | Tipo Documento |  Numero Documento | Nombres    |  Apellidos      | Email                     |
      | DNI            |  99223300         | Alicia     |  Rojas Diaz     | everis.mail.box@gmail.com |
    Y el operador ingresa los datos personales del RL1
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 72877043         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador selecciona que no tiene facultades para aperturar cuenta a sola firma
    Y el operador ingresa los datos personales del RL2
      | Tipo Documento  | Numero Documento |  Email                      | Celular   |
      | DNI             | 46605491         |  everis.mail.box@gmail.com  | 989491752 |
    Y el operador no adjunta  certificado de vigencia de poder en PDF del RL1
    Y el operador elije la moneda y el envío de estado de cuenta
      |Moneda   | Estado de Cuenta |
      |SOLES    | FISICO          |
    Y el operador selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Agencia Provincia |
      | LORETO               | MAYNAS2            |

    Entonces el operador realiza la solicitud para la apertura de cuenta corriente