# language: es
@regresionOrquestadorCN
Característica: CUENTA NEGOCIO - ORQUESTADOR

  @Empresa-constituida-orquestador
  @Empresa-no-cliente-orquestador
  @ORQ001
  Esquema del escenario: FLUJO CON MONEDA SOLES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA CON RL NO CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan     |TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL | TipodocumentoRL | NumerodocumentoRL |
      | PEN    | DIGITAL  | NO_CLIENTE    | RUC           | 20323097128    | NO_CLIENTE    | DNI             | 89889062          |
      | PEN    | DIGITAL  | NO_CLIENTE    | RUC           | 20322919451    | NO_CLIENTE    | CE              | 89889063          |

  @Empresa-constituida-orquestador
  @Empresa-no-cliente-orquestador
  @ORQ002
  Esquema del escenario: FLUJO CON MONEDA DOLARES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA CON RL NO CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan     |TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | USD    | DIGITAL  | NO_CLIENTE    | RUC           | 20452534062    |   NO_CLIENTE  |DNI             | 89880062          |
      | USD    | DIGITAL  | NO_CLIENTE    | RUC           | 20452556546    |   NO_CLIENTE  |CE              | 89880063          |

  @Empresa-constituida-orquestador
  @Empresa-no-cliente-orquestador
  @ORQ003
  Esquema del escenario: FLUJO CON MONEDA SOLES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA CON RL NO CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan         |TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | PEN    | TRADICIONAL  | NO_CLIENTE    | RUC           | 20452565707    |    CLIENTE    |DNI             | 89880064          |
      | PEN    | TRADICIONAL  | NO_CLIENTE    | RUC           | 20452576148    |    CLIENTE    |CE              | 89880065          |

  @Empresa-constituida-orquestador
  @Empresa-no-cliente-orquestador
  @ORQ004
  Esquema del escenario: FLUJO CON MONEDA DOLARES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA CON RL NO CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan     |TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | USD    | DIGITAL  | NO_CLIENTE    | RUC           | 20452626935    |  NO_CLIENTE   |DNI             | 89880066          |
      | USD    | DIGITAL  | NO_CLIENTE    | RUC           | 20533683445    |  NO_CLIENTE   |CE              | 89880067          |

  @Empresa-constituida-orquestador
  @Empresa-no-cliente-orquestador
  @ORQ005
  Esquema del escenario: FLUJO CON AMBAS MONEDAS EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA CON RL NO CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan        | TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | AMBAS  | TRADICIONAL | NO_CLIENTE     | RUC           | 20166640068    | NO_CLIENTE    |DNI             | 89889064          |
      | AMBAS  | TRADICIONAL | NO_CLIENTE     | RUC           | 20232346613    | NO_CLIENTE    |CE              | 89889065          |

  @Empresa-constituida-orquestador
  @Empresa-no-cliente-orquestador
  @ORQ006
  Esquema del escenario: FLUJO CON AMBAS MONEDAS EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA CON RL NO CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan        | TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | AMBAS  | DIGITAL     | NO_CLIENTE     | RUC           | 20533901370    |  NO_CLIENTE   | DNI            | 89880070          |
      | AMBAS  | DIGITAL     | NO_CLIENTE     | RUC           | 20533934545    |  NO_CLIENTE   | CE             | 89880071          |

  @Empresa-no-constituida-orquestador
  @Empresa-no-cliente-orquestador
  @ORQ007
  Esquema del escenario: FLUJO CON MONEDA SOLES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO NO CONSTITUIDA CON RL NO CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando consumimos el servicio para empresa no constituida con los datos "<Moneda>","<Plan>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan       | TipopersonaRL | TipodocumentoRL | NumerodocumentoRL |
      | PEN    | DIGITAL    | NO_CLIENTE    |  DNI            |  89880072         |
      | PEN    | DIGITAL    | NO_CLIENTE    |  CE             |  89880073         |

  @Empresa-no-constituida-orquestador
  @Empresa-no-cliente-orquestador
  @ORQ008
  Esquema del escenario: FLUJO CON MONEDA DOLARES EN PLAN TRADICIONAL PARA UNA EMPRESA NO CONSTITUIDA CON RL NO CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio para empresa no constituida con los datos "<Moneda>","<Plan>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan        | TipopersonaRL | TipodocumentoRL | NumerodocumentoRL |
      | USD    | TRADICIONAL | NO_CLIENTE    |  DNI            |  89880074         |
      | USD    | TRADICIONAL | NO_CLIENTE    |  CE             |  89880075         |

  @Empresa-constituida-orquestador
  @Empresa-cliente-orquestador
  @ORQ009
  Esquema del escenario: FLUJO CON MONEDA SOLES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA CON RL CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan     |TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | PEN    | DIGITAL  | CLIENTE       | RUC           | 20533935517    | CLIENTE       | DNI            | 89880076          |
      | PEN    | DIGITAL  | CLIENTE       | RUC           | 20533946471    | CLIENTE       | CE             | 89880077          |

  @Empresa-constituida-orquestador
  @Empresa-cliente-orquestador
  @ORQ010
  Esquema del escenario: FLUJO CON MONEDA DOLARES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA CON RL CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan     |TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | USD    | DIGITAL  | CLIENTE       | RUC           | 20534197803    |  CLIENTE      | DNI            | 89880078          |
      | USD    | DIGITAL  | CLIENTE       | RUC           | 20494482011    |  CLIENTE      | CE             | 89880079          |

  @Empresa-constituida-orquestador
  @Empresa-cliente-orquestador
  @ORQ011
  Esquema del escenario: FLUJO CON AMBAS MONEDAS EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA CON RL CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan        |TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | AMBAS  | TRADICIONAL | CLIENTE       | RUC           | 20494438624    |  CLIENTE      | DNI            | 89880080          |
      | AMBAS  | TRADICIONAL | CLIENTE       | RUC           | 20534197803    |  CLIENTE      | CE             | 89880081          |

  @Empresa-constituida-orquestador
  @Empresa-rl-cliente-no-cliente-orquestador
  @ORQ012
  Esquema del escenario: FLUJO CON MONEDA SOLES EN PLAN DIGITAL PARA UNA EMPRESA DE TIPO CLIENTE CONSTITUIDA CON RL NO CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan     |TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | PEN    | DIGITAL  | CLIENTE       | RUC           | 20494554535    |  NO_CLIENTE   | DNI            | 89880082          |
      | PEN    | DIGITAL  | CLIENTE       | RUC           | 20534197803    |  NO_CLIENTE   | CE             | 89880083          |

  @Empresa-constituida-orquestador
  @Empresa-rl-cliente-no-cliente-orquestador
  @ORQ013
  Esquema del escenario: FLUJO CON MONEDA DOLARES EN PLAN TRADICIONAL PARA UNA EMPRESA DE TIPO NO CLIENTE CONSTITUIDA CON RL CLIENTE
    Dado que el cliente apertura una cuenta negocio
    Cuando  consumimos el servicio con los datos "<Moneda>","<Plan>","<Tipodocumento>","<Nrodocumento>","<TipopersonaEmp>","<TipopersonaRL>","<TipodocumentoRL>","<NumerodocumentoRL>"
    Entonces se apertura la cuenta negocio y la respuesta es 200.

    Ejemplos:
      | Moneda | Plan         |TipopersonaEmp | Tipodocumento | Nrodocumento   | TipopersonaRL |TipodocumentoRL | NumerodocumentoRL |
      | USD    | TRADICIONAL  | NO_CLIENTE    | RUC           | 20542057247    |    CLIENTE    | DNI            | 89880084          |
      | USD    | TRADICIONAL  | NO_CLIENTE    | RUC           | 20542092158    |    CLIENTE    | CE             | 89880085          |

