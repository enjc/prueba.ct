# language: es
@regresion_assi
Característica: APERTURA CUENTA NEGOCIO - ASSI

  @persona-juridica
  @plan-tradicional-soles
  @C001
  Escenario: FLUJO CLIENTE CON MONEDA SOLES EN PLAN TRADICIONAL
  Dado el usuario ingresa en ASSI
  * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
    | Tipo Documento  | Numero Documento |
    | RUC             | 20100002621      |
  Cuando selecciona abrir una cuenta negocio
  * el usuario completa los datos de la empresa
    | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
    | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
  * el usuario completa los datos del representate legal de tipo CLIENTE
    | Tipo Documento  | Numero Documento | Email                        | Operador | Celular    |
    | DNI             | 00540289         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR | 9542126940 |
  * el usuario selecciona un tarifario
    | Moneda  | Plan        |
    | SOLES   | TRADICIONAL |
  Entonces el usuario crea la cuenta negocio
  * el usuario verifica que la cuenta se genero exitosamente

  @persona-juridica
  @plan-digital-dolares
  @C002
  Escenario: FLUJO CLIENTE CON MONEDA DOLARES EN PLAN DIGITAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
      | Tipo Documento  | Numero Documento |
      | RUC             | 20304541980      |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa los datos de la empresa
      | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos del representate legal de tipo CLIENTE
      | Tipo Documento  | Numero Documento | Email                        | Operador | Celular    |
      | DNI             | 45455677         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR | 9542126940 |
    * el usuario selecciona un tarifario
      | Moneda  | Plan    |
      | DOLARES | DIGITAL |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-juridica
  @plan-digital-soles
  @C003
  Escenario: FLUJO CLIENTE CON MONEDA SOLES EN PLAN DIGITAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
      | Tipo Documento  | Numero Documento |
      | RUC             | 20304541980      |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa los datos de la empresa
      | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos del representate legal de tipo CLIENTE
      | Tipo Documento  | Numero Documento | Email                        | Operador | Celular    |
      | DNI             | 45455677         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR | 9542126940 |
    * el usuario selecciona un tarifario
      | Moneda  | Plan    |
      | SOLES   | DIGITAL |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-juridica
  @plan-tradicional-dolares
  @C004
  Escenario: FLUJO CLIENTE CON MONEDA DOLARES EN PLAN TRADICIONAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
      | Tipo Documento  | Numero Documento |
      | RUC             | 20304541980      |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa los datos de la empresa
      | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos del representate legal de tipo CLIENTE
      | Tipo Documento  | Numero Documento | Email                        | Operador | Celular    |
      | DNI             | 45455677         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR | 9542126940 |
    * el usuario selecciona un tarifario
      | Moneda  | Plan        |
      | DOLARES | TRADICIONAL |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-juridica
  @plan-tradicional-soles
  @C005
  Escenario: FLUJO NO CLIENTE CON MONEDA SOLES EN PLAN TRADICIONAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
      | Tipo Documento  | Numero Documento |
      | RUC             | 20304541980      |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa los datos de la empresa
      | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos personales del representate legal de tipo NO CLIENTE
      | Tipo Documento  | Numero Documento | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Email                       | Operador | Celular    |
      | DNI             | 40061009         | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario completa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero | Manzana | Lote | Interior | Urbanizacion | Referencia  |
      | LIMA         | LIMA      | LIMA     | JIRON    | MEXICO     | 545    | 16      | 2    | A2       | ANTON        | BANCO       |
    * el usuario selecciona un tarifario
      | Moneda  | Plan        |
      | SOLES   | TRADICIONAL |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-juridica
  @plan-digital-soles
  @C006
  Escenario: FLUJO NO CLIENTE CON MONEDA SOLES EN PLAN DIGITAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
      | Tipo Documento  | Numero Documento |
      | RUC             | 20304541980      |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa los datos de la empresa
      | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos personales del representate legal de tipo NO CLIENTE
      | Tipo Documento  | Numero Documento | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Email                       | Operador | Celular    |
      | DNI             | 40061010         | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario completa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero | Manzana | Lote | Interior | Urbanizacion | Referencia  |
      | LIMA         | LIMA      | LIMA     | JIRON    | MEXICO     | 545    | 16      | 2    | A2       | ANTON        | BANCO       |
    * el usuario selecciona un tarifario
      | Moneda  | Plan    |
      | SOLES   | DIGITAL |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-juridica
  @plan-digital-dolares
  @C007
  Escenario: FLUJO NO CLIENTE CON MONEDA DOLARES EN PLAN DIGITAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
      | Tipo Documento  | Numero Documento |
      | RUC             | 20304541980      |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa los datos de la empresa
      | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos personales del representate legal de tipo NO CLIENTE
      | Tipo Documento  | Numero Documento | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Email                       | Operador | Celular    |
      | DNI             | 40061011         | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario completa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero | Manzana | Lote | Interior | Urbanizacion | Referencia  |
      | LIMA         | LIMA      | LIMA     | JIRON    | MEXICO     | 545    | 16      | 2    | A2       | ANTON        | BANCO       |
    * el usuario selecciona un tarifario
      | Moneda  | Plan        |
      | DOLARES | DIGITAL     |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-juridica
  @plan-tradicional-dolares
  @C008
  Escenario: FLUJO NO CLIENTE CON MONEDA DOLARES EN PLAN TRADICIONAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
      | Tipo Documento  | Numero Documento |
      | RUC             | 20304541980      |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa los datos de la empresa
      | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos personales del representate legal de tipo NO CLIENTE
      | Tipo Documento  | Numero Documento | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Email                       | Operador | Celular    |
      | DNI             | 40061012         | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario completa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero | Manzana | Lote | Interior | Urbanizacion | Referencia  |
      | LIMA         | LIMA      | LIMA     | JIRON    | MEXICO     | 545    | 16      | 2    | A2       | ANTON        | BANCO       |
    * el usuario selecciona un tarifario
      | Moneda  | Plan        |
      | DOLARES | TRADICIONAL |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-natural
  @plan-tradicional-soles
  @C009
  Escenario: FLUJO CLIENTE - PERSONA NATURAL CON MONEDA SOLES EN PLAN TRADICIONAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA NATURAL
      | Tipo Documento  | Numero Documento |
      | DNI             | 49161035         |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa datos de la empresa
      | RUC         | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | 20304541980 | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos personales del representate legal de tipo CLIENTE
      | Tipo Documento  | Numero Documento | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Email                       | Operador | Celular    |
      | DNI             | 45120015         | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario selecciona un tarifario
      | Moneda  | Plan        |
      | SOLES   | TRADICIONAL |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-natural
  @plan-digital-dolares
  @C0010
  Escenario: FLUJO NO CLIENTE - PERSONA NATURAL CON MONEDA DOLARES EN PLAN DIGITAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA NATURAL
      | Tipo Documento  | Numero Documento |
      | CE              | 611102CE         |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa datos de la empresa
      | RUC         | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | 20304541980 | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos personales del representate legal de tipo NO CLIENTE
      | Tipo Documento  | Numero Documento | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Email                       | Operador | Celular    | Numero de pasaporte |
      | CE              | 611102CE         | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 | 611102CE            |
    * el usuario completa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero | Manzana | Lote | Interior | Urbanizacion | Referencia  |
      | LIMA         | LIMA      | LIMA     | JIRON    | MEXICO     | 545    | 16      | 2    | A2       | ANTON        | BANCO       |
    * el usuario selecciona un tarifario
      | Moneda  | Plan        |
      | DOLARES | DIGITAL     |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-natural
  @plan-digital-soles
  @C0011
  Escenario: FLUJO CLIENTE POTENCIAL - PERSONA NATURAL CON MONEDA SOLES EN PLAN DIGITAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA NATURAL
      | Tipo Documento  | Numero Documento |
      | DNI             | 40061012         |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa datos de la empresa
      | RUC         | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | 20304541980 | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos personales del representate legal de tipo POTENCIAL
      | Tipo Documento  | Numero Documento | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Email                       | Operador | Celular    |
      | DNI             | 45120015         | ROBERTO       | MANUEL         | GUITERREZ        | CHAVEZ           | EVERIS.QA.TESTING@GMAIL.COM | MOVISTAR | 9542126940 |
    * el usuario completa los datos de domicilio del representate legal
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero | Manzana | Lote | Interior | Urbanizacion | Referencia  |
      | LIMA         | LIMA      | LIMA     | JIRON    | MEXICO     | 545    | 16      | 2    | A2       | ANTON        | BANCO       |
    * el usuario selecciona un tarifario
      | Moneda  | Plan      |
      | SOLES   | DIGITAL   |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-juridica
  @plan-tradicional-bimoneda
  @C0012
  Escenario: FLUJO CLIENTE CON MONEDA BIMONEDA EN PLAN TRADICIONAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
      | Tipo Documento  | Numero Documento |
      | RUC             | 20100002621      |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa los datos de la empresa
      | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos del representate legal de tipo CLIENTE
      | Tipo Documento  | Numero Documento | Email                        | Operador | Celular    |
      | DNI             | 00540289         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR | 9542126940 |
    * el usuario selecciona un tarifario
      | Moneda   | Plan        |
      | BIMONEDA | TRADICIONAL |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente

  @persona-juridica
  @plan-digital-bimoneda
  @C0013
  Escenario: FLUJO CLIENTE CON MONEDA BIMONEDA EN PLAN DIGITAL
    Dado el usuario ingresa en ASSI
    * ingresa el tipo de documento y numero de documento de una PERSONA JURIDICA
      | Tipo Documento  | Numero Documento |
      | RUC             | 20100002621      |
    Cuando selecciona abrir una cuenta negocio
    * el usuario completa los datos de la empresa
      | PRINCIPALES CLIENTES | EMAIL                        | NUMERO DE PARTIDA REGISTRAL |
      | EMPRESA              | EVERIS.QA.TESTING@GMAIL.COM  | 5435435                     |
    * el usuario completa los datos del representate legal de tipo CLIENTE
      | Tipo Documento  | Numero Documento | Email                        | Operador | Celular    |
      | DNI             | 00540289         | EVERIS.QA.TESTING@GMAIL.COM  | MOVISTAR | 9542126940 |
    * el usuario selecciona un tarifario
      | Moneda   | Plan    |
      | BIMONEDA | DIGITAL |
    Entonces el usuario crea la cuenta negocio
    * el usuario verifica que la cuenta se genero exitosamente