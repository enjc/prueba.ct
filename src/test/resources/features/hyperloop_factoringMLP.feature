# language: es
@regresion-hyperloopFactoringMLP
Característica: MLP FACTORING

  @apertura-cuenta-dolares-hyperloop-Factoring
  @plan-digital-hyperloop-Factoring
  @RL-Girador-NoCliente
  @CF001
  Esquema del escenario: FLUJO RL NO CLIENTE Y GIRADOR NO CLIENTE APERTURA DE UNA CUENTA NEGOCIOS EN PLAN DIGITAL DOLARES
    Dado que el usuario desea aperturar una cuenta negocio en el flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal completa sus datos personales basado en "<TipoDocumento>"
      | Primer Nombre | Segundo Nombre   | Apellido Paterno   | Apellido Materno    | Fecha Nacimiento | Sexo   | Estado Civil | Pasaporte | P Nacimiento | P Nacionalidad |
      | GISELL        | ZARELA           | HUALLPA            | HUAMANI             | 12121991         | MUJER  | SOLTERO      | A0012778  | CHILE        | ARGENTINA      |
    * el representante legal completa sus datos de domicilio basado en "<TipoDocumento>"
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | LIMA     | AVENIDA  | MEXICO     | 545    |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales y de direccion de su empresa
      | Ventas anuales | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | 100000000      | LIMA         | LIMA      | LIMA     | CALLE    | MEXICO     | 545    |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 78000         | 150            |
    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |
    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | DIGITAL        |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 123456            | Empresas             |
    Y el representante legal ingresa el correo de trabajo de su empresa
      | Correo empresa            |
      |everis.mail.box@gmail.com  |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | ICA                  | CHINCHA                |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 47324544         | 20538016170 |
      | CE              | 473245441        | 20538027457 |
      | DNI             | 47324545         | 20538033007 |



  @apertura-cuenta-soles-hyperloop-Factoring
  @plan-digital-hyperloop-Factoring
  @RL-Girador-NoCliente
  @CF002
  Esquema del escenario: FLUJO RL NO CLIENTE Y GIRADOR NO CLIENTE APERTURA DE UNA CUENTA NEGOCIOS EN PLAN DIGITAL SOLES
    Dado que el usuario desea aperturar una cuenta negocio en el flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal completa sus datos personales basado en "<TipoDocumento>"
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Pasaporte | P Nacimiento | P Nacionalidad |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | A0012778  | BRASIL       | VENEZUELA      |
    * el representante legal completa sus datos de domicilio basado en "<TipoDocumento>"
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales y de direccion de su empresa
      | Ventas anuales | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | 100000000      | ICA          | ICA       | ICA      | CALLE    | BRISAS     | 789    |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |
    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | DIGITAL        |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal ingresa el correo de trabajo de su empresa
      | Correo empresa            |
      |everis.mail.box@gmail.com  |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | LIMA                 | BARRANCA               |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 47324546         | 20538038572 |
      | CE              | 473245442        | 20538049779 |
      | CE              | 473245443        | 20538051595 |

  @apertura-cuenta-dolares-hyperloop-Factoring
  @plan-tradicional-hyperloop-Factoring
  @RL-Girador-NoCliente
  @CF003
  Esquema del escenario: FLUJO RL NO CLIENTE Y GIRADOR NO CLIENTE APERTURA DE UNA CUENTA NEGOCIOS EN PLAN TRADICIONAL DOLARES
    Dado que el usuario desea aperturar una cuenta negocio en el flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal completa sus datos personales basado en "<TipoDocumento>"
      | Primer Nombre | Segundo Nombre   | Apellido Paterno   | Apellido Materno    | Fecha Nacimiento | Sexo   | Estado Civil | Pasaporte | P Nacimiento | P Nacionalidad |
      | GISELL        | ZARELA           | HUALLPA            | HUAMANI             | 12121991         | MUJER  | SOLTERO      | A0012778  | CHILE        | ARGENTINA      |
    * el representante legal completa sus datos de domicilio basado en "<TipoDocumento>"
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | LIMA     | AVENIDA  | MEXICO     | 545    |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales y de direccion de su empresa
      | Ventas anuales | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | 100000000      | LIMA         | LIMA      | LIMA     | CALLE    | MEXICO     | 545    |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 78000         | 60             |
    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |
    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | TRADICIONAL    |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 123456            | Empresas             |
    Y el representante legal ingresa el correo de trabajo de su empresa
      | Correo empresa            |
      |everis.mail.box@gmail.com  |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | LIMA                 | LIMA                   |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 47324547         | 20538054691 |
      | CE              | 473245444        | 20538056805 |
      | DNI             | 47324548         | 20538065545 |

  @apertura-cuenta-soles-hyperloop-Factoring
  @plan-tradicional-hyperloop-Factoring
  @RL-Girador-NoCliente
  @CF004
  Esquema del escenario: FLUJO RL NO CLIENTE Y GIRADOR NO CLIENTE APERTURA DE UNA CUENTA NEGOCIOS EN PLAN TRADICIONAL SOLES
    Dado que el usuario desea aperturar una cuenta negocio en el flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal completa sus datos personales basado en "<TipoDocumento>"
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Pasaporte | P Nacimiento | P Nacionalidad |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | A0012778  | BRASIL       | VENEZUELA      |
    * el representante legal completa sus datos de domicilio basado en "<TipoDocumento>"
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales y de direccion de su empresa
      | Ventas anuales | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | 100000000      | ICA          | ICA       | ICA      | JIRON    | BRISAS     | 789    |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |
    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | TRADICIONAL    |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal ingresa el correo de trabajo de su empresa
      | Correo empresa            |
      |everis.mail.box@gmail.com  |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | ANCASH               | SANTA                  |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 47324548         | 20538066941 |
      | CE              | 473245445        | 20538076237 |


  @apertura-cuenta-ambasmonedas-hyperloop-Factoring
  @plan-tradicional-hyperloop-Factoring
  @RL-Girador-NoCliente
  @CF005
  Esquema del escenario: FLUJO RL NO CLIENTE Y GIRADOR NO CLIENTE APERTURA DE UNA CUENTA NEGOCIOS EN PLAN TRADICIONAL CON AMBAS MONEDAS
    Dado que el usuario desea aperturar una cuenta negocio en el flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal completa sus datos personales basado en "<TipoDocumento>"
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Pasaporte | P Nacimiento | P Nacionalidad |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | A0012778  | BRASIL       | VENEZUELA      |
    * el representante legal completa sus datos de domicilio basado en "<TipoDocumento>"
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales y de direccion de su empresa
      | Ventas anuales | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | 100000000      | ICA          | ICA       | ICA      | JIRON    | BRISAS     | 789    |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de las ventas mensuales en soles de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 150            |
    Y el representante legal completa los datos de las ventas mensuales en dolares de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 79000         | 180            |
    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |
    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | TRADICIONAL    |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal ingresa el correo de trabajo de su empresa
      | Correo empresa            |
      |everis.mail.box@gmail.com  |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | LIMA                 | BARRANCA               |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 47324549         | 20538085571 |
      | CE              | 473245446        | 20538093590 |


  @apertura-cuenta-ambasmonedas-hyperloop-Factoring
  @plan-digital-hyperloop-Factoring
  @RL-Girador-NoCliente
  @CF006
  Esquema del escenario: FLUJO RL NO CLIENTE Y GIRADOR NO CLIENTE APERTURA DE UNA CUENTA NEGOCIOS EN PLAN DIGITAL CON AMBAS MONEDAS
    Dado que el usuario desea aperturar una cuenta negocio en el flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal completa sus datos personales basado en "<TipoDocumento>"
      | Primer Nombre | Segundo Nombre   | Apellido Paterno   | Apellido Materno    | Fecha Nacimiento | Sexo   | Estado Civil | Pasaporte | P Nacimiento | P Nacionalidad |
      | GISELL        | ZARELA           | HUALLPA            | HUAMANI             | 12121991         | MUJER  | SOLTERO      | A0012778  | CHILE        | ARGENTINA      |
    * el representante legal completa sus datos de domicilio basado en "<TipoDocumento>"
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | LIMA         | LIMA      | LIMA     | AVENIDA  | MEXICO     | 545    |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales y de direccion de su empresa
      | Ventas anuales | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | 100000000      | ICA          | ICA       | ICA      | JIRON    | BRISAS     | 789    |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de las ventas mensuales en soles de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal completa los datos de las ventas mensuales en dolares de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 79000         | 120            |
    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |
    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | DIGITAL        |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Empresas             |
    Y el representante legal ingresa el correo de trabajo de su empresa
      | Correo empresa            |
      |everis.mail.box@gmail.com  |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | PIURA                | SULLANA                |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 47324550         | 20538101525 |
      | CE              | 473245447        | 20538106080 |

  @afiliacion-hyperloop-Factoring
  @RL-Girador-Cliente
  @CF007
  Esquema del escenario: FLUJO RL CLIENTE Y GIRADOR CLIENTE AFILIACION DE CUENTA EN SOLES
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal selecciona la opcion Estos Datos No son Mios
    Y el representante legal se autentica por equifax

    Y el representante legal selecciona la oferta en la moneda
      | Moneda Cuenta  |
      | SOLES          |
    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |

    Entonces el representante legal acepta y envía la solicitud de afiliacion para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento   | Ruc         |
      | DNI             | 01282520          | 20168910020 |
      | CE              | CE010107          | 20528272216 |


  @afiliacion-hyperloop-Factoring
  @RlNoCliente-GiradorCliente
  @CF008
  Esquema del escenario: FLUJO RL NO CLIENTE Y GIRADOR CLIENTE AFILIACION DE CUENTA EN DOLARES
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |
    Y el representante legal completa sus datos personales basado en "<TipoDocumento>"
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Pasaporte | P Nacimiento | P Nacionalidad |
      | ANGELO        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | A0012778  | BRASIL       | VENEZUELA      |
    * el representante legal completa sus datos de domicilio basado en "<TipoDocumento>"
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |
    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 78000         | 60             |

    Y el representante legal se autentica por equifax

    Y el representante legal selecciona la oferta en la moneda
      | Moneda Cuenta  |
      | DOLARES        |
    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |

    Entonces el representante legal acepta y envía la solicitud de afiliacion para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento   | Ruc         |
      | CE              | 473245787         | 20134782103 |
      | DNI             | 47324577          | 20528234985 |
      | DNI             | 47324578          | 20134782103 |

  @afiliacion-hyperloop-Factoring
  @RL-Girador-Cliente
  @CF009
  Esquema del escenario: FLUJO RL CLIENTE Y GIRADOR CLIENTE AFILIACION DE CUENTA EN AMBAS MONEDAS
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |

    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |

    Y el representante legal completa los datos de las ventas mensuales en soles de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal completa los datos de las ventas mensuales en dolares de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 79000         | 120            |
    Y el representante legal selecciona la opcion Estos Datos No son Mios
    Y el representante legal se autentica por equifax

    Y el representante legal selecciona la oferta en la moneda
      | Moneda Cuenta  |
      | AMBAS          |
    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |

    Entonces el representante legal acepta y envía la solicitud de afiliacion para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 01282520         | 20100439485 |
      | CE              | CE010107         | 20100439485 |
      | DNI             | 99223300         | 20528261958 |


  @apertura-cuenta-dolares-hyperloop-Factoring
  @plan-tradicional-hyperloop-Factoring
  @RL-Girador-Cliente
  @CF010
  Esquema del escenario: FLUJO RL CLIENTE Y GIRADOR CLIENTE CON CUENTA EN SOLES APERTURA DE UNA CUENTA NEGOCIOS EN PLAN TRADICIONAL DOLARES
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de las ventas mensuales en soles de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal completa los datos de las ventas mensuales en dolares de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 79000         | 120            |
    Y el representante legal selecciona la opcion Estos Datos No son Mios
    Y el representante legal se autentica por equifax

    Y el representante legal selecciona la oferta en la moneda
      | Moneda Cuenta  |
      | AMBAS          |
    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |

    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | TRADICIONAL    |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | PIURA                | SULLANA                |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 01282520         | 20170286180 |
      | CE              | CE010107         | 20170453931 |


  @apertura-cuenta-soles-hyperloop-Factoring
  @plan-digital-hyperloop-Factoring
  @RL-Girador-Cliente
  @CF011
  Esquema del escenario: FLUJO RL CLIENTE Y GIRADOR CLIENTE CON CUENTA EN DOLARES APERTURA DE UNA CUENTA NEGOCIOS EN PLAN DIGITAL SOLES
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de las ventas mensuales en soles de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal completa los datos de las ventas mensuales en dolares de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 79000         | 120            |
    Y el representante legal selecciona la opcion Estos Datos No son Mios
    Y el representante legal se autentica por equifax

    Y el representante legal selecciona la oferta en la moneda
      | Moneda Cuenta  |
      | AMBAS          |
    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |

    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | DIGITAL        |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | PIURA                | SULLANA                |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 01282520         | 20510194668 |
      | CE              | CE010107         | 20538106161 |


  @apertura-cuenta-soles-hyperloop-Factoring
  @plan-digital-hyperloop-Factoring
  @RL-Girador-Cliente
  @CF012
  Esquema del escenario: FLUJO RL CLIENTE Y GIRADOR CLIENTE CON CUENTA EN DOLARES APERTURA DE UNA CUENTA NEGOCIOS EN PLAN DIGITAL SOLES
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de las ventas mensuales en soles de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal completa los datos de las ventas mensuales en dolares de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 79000         | 120            |
    Y el representante legal selecciona la opcion Estos Datos No son Mios
    Y el representante legal se autentica por equifax

    Y el representante legal selecciona la oferta en la moneda
      | Moneda Cuenta  |
      | SOLES          |
    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |

    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | DIGITAL        |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | PIURA                | PAITA                  |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 01282520         | 20538108295 |
      | CE              | CE010107         | 20538109348 |


  @apertura-cuenta-dolares-hyperloop-Factoring
  @plan-digital-hyperloop-Factoring
  @RL-Girador-Cliente
  @CF013
  Esquema del escenario: FLUJO RL CLIENTE Y GIRADOR CLIENTE CON CUENTA EN SOLES INGRESA VENTAS EN DOLARES APERTURA DE UNA CUENTA NEGOCIOS EN PLAN DIGITAL DOLARES
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 78000         | 60             |

    Y el representante legal selecciona la opcion Estos Datos No son Mios
    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |
    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | DIGITAL        |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | LIMA                 | LIMA                   |

    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 01282520         | 20510134410 |
      | CE              | CE010107         | 20538110869 |

  @apertura-cuenta-soles-hyperloop-Factoring
  @plan-tradicional-hyperloop-Factoring
  @RL-Girador-Cliente
  @CF014
  Esquema del escenario: FLUJO RL CLIENTE Y GIRADOR CLIENTE CON CUENTA EN DOLARES INGRESA VENTAS EN SOLES APERTURA DE UNA CUENTA NEGOCIOS EN PLAN TRADICIONAL SOLES
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | INTRADEVCO          |
    Y el representante legal completa los datos de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |

    Y el representante legal selecciona la opcion Estos Datos No son Mios
    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |
    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | TRADICIONAL    |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | LIMA                 | LIMA                   |
    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 01282520         | 20538112993 |
      | CE              | CE010107         | 20538113612 |

  @apertura-cuenta-soles-hyperloop-Factoring
  @plan-digital-hyperloop-Factoring
  @RlNoCliente-GiradorCliente
  @CF015
  Esquema del escenario: FLUJO RL NO CLIENTE Y GIRADOR CLIENTE CON CUENTA EN DOLARES APERTURA DE UNA CUENTA NEGOCIOS EN PLAN DIGITAL SOLES
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal completa sus datos personales basado en "<TipoDocumento>"
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Pasaporte | P Nacimiento | P Nacionalidad |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | A0012778  | BRASIL       | VENEZUELA      |
    * el representante legal completa sus datos de domicilio basado en "<TipoDocumento>"
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de las ventas mensuales en soles de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal completa los datos de las ventas mensuales en dolares de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 79000         | 120            |

    Y el representante legal se autentica por equifax

    Y el representante legal selecciona la oferta en la moneda
      | Moneda Cuenta  |
      | AMBAS          |
    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |

    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | DIGITAL        |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | ICA                  | NAZCA                  |

    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 47324552         | 20538117952 |
      | CE              | 473245449        | 20538118096 |


  @apertura-cuenta-ambasmonedas-hyperloop-Factoring
  @plan-tradicional-hyperloop-Factoring
  @RlNoCliente-GiradorCliente
  @CF016
  Esquema del escenario: FLUJO RL NO CLIENTE Y GIRADOR CLIENTE SIN CUENTAS APERTURA DE UNA CUENTA NEGOCIOS EN PLAN TRADICIONAL CON AMBAS MONEDAS
    Dado que el usuario ingresa al flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |
    Y el representante legal acepta LDPD

    Y el representante legal completa sus datos personales basado en "<TipoDocumento>"
      | Primer Nombre | Segundo Nombre | Apellido Paterno | Apellido Materno | Fecha Nacimiento | Sexo   | Estado Civil | Pasaporte | P Nacimiento | P Nacionalidad |
      | DANIEL        | LUIS           | PEREZ            | LEON             | 25051993         | HOMBRE | CASADO       | A0012778  | BRASIL       | VENEZUELA      |
    * el representante legal completa sus datos de domicilio basado en "<TipoDocumento>"
      | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | ICA          | ICA       | ICA      | CALLE    | LOS LIRIOS | 678    |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales de su empresa
      | Ventas anuales |
      | 100000000      |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de las ventas mensuales en soles de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal completa los datos de las ventas mensuales en dolares de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 79000         | 120            |

    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |

    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | TRADICIONAL    |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Personas             |
    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | LIMA                 | CAÑETE                 |

    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | DNI             | 47324553         | 20538119815 |
      | CE              | 473245450        | 20538128997 |

  @apertura-cuenta-ambasmonedas-hyperloop-Factoring
  @plan-tradicional-hyperloop-Factoring
  @RlCliente-GiradorNoCliente
  @CF017
  Esquema del escenario: FLUJO RL CLIENTE Y GIRADOR NO CLIENTE APERTURA DE UNA CUENTA NEGOCIOS EN PLAN TRADICIONAL CON AMBAS MONEDAS
    Dado que el usuario desea aperturar una cuenta negocio en el flujo de factoring MLP
    Cuando el usuario completa los datos del representante legal "<TipoDocumento>", "<NumeroDocumento>"
      | Email               | Operador | Celular   |
      | QAALTERNA@GMAIL.COM | MOVISTAR | 930145807 |

    Y el representante legal ingresa y busca el "<Ruc>" de su empresa

    Y el representante legal completa las ventas anuales y de direccion de su empresa
      | Ventas anuales | Departamento | Provincia | Distrito | Tipo Via | Nombre Via | Numero |
      | 100000000      | ICA          | ICA       | ICA      | JIRON    | BRISAS     | 789    |
    Y el representante legal busca la razon social de su empresa
      | Razon social        |
      | CHIMU               |
    Y el representante legal completa los datos de las ventas mensuales en soles de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | SOLES    | 78000         | 60             |
    Y el representante legal completa los datos de las ventas mensuales en dolares de su aceptante
      | Moneda   | Monto mensual | Plazo mensual  |
      | DOLARES  | 79000         | 120            |

    Y el representante legal selecciona la opcion Estos Datos No son Mios
    Y el representante legal se autentica por equifax

    Y el representante legal acepta los TC e ingresa datos de contacto de su empresa
      | Nombre contacto | Email contacto    |
      | Kevin Paucar    | qa1993@everis.com |
    Y el representante legal selecciona abrir una cuenta negocios
    Y el representante legal elige un plan tarifario
      | Plan tarifario |
      | TRADICIONAL    |
    Y el representante legal ingresa el numero de partida registral y selecciona Principales Clientes
      | Partida Registral | Principales clientes |
      | 12345678          | Empresas             |
    Y el representante legal ingresa el correo de trabajo de su empresa
      | Correo empresa            |
      |everis.mail.box@gmail.com  |

    Y el representante legal selecciona la agencia de operaciones mas frecuentes
      | Agencia Departamento | Provincia Departamento |
      | ICA                  | CHINCHA                |

    Entonces el representante legal apertura una cuenta negocios para su empresa

    Ejemplos:
      | TipoDocumento   | NumeroDocumento  | Ruc         |
      | CE              | CE010107         | 20538132242 |
      | DNI             | 01282520         | 20538132757 |